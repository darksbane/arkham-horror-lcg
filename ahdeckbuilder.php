<?php

/**
 * <pre>
 * Drew Dallas
 * Last Updated: $Date: 2016-08-29
 * </pre>
 *
 * @author 		$Author: Drew Dallas $
 * @package		IP.Content
 * @version		$Revision: .1 $
 *
 */
class public_ccs_ajax_ahdeckbuilder extends ipsAjaxCommand 
{
	 /**
	 * Class entry point
	 *
	 * @access	public
	 * @param	object		Registry reference
	 * @return	@e void		[Outputs to screen]
	 */
	public function doExecute( ipsRegistry $registry ) 
	{
		
		switch( $this->request['do'] )
		{
			case 'loadDeck':
				$this->fetchLoadDeck();
			case 'export':
				$this->exportCards();
			case 'save':
				$this->saveDeck();
			case 'share':
				$this->shareDeck();
			case 'checkforsubmission':
				$this->checkDeckSubmission();
			case 'submit':
				$this->submitDeck();
			case 'update':
				$this->saveDeck();
			case 'delete':
				$this->deleteDeck();
			case 'loadSavedDecks':
				$this->fetchSavedDecksList();
			case 'cardpopup':
				$this->cardpopupjs();
			default:
				$this->defaultMessage();
    		break;
		}

	}
	
	/**
	 * Further input scrubbing to prevent xss and any HTML injection
	 *
	 * @access	public
	 * @param	string 		Field data
	 * @param	bool 		reconvert HTMLEntities
	 * @return	string		cleaned data
	 */
	public function scrubInput( $scrubInputValue, $fixhtmlentities ){
		
		if(!$this->registry->isClassLoaded('HTMLPurifier')){
			$classToLoad	= IPSLib::loadLibrary( IPSLib::getAppDir('ccs') . '/sources/databases/validators/htmlpurifier/HTMLPurifier.standalone.php', 'HTMLPurifier', 'ccs' );
			$this->registry->setClass( 'HTMLPurifier', new $classToLoad( $this->registry ) );
		}	
			
		$value = IPSText::xssMakeJavascriptSafe($scrubInputValue);
		$value = $this->registry->HTMLPurifier->purify( $value );
		if ($fixhtmlentities){
			$value = $this->convertAndMakeSafe( IPSText::br2nl($value) ); //Html purifier converts htmlspecial characters back to normal, we don't want this
		}
		return $value;
	}
	
	/**
	 * Default message for unknown actions
	 *
	 * @access	public
	 * @return	string		Error Message
	 */
	public function defaultMessage(){
		$error['errorMessage'] = 'Unknown action requested.';
		$error['status'] = 'error';
		$this->returnJsonArray( $error );
	}

	public function shareDeck( )
	{	
		$memberID = $this->request['pid'];
		$deckGUID = $this->request['dguid'];
		$deckID = $this->request['did'];
		if (preg_match("/^[0-9]+$/", $deckID) && preg_match("/^[0-9]+$/", $memberID) && preg_match("/^(ahdeck_)[0-9a-zA-Z]+$/", $deckGUID)) {
				$this->DB->build( array( 'select' => '*', 'from' => 'ccs_custom_database_47', 'where' => "field_662 ='{$deckGUID}' and primary_id_field = '{$deckID}' and member_id ='{$memberID}'" ) );
				$decks = $this->DB->execute();
				$num_rows = $this->DB->getTotalRows();	
				if ($num_rows == 0){
					$error['status'] = 'error';
					$error['errorMessage'] = 'Specified deck does not exist!';
					$this->returnJsonArray( $error );
				} else {
					while ( $row = $this->DB->fetch($decks) ){
						$sumMember = IPSMember::load($row['member_id']);
						$obj = json_decode(str_ireplace('|sq|','&#39;',htmlspecialchars_decode($this->scrubInput( $row['field_663'],true), ENT_QUOTES)));
						$objI = json_decode(str_ireplace('|sq|','&#39;',htmlspecialchars_decode($this->scrubInput( $row['field_664'],true), ENT_QUOTES)));

						$return	= array(
							'status'	=> 'success',
							'name'	=> $this->scrubInput( $row['field_658'],true),
							'strategy'		=> $this->scrubInput( $row['field_660'],true),
							'teaser'	=> $this->scrubInput( $row['field_659'],true),
							'tags'	=> $this->scrubInput( $row['field_661'],true),
							'deckClass'	=> $this->scrubInput( $row['field_676'],true),
							'deckContents'	=> $obj,
							'deckInvestigator' =>	$objI,
							'campaign'		=> $this->scrubInput( $row['field_678'],true),
							'notes'			=> $this->scrubInput( $row['field_679'],true),
							'physicaltrauma'	=> $this->scrubInput( $row['field_680'],true),
							'mentaltrauma'	=> $this->scrubInput( $row['field_681'],true),
							'unspentxp'		=> $this->scrubInput( $row['field_682'],true),
							'deckGUID'	=> $this->scrubInput( $row['field_662'],true),
							'deckid'		=> $row['primary_id_field'],
							'member' => $this->scrubInput( $sumMember['members_display_name'],true),
							'game'	=> 'arkhamhorror'
							);

						$this->returnJsonArray( $return );
				 }
			  }
			
		} else {
			$error['status'] = 'error';
			$error['errorMessage'] = 'Cannot Locate Deck!';
			$this->returnJsonArray( $error );
		}
	}

	public function submitDeck( )
	{	
		$primaryKey = intval($this->request['did']);
		$game = $this->request['fgame'];
		$deckGUID = $this->request['dguid'];
		
		//checks to see if primarykey is a non 0 number and that the GUID matches the pattern
		if ($primaryKey && preg_match("/^(ahdeck_)[0-9a-zA-Z]+$/", $deckGUID)) {
			if (!$this->memberData['member_id']){
				$error['status'] = 'error';
				$error['errorMessage'] = 'Unable to submit deck. You are not logged in.';
				$this->returnJsonArray( $error );
			} else {
				//Flood control. I don't want people spam submitting their decks so we put a 2 minute limit inbetween when they can submit.
				$this->DB->build( array( 'select' => 'max(record_updated) as lasttime', 'from' => 'ccs_custom_database_48', 'where' => 'member_id ="'.$this->memberData['member_id'].'"' ) );
				$flood = $this->DB->execute();
				while ( $frow = $this->DB->fetch($flood) ){
					if ($frow['lasttime']==null){
						$cansave= true;
					} else {
						if ($frow['lasttime']+120 < time())
							$cansave= true;
						else
							$cansave= false;
					}
				}
				
				if ($cansave){
					//grab saved deck from database
					$this->DB->build( array( 'select' => '*', 'from' => 'ccs_custom_database_47', 'where' => "member_id ='{$this->memberData['member_id']}' and primary_id_field ='{$primaryKey}'" ) );
					$decks = $this->DB->execute();
					$num_rows = $this->DB->getTotalRows();	
					if ($num_rows == 0){ //no saved decks were found
						$error['status'] = 'error';
						$error['errorMessage'] = 'Unable to submit deck. Specified deck does not exist!';
						$this->returnJsonArray( $error );
					} else {
						while ( $row = $this->DB->fetch($decks) ){
							$stopDoubleEncodeNeedle = array("|sq|","|dq|");
							$stopDoubleEncodeReplace = array("&#39;","&amp;quot;");
							//grabs JSON from database and decode it so we can grab the card ids
							$obj = json_decode(str_ireplace($stopDoubleEncodeNeedle,$stopDoubleEncodeReplace,htmlspecialchars_decode($this->scrubInput( $row['field_663'],true), ENT_QUOTES)));

							$cardIDs = ",";
							foreach ($obj as $card){
								$cardIDs .= $card->id.",";
							}
							
							//most decks should default to approved
							$approved = "1";

							$category = 493;
							////////////////////////////////////////////////////
							///////Holds database info, the following query can be used to get live info but since I don't currently need
							///////to abstract this to work with multiple databases I'm saving the query and hardcodeing the info
							/*$this->database	= $this->DB->buildAndFetch( array(
															'select'	=> 'd.*',
															'from'		=> array( 'ccs_databases' => 'd' ),
															'where'		=> 'd.database_id=' . $_id,
															'add_join'	=> array(
																				array(
																					'select'	=> 'i.*',
																					'from'		=> array( 'permission_index' => 'i' ),
																					'where'		=> "i.app='ccs' AND i.perm_type='databases' AND i.perm_type_id=d.database_id",
																					'type'		=> 'left',
																					),
																				),
													)		);
							*///////////////////////////////////////////////////////
							$_database= array(
									'database_id'=> 48,
									'database_field_title' => 'field_665',
									'database_database' => 'ccs_custom_database_48'
								);
								
							//Gets Category info for database
							$_categories	= $this->registry->ccsFunctions->getCategoriesClass( $_database );//48 is database number
							
							//Search to see if this deck has already been submitted, if yes update it, if no insert it new

							$exists	= $this->DB->buildAndFetch( array( 'select' => '*', 'from' => $_database['database_database'], 'where' =>"member_id ='{$this->memberData['member_id']}' and field_673 ='{$primaryKey}'" ) );
							if( !$exists['primary_id_field'] ){
								//array of data to save
								$_save		= array( 
									'member_id'=> $this->memberData['member_id'],
									'record_saved'=> time(),
									'record_updated'=> time(),
									'post_key'=>md5( microtime() ),
									'record_approved'=>$approved,
									'category_id'=>$category,
									'record_dynamic_furl' => IPSText::makeSeoTitle( $row['field_658'] ),
									'field_665'=>$row['field_658'], //name
									'field_667'=>$row['field_659'], //teaser
									'field_668'=>$row['field_660'], //strategy
									'field_669'=>$row['field_661'], //suggested Tags
									'field_677'=>$row['field_676'], //deck class
									'field_672'=>$row['field_662'], //Saved deck Guid
									'field_683'=>$row['field_678'], //Campaign or Scenario
									'field_684'=>$row['field_679'], //Campaign Notes
									'field_685'=>$row['field_680'], //Physical Trauma
									'field_686'=>$row['field_681'], //Mental Trauma
									'field_687'=>$row['field_682'], //Unspent XP
									'field_671'=>str_ireplace($stopDoubleEncodeNeedle,$stopDoubleEncodeReplace,$row['field_664']), //Investigator
									'field_666'=>'1', //deck version
									'field_673'=>$primaryKey, //Saved Deck ID
									'field_674'=>$cardIDs, //Forign Key for card ids
									'field_675'=>$row['record_updated'], //last updated time of the saved deck
									'field_670'=>str_ireplace($stopDoubleEncodeNeedle,$stopDoubleEncodeReplace,$row['field_663']) // current JSON 
								);
								
								$this->DB->insert( $_database['database_database'], $_save );
								
								$id	= $this->DB->getInsertId();
								$_save['primary_id_field']	= $id;
								
								//-----------------------------------------
								// Update database counts
								//-----------------------------------------
								$this->DB->update( 'ccs_databases', "database_record_count=database_record_count+1", 'database_id='.$_database['database_id'], false, true );
								
								//if we decided to modqueue decks, would not need to run the above count update if modqueuing
								/*$modqueue       = array(
									'mod_database'          => $_database['database_id'],
									'mod_record'            => $id,
									'mod_comment'           => 0,
									'mod_poster'            => $this->memberData['member_id'],
									);
							
								$this->DB->insert( 'ccs_database_modqueue', $modqueue );*/
								
								//-----------------------------------------
								// Send notifications
								//-----------------------------------------								
								$_save['poster_name']			= $this->memberData['members_display_name'];
								
								
								$classToLoad	= IPSLib::loadLibrary( IPSLib::getAppDir( 'ccs' ) . '/sources/databases.php', 'databaseBuilder', 'ccs' );
								$databases		= new $classToLoad( $this->registry );
								$databases->sendRecordNotification( $_database, $_categories->categories[ $_save['category_id'] ], $_save );
								
								//-----------------------------------------
								// Recache categories
								//-----------------------------------------
								$_categories->recache( $_save['category_id'] );
								
								//return results
								$return	= array(
										'status'	=> 'success',
										'message'		=> 'Deck submission successful. It should now be in the Arkham Horror Deck Section.'
										);
			
								$this->returnJsonArray( $return );
							} else {
								if ($row['record_updated']==$exists['field_675']){
									$error['status'] = 'error';
									$error['errorMessage'] = 'Unable to submit deck. The currently saved deck and the submitted deck appear to be the same.';
									$this->returnJsonArray( $error );
								} else {
									$_update		= array( 
										'member_id'=> $this->memberData['member_id'],
										'record_updated'=> time(),
										'category_id'=>$category,
										'record_dynamic_furl' => IPSText::makeSeoTitle( $row['field_658'] ),
										'field_665'=>$row['field_658'], //name
										'field_667'=>$row['field_659'], //teaser
										'field_668'=>$row['field_660'], //strategy
										'field_669'=>$row['field_661'], //suggested Tags
										'field_677'=>$row['field_676'], //deck class
										'field_672'=>$row['field_662'], //Saved deck Guid
										'field_683'=>$row['field_678'], //Campaign or Scenario
										'field_684'=>$row['field_679'], //Campaign Notes
										'field_685'=>$row['field_680'], //Physical Trauma
										'field_686'=>$row['field_681'], //Mental Trauma
										'field_687'=>$row['field_682'], //Unspent XP
										'field_666'=>intval($exists['field_666'])+1, //deck version
										'field_674'=>$cardIDs, //Forign Key for card ids
										'field_675'=>$row['record_updated'], //last updated time of the saved deck
										'field_671'=>str_ireplace($stopDoubleEncodeNeedle,$stopDoubleEncodeReplace,$row['field_664']), //Investigator
										'field_670'=>str_ireplace($stopDoubleEncodeNeedle,$stopDoubleEncodeReplace,$row['field_663']) // current JSON 
									);
									$this->DB->update( $_database['database_database'], $_update, 'primary_id_field=' . $exists['primary_id_field'] );
									
									//-----------------------------------------
									// Recache category
									//-----------------------------------------

									$_categories->recache( $_update['category_id'] );
									
									if( $exists['category_id'] AND $_update['category_id'] != $exists['category_id'] )
									{
										$_categories->recache( $exists['category_id'] );
									}
									$return	= array(
											'status'	=> 'success',
											'message'		=> 'Deck update successful. The new version should now be in the Arkham Horror Deck Section.'
											);
									$this->returnJsonArray( $return );
								}
							}
						}
					}	
				} else {
					$error['status'] = 'error';
					$error['errorMessage'] = 'Unable to submit deck. Decks can only be submitted once every 2 minutes.';
					$this->returnJsonArray( $error );
				}
			}
		} else {
			$error['status'] = 'error';
			$error['errorMessage'] = 'Specified deck could not be found!';
			$this->returnJsonArray( $error );
		}
		
	}

	public function checkDeckSubmission(){
		if (!$this->memberData['member_id']){
			$error['errorMessage'] = 'Unable to submit deck. You are not logged in.';
			$error['status'] = 'error';
			$this->returnJsonArray( $error );
		} else {
			//The user is logged in
			$deckid = $this->DB->addSlashes($this->request['did']);
			$deckGUID = $this->DB->addSlashes($this->request['dguid']);
			if (preg_match("/^[0-9]+$/", $deckid) && preg_match("/^(ahdeck_)[0-9a-zA-Z]+$/", $deckGUID)) {

				$this->DB->build( array( 'select' => '*', 'from' => 'ccs_custom_database_47', 'where' => "member_id ='{$this->memberData['member_id']}' and primary_id_field ='{$deckid}' and field_662 ='{$deckGUID}'" ) );
				$decks = $this->DB->execute();
				$num_rows = $this->DB->getTotalRows();
				if ($num_rows == 0){
					$error['status'] = 'error';
					$error['errorMessage'] = 'Specified deck could not be found!';
					$this->returnJsonArray( $error );
				} else {
					$stopDoubleEncodeNeedle = array("|sq|","[br]","|dq|");
					$stopDoubleEncodeReplace = array("&#39;", "<br />","&amp;quot;");
					while ( $row = $this->DB->fetch($decks) ){
						$obj = json_decode(str_ireplace($stopDoubleEncodeNeedle,$stopDoubleEncodeReplace,htmlspecialchars_decode($this->scrubInput( $row['field_663'],true), ENT_QUOTES)));
						$objI = json_decode(str_ireplace($stopDoubleEncodeNeedle,$stopDoubleEncodeReplace,htmlspecialchars_decode($this->scrubInput( $row['field_664'],true), ENT_QUOTES)));
	

						$return['deck'][]	= array(
							'name'			=> $this->scrubInput( $row['field_658'],true),
							'strategy'		=> $this->scrubInput( $row['field_660'],true),
							'teaser'		=> $this->scrubInput( $row['field_659'],true),
							'tags'			=> $this->scrubInput( $row['field_661'],true),
							'campaign'		=> $this->scrubInput( $row['field_678'],true),
							'notes'			=> $this->scrubInput( $row['field_679'],true),
							'physicaltrauma'	=> $this->scrubInput( $row['field_680'],true),
							'mentaltrauma'	=> $this->scrubInput( $row['field_681'],true),
							'unspentxp'		=> $this->scrubInput( $row['field_682'],true),
							'deckContents'	=> $obj,
							'deckInvestigator'	=> $objI,
							'deckGUID'		=> $this->scrubInput( $row['field_662'],true),
							'deckid'		=> $row['primary_id_field']
							);

					}
					$this->DB->build( array( 'select' => '*', 'from' => 'ccs_custom_database_48', 'where' => "member_id ='{$this->memberData['member_id']}' and field_673 ='{$deckid}' " ) );
					$submitted = $this->DB->execute();
					$num_submittedrows = $this->DB->getTotalRows();
					if ($num_submittedrows > 0){
						$return['submitted'][]	= array(
							'name'	=> $row['field_665'],
							'version'		=> $row['field_666'] ,
							'id'	=> $row['primary_id_field'],
							'guid'	=> $row['field_672']
							);
					}
					$return['status'] = "success";
					$return['message'] = "Please review your deck before submiting.";
					$this->returnJsonArray( $return );
				}
			} else {	//  The ID or GUID was not of the proper format
				$error['status'] = 'error';
				$error['errorMessage'] = 'Specified deck could not be found!';
				$this->returnJsonArray( $error );
			}
		}
	}

	public function deleteDeck(){
		if (!$this->memberData['member_id']){
			$error['errorMessage'] = 'Unable to delete deck. You are not logged in.';
			$error['status'] = 'error';
			$this->returnJsonArray( $error );
		} else {
			$deckid = $this->DB->addSlashes($this->request['did']);
			$pkeyValid =preg_match("/^[0-9]+$/", $deckid);
			$deckGUID = $this->DB->addSlashes($this->request['dguid']);
			$deckGUIDValid =preg_match("/^(ahdeck_)[0-9a-zA-Z]+$/", $deckGUID);
			if (!$pkeyValid || !$deckGUIDValid){
				
				$error['errorMessage'] = 'Deck could not be found.';
				$error['status'] = 'error';
				$this->returnJsonArray( $error );
			} else {
				$game = $this->request['fgame'];
				if ($game != 'arkhamhorror'){
					$error['errorMessage'] = 'Deck could not be found.';
					$error['status'] = 'error';
					$this->returnJsonArray( $error );
				} else {
					$onDatabase = 'ccs_custom_database_47';
				}
				
				$this->DB->delete( $onDatabase,"member_id ='{$this->memberData['member_id']}' and primary_id_field ={$deckid} and field_662 ='{$deckGUID}' ");
				$decksDeleted = $this->DB->getAffectedRows();
				if ($decksDeleted ==0){
					$error['errorMessage'] = 'Unable to delete deck. Deck could not be found.';
					$error['status'] = 'error';
					$this->returnJsonArray( $error );
				}else{
					$msg = "Delete successful. ".$decksDeleted." total decks deleted.";
					$status = "success";
					$return	= array(
							'status'	=> $status,
							'message'	=> $msg,
							'deckGUID'	=> $deckGUID,
							'deckID'	=> $deckid
							);

					$this->returnJsonArray( $return );
				}
			} 
		}
	}

	public function fetchSavedDecksList(){
		$record_per_page = 50;
		//$startPosition = 0;

		$startPosition = isset( $this->request['sp'] ) ? (int)$this->request['sp'] : 0;
		
		if (!$this->memberData['member_id']){
			$error['status'] = 'error';
			$error['errorMessage'] = 'Unable to load decks. You are not logged in.';
			$this->returnJsonArray( $error );
		} else {
			$game = $this->request['game'];
			$onDatabase = 'ccs_custom_database_47';
			$this->DB->build( array( 'select' => '*', 'from' => $onDatabase, 'where' => "member_id ='{$this->memberData['member_id']}'",'order'	=> 'record_updated DESC','limit' => array( $startPosition, $record_per_page )) );
			$decks = $this->DB->execute();
			$num_rows = $this->DB->getTotalRows();	
			if ($num_rows == 0){
				$error['message'] = 'No Decks could be found to load.';
				$error['status'] = 'success';
				$error['count'] = $num_rows;
				$this->returnJsonArray( $error );
			} else {
				$stopDoubleEncodeNeedle = array("|sq|","[br]","|dq|");
				$stopDoubleEncodeReplace = array("&#39;", "<br />","&amp;quot;");
				while ( $row = $this->DB->fetch($decks) ){
					$objI = json_decode(str_ireplace($stopDoubleEncodeNeedle,$stopDoubleEncodeReplace,htmlspecialchars_decode($row['field_664'], ENT_QUOTES)));
						
					$return['status'] = "success";
					$return['count'] = $num_rows;
					$return['decks'][]	= array(
						'name'		=> $row['field_658'],
						'id'		=> $row['primary_id_field'],
						'guid'		=> $row['field_662'],
						'investigator' => $objI,
						'updated'	=> $this->registry->class_localization->getDate( $row['record_updated'], 'SHORT', 1 ),
						'saved'		=> $this->registry->class_localization->getDate( $row['record_saved'], 'SHORT', 1 ),
						'ISO8601'	=> date("c", $row['record_updated']) ,
						'offset'	=> $this->registry->class_localization->getTimeOffset(),
						'game'	=> $thisGame
						);

					
				}
				$this->returnJsonArray( $return );
			}
		}
	}

	public function fetchLoadDeck( )
	{
		if (!$this->memberData['member_id']){
			//The user is not logged in anymore...
			$error['status'] = 'error';
			$error['errorMessage'] = 'You are not logged in. You must be logged in to load a deck.';
			$this->returnJsonArray( $error );
		} else {
		   //The user is logged in
			$deckid = $this->DB->addSlashes($this->request['did']);
			$deckGUID = $this->DB->addSlashes($this->request['dguid']);
			if (preg_match("/^[0-9]+$/", $deckid) && preg_match("/^(ahdeck_)[0-9a-zA-Z]+$/", $deckGUID)) {
			
				$this->DB->build( array( 'select' => '*', 'from' => 'ccs_custom_database_47', 'where' => "member_id ='{$this->memberData['member_id']}' and primary_id_field ='{$deckid}' and field_662 ='{$deckGUID}'" ) );
				$decks = $this->DB->execute();
				$num_rows = $this->DB->getTotalRows();
				if ($num_rows == 0){
					$error['status'] = 'error';
					$error['errorMessage'] = 'Specified deck could not be found!';
					$this->returnJsonArray( $error );
				} else {
					$stopDoubleEncodeNeedle = array("|sq|","[br]","|dq|");
					$stopDoubleEncodeReplace = array("&#39;", "<br />","&amp;quot;");
					while ( $row = $this->DB->fetch($decks) ){
						$obj = json_decode(str_ireplace($stopDoubleEncodeNeedle,$stopDoubleEncodeReplace,htmlspecialchars_decode($row['field_663'], ENT_QUOTES)));
						$objI = json_decode(str_ireplace($stopDoubleEncodeNeedle,$stopDoubleEncodeReplace,htmlspecialchars_decode($row['field_664'], ENT_QUOTES)));
						$return['status'] = "success";
						$return['deck'][]	= array(
							'name'	=> $row['field_658'],
							'strategy'		=> IPSText::br2nl( $row['field_660'] ),
							'teaser'	=> IPSText::br2nl( $row['field_659']),
							'campaign'	=> $row['field_678'],
							'notes'	=> IPSText::br2nl( $row['field_679']),
							'physicaltrauma'	=> $row['field_680'],
							'mentaltrauma'	=> $row['field_681'],
							'unspentxp'	=> $row['field_682'],
							'tags'	=> $row['field_661'],
							'deckContents'	=> $obj,
							'deckInvestigator'	=> $objI,
							'deckGUID'	=> $row['field_662'],
							'deckid'		=> $row['primary_id_field']
							);

						$this->returnJsonArray( $return );
				 }
			  }
		   } else {	//  The ID or GUID was not of the proper format
				$error['status'] = 'error';
				$error['errorMessage'] = 'Specified deck could not be found!';
				$this->returnJsonArray( $error );
			}
		}
	}

	public function saveDeck(){
		if (!$this->memberData['member_id']){
			$error['status'] = 'error';
			$error['errorMessage'] = 'Unable to save deck. You are not logged in.';
			$this->returnJsonArray( $error );
		} else {
			$deckName = $this->request['dname'];
			$deckName = IPSText::convertUnicode( $this->convertAndMakeSafe( $deckName, 0 ), true );
			$deckName = IPSText::convertCharsets( $deckName, 'utf-8', IPS_DOC_CHAR_SET );
			$deckName = $this->scrubInput( $deckName, true);
			$deckName = $this->DB->addSlashes( $deckName );

			if (trim($deckName) == ""){
				$error['status'] = 'error';
				$error['errorMessage'] = 'You must name your deck to save it. Please add a name and try again.';
				$this->returnJsonArray( $error );	
			}

			$deckStrat = $this->request['dstrat'];
			$deckStrat = IPSText::convertUnicode( $this->convertAndMakeSafe( $deckStrat, 0 ), true );
			$deckStrat = IPSText::convertCharsets( $deckStrat, 'utf-8', IPS_DOC_CHAR_SET );
			$deckStrat = $this->scrubInput( $deckStrat, true);
			$deckStrat = $this->DB->addSlashes( $deckStrat );

			
			$deckTeaser = $this->request['dteaser'];
			$deckTeaser = IPSText::convertUnicode( $this->convertAndMakeSafe( $deckTeaser, 0 ), true ); //Normalize the character set of incoming AJAX data and optionally parse the value through the parseCleanValue routine
			$deckTeaser = IPSText::convertCharsets( $deckTeaser, 'utf-8', IPS_DOC_CHAR_SET ); //Make sure charset is utf-8
			$deckTeaser = $this->scrubInput( $deckTeaser, true); //scrub input using HTMLPurifier
			$deckTeaser = $this->DB->addSlashes( $deckTeaser ); //Just in case something was missed.
			
			
			$deckTags = $this->request['dtype'];
			$deckTags = IPSText::convertUnicode( $this->convertAndMakeSafe( $deckTags, 0 ), true );
			$deckTags = IPSText::convertCharsets( $deckTags, 'utf-8', IPS_DOC_CHAR_SET );
			$deckTags = $this->scrubInput( $deckTags,true);
			$deckTags = $this->DB->addSlashes( $deckTags );

			$deckCamp = $this->request['dcampaign'];
			$deckCamp = IPSText::convertUnicode( $this->convertAndMakeSafe( $deckCamp, 0 ), true );
			$deckCamp = IPSText::convertCharsets( $deckCamp, 'utf-8', IPS_DOC_CHAR_SET );
			$deckCamp = $this->scrubInput( $deckCamp, true);
			$deckCamp = $this->DB->addSlashes( $deckCamp );

			$deckNotes = $this->request['dnotes'];
			$deckNotes = IPSText::convertUnicode( $this->convertAndMakeSafe( $deckNotes, 0 ), true );
			$deckNotes = IPSText::convertCharsets( $deckNotes, 'utf-8', IPS_DOC_CHAR_SET );
			$deckNotes = $this->scrubInput( $deckNotes, true);
			$deckNotes = $this->DB->addSlashes( $deckNotes );

			$deckClass = $this->request['dclass'];
			switch( $deckClass )
			{
				case 'Guardian':
					$deckClass = 'Guardian';
					break;
				case 'Mystic':
					$deckClass = 'Mystic';
					break;
				case 'Rogue':
					$deckClass = 'Rogue';
					break;
				case 'Seeker':
					$deckClass = 'Seeker';
					break;
				case 'Survivor':
					$deckClass = 'Survivor';
					break;
				default:
					$error['status'] = 'error';
					$error['errorMessage'] = 'Invalid Class Detected.';
					$this->returnJsonArray( $error );
	    		break;
			}

			$deckPhysicalTrauma = $this->request['dphysicaltrauma'];
			$PhysicalTraumaValid =preg_match("/^[0-9]+$/", $deckPhysicalTrauma);
			if (!$PhysicalTraumaValid){
				$deckPhysicalTrauma = 0;
			}

			$deckMentalTrauma = $this->request['dmentaltrauma'];
			$MentalTraumaValid =preg_match("/^[0-9]+$/", $deckMentalTrauma);
			if (!$MentalTraumaValid){
				$deckMentalTrauma = 0;
			}

			$deckUnspentXP = $this->request['dunspentxp'];
			$UnspentXPValid =preg_match("/^[0-9]+$/", $deckUnspentXP);
			if (!$UnspentXPValid){
				$deckUnspentXP = 0;
			}

			$stopDoubleEncodeNeedle = array("&#39;","&amp;quot;");
			$stopDoubleEncodeReplace = array("|sq|","|dq|");

			$deckContent = $this->request['dcontent'];
			$deckContent = str_ireplace($stopDoubleEncodeNeedle,$stopDoubleEncodeReplace,$deckContent );
			$deckContent = IPSText::convertUnicode( $this->convertAndMakeSafe( $deckContent, 0 ), true );
			$deckContent = IPSText::convertCharsets( $deckContent, 'utf-8', IPS_DOC_CHAR_SET );
			$deckContent = $this->scrubInput( $deckContent, true);
			$deckContent = $this->DB->addSlashes( $deckContent );

			$deckInvestigator = $this->request['dinvestigator'];
			$deckInvestigator = str_ireplace($stopDoubleEncodeNeedle,$stopDoubleEncodeReplace,$deckInvestigator );
			$deckInvestigator = IPSText::convertUnicode( $this->convertAndMakeSafe( $deckInvestigator, 0 ), true );
			$deckInvestigator = IPSText::convertCharsets( $deckInvestigator, 'utf-8', IPS_DOC_CHAR_SET );
			$deckInvestigator = $this->scrubInput( $deckInvestigator, true);
			$deckInvestigator = $this->DB->addSlashes( $deckInvestigator );

			$this->DB->build( array( 'select' => 'max(record_updated) as lasttime', 'from' => 'ccs_custom_database_47', 'where' => 'member_id ="'.$this->memberData['member_id'].'"' ) );
		
			$flood = $this->DB->execute();
		
			while ( $row = $this->DB->fetch($flood) ){
				if ($row['lasttime']==null){
					$cansave= true;
				} else {
					if ($row['lasttime']+60 < time())
						$cansave= true;
					else
						$cansave= false;
				}
			}
			$insertTime = time();
			
			if ($cansave){
				
				if ($this->request['do'] == "save"){
					$this->DB->build( array( 'select' => 'count(*) as deckcount', 'from' => 'ccs_custom_database_47', 'where' => 'member_id ="'.$this->memberData['member_id'].'"' ) );
					$deckcount = $this->DB->execute();
					while ( $row = $this->DB->fetch($deckcount) ){
						$theCount = $row['deckcount'];
					}
					if ($theCount >50){
						$error['status'] = 'error';
						$error['errorMessage'] = 'You have exceeded your maximum number (50) of saved decks. Please delete some old decks to save new ones.';
						$this->returnJsonArray( $error );
					} else {
						$deckGUID = uniqid('ahdeck_');
						
						$this->DB->insert( 'ccs_custom_database_47', array( 'member_id'=> $this->memberData['member_id'],
						'record_saved'=> $insertTime,
						'record_updated'=> $insertTime,
						'post_key'=>md5( microtime() ),
						'record_approved'=>'1',
						'field_658'=>$deckName,
						'field_659'=>$deckTeaser,
						'field_660'=>$deckStrat,
						'field_661'=>$deckTags,
						'field_662'=>$deckGUID,
						'field_678'=>$deckCamp,
						'field_679'=>$deckNotes,
						'field_680'=>$deckPhysicalTrauma,
						'field_681'=>$deckMentalTrauma,
						'field_682'=>$deckUnspentXP,
						'field_664'=>$deckInvestigator,
						'field_676'=>$deckClass,
						'field_663'=>$deckContent));
						$rid = $this->DB->getInsertId();
						$msg = "Deck Saved.";
						$status = "success";

						$return	= array(
								'status'	=> $status,
								'message'		=> $msg,
								'deckName'	=> $deckName,
								'deckID'	=> $rid,
								'deckUpdate'	=> $this->registry->class_localization->getDate( $insertTime, 'SHORT', 1 ),
								'deckGUID'		=> $deckGUID
								);

						$this->returnJsonArray( $return );
					}
					
				} else if ($this->request['do'] == "update"){
					$deckGUID = $this->request['dGUID'];
					$deckGUIDValid =preg_match("/^(ahdeck_)[0-9a-zA-Z]+$/", $deckGUID);
					$primaryKey = $this->request['dPK'];

					if (is_numeric($primaryKey)){
						if ((int)$primaryKey==$primaryKey){
							$pkeyValid = true;
						} else {
							$pkeyValid = false;
						}
					} else {
						$pkeyValid = false;
					}
					if (!$pkeyValid || !$deckGUIDValid){
						$error['status'] = 'error';
						$error['errorMessage'] = 'Unable to update deck.';
						$this->returnJsonArray( $error );	
					} else {
						$this->DB->update( 'ccs_custom_database_47', array( 'record_updated'	=> $insertTime,
							'field_658'=>$deckName,
							'field_659'=>$deckTeaser,
							'field_660'=>$deckStrat,
							'field_661'=>$deckTags,
							'field_678'=>$deckCamp,
							'field_679'=>$deckNotes,
							'field_680'=>$deckPhysicalTrauma,
							'field_681'=>$deckMentalTrauma,
							'field_682'=>$deckUnspentXP,
							'field_664'=>$deckInvestigator,
							'field_676'=>$deckClass,
							'field_663'=>$deckContent),
							'primary_id_field=\''.$primaryKey.'\' and member_id = \''.$this->memberData['member_id'].'\''
						
						);
						$decksUpdated = $this->DB->getAffectedRows();
						$msg = "Deck Saved.";
						if ($decksUpdated ==0) {
							$error['status'] = 'error';
							$error['errorMessage'] = 'Unable to update your deck.';
							$this->returnJsonArray( $error );	
						} else {
							
							
							$msg = $decksUpdated . " Deck Updated.";
							$status = "success";
							$return	= array(
								'status'	=> $status,
								'message'		=> $msg,
								'deckName'	=> $deckName,
								'deckID'	=> $primaryKey,
								'deckUpdate'	=> $this->registry->class_localization->getDate( $insertTime, 'SHORT', 1 ),
								'deckGUID'		=> $deckGUID
								);
	
							$this->returnJsonArray( $return );
						}
					}
				}
				
			} else {
				$error['status'] = 'error';
				$error['errorMessage'] = 'You may only save decks once every minute. Please try again shortly.';
				$this->returnJsonArray( $error );
			}
		}
	}
	
	/**
	 * function that exports the cards in Json format
	 *
	 * @access	public
	 * @return	JSON		
	 */
	public function exportCards(){
		$fullTextSearch = $this->request['fTS'];
		$tpage = $this->request['fPage'];
		$combinedWhere = " record_approved = 1 ";
		$orderByField = "primary_id_field";
		$tstart_record = $this->request['start'];
		if (!is_numeric($tstart_record)){
			$tstart_record = 0;
		}
		$record_per_page = 300;

		$this->DB->build( array( 'select' => '*', 'from' => 'ccs_custom_database_46', 'where' => $combinedWhere, 'order'	=> $orderByField,'limit' => array( $tstart_record, $record_per_page ),) );
		$rslt = $this->DB->execute();
		$recordCount = 0;

		while ( $row = $this->DB->fetch($rslt) ){
			$recordCount++;


			if ($row['field_619']==''||$row['field_619']=='X'){
				$baseCost = 0;
			} else {
				$baseCost = $row['field_619']*1;
			}


			$category = $this->registry->ccsFunctions->getCategoriesClass( $this->caches['ccs_databases'][ 46 ] )->getCategory( $row['category_id'] );
			$fullURL = $this->registry->ccsFunctions->returnDatabaseUrl( 46, 0,$row['primary_id_field']);

			$gameText = IPSText::getTextClass( 'bbcode' )->preDisplayParse( $row['field_640'] );
			$gameTextBack = IPSText::getTextClass( 'bbcode' )->preDisplayParse( $row['field_653'] );
			
			$gameText = str_ireplace("<BR>","<br />",$gameText);
			$gameText = preg_replace('/<SPAN\s*style=.font-weight: bold.\s*>(.*?)<\\/SPAN>/i',"<strong class='bbc'>$1</strong>",$gameText);
			$gameText = preg_replace('/<SPAN\s*style=.font-style:\s*italic.\s*>(.*?)<\\/SPAN>/i',"<em class='bbc'>$1</em>",$gameText);
			$gameText = preg_replace('/<SPAN\s*style=.font-weight: bold;font-style:italic.\s*>(.*?)<\\/SPAN>/i',"<em class='bbc'><strong class='bbc'>$1</strong></em>",$gameText);
			$gameText = preg_replace('/<SPAN\s*style=..\s*>(.*?)<\\/SPAN>/i',"$1",$gameText);

			$gameTextBack = str_ireplace("<BR>","<br />",$gameTextBack);
			$gameTextBack = preg_replace('/<SPAN\s*style=.font-weight: bold.\s*>(.*?)<\\/SPAN>/i',"<strong class='bbc'>$1</strong>",$gameTextBack);
			$gameTextBack = preg_replace('/<SPAN\s*style=.font-style:\s*italic.\s*>(.*?)<\\/SPAN>/i',"<em class='bbc'>$1</em>",$gameTextBack);
			$gameTextBack = preg_replace('/<SPAN\s*style=.font-weight: bold;font-style:italic.\s*>(.*?)<\\/SPAN>/i',"<em class='bbc'><strong class='bbc'>$1</strong></em>",$gameTextBack);
			$gameTextBack = preg_replace('/<SPAN\s*style=..\s*>(.*?)<\\/SPAN>/i',"$1",$gameTextBack);

			$cardName = str_ireplace('"','&quot;',$row['field_605'] );
			$cardName = str_ireplace("'",'&#39;',$cardName );

			$show_in_deckbuilder = "N";
			if ($row['field_614'] == 'P') {
				$show_in_deckbuilder = "Y";
			}
			$max_in_deck = 0;
			if ($row['field_644'] == "" && ($row['field_614'] == 'P' || $row['field_657'] == "Basic Weakness" || $row['field_657'] == "Weakness")){ // if (max_in_deck == "" && deck == Player)
				if ($row['field_654'] == "" && $row['field_610'] != "Investigator"){ //if card required for == "" and card_type == Investigator
					$max_in_deck = 2;
				} else {
					$max_in_deck = 1;
				}
			} else {
				$max_in_deck = $row['field_644']*1;
			}


			$card[] = array(
							'name'	=> $cardName,
							'label'	=> $cardName,
							'fullurl'	=> $fullURL,
							'furl'	=>	$row['record_static_furl'],
							'id'	=> $row['primary_id_field'],
							'imgf'	=>	$row['field_606'],
							'imgb'	=>	$row['field_607'],
							'unique'	=>	$row['field_608'],
							'quantity'	=>	$row['field_609'],
							'type'	=>	$row['field_610'],
							'num'	=>	substr("000".$row['field_611'],-3),
							'illus'	=>	$row['field_613'],
							'deck'	=>	$row['field_614'],
							'encounter'	=>	$row['field_615'],
							'enum'	=>	$row['field_616'],
							'subtitle'	=>	$row['field_617'],
							'traits'	=>	$row['field_618'],
							'cost'	=>	$row['field_619'],
							'costnumeric'	=>	$baseCost,
							'lvl'	=>	$row['field_620'],
							'clss'	=>	$row['field_621'],
							
							'will'	=>	$row['field_622'],
							'int'	=>	$row['field_623'],
							'cmbt'	=>	$row['field_624'],
							'agi'	=>	$row['field_625'],
							'wild'	=>	$row['field_626'],
							'hlth'	=>	$row['field_627'],
							'snty'	=>	$row['field_628'],
							'slot'	=>	$row['field_651'],
							
							'fght'	=>	$row['field_629'],
							'evade'	=>	$row['field_630'],
							'dmg'	=>	$row['field_631'],
							'horr'	=>	$row['field_632'],
							'shrd'	=>	$row['field_633'],
							'clue'	=>	$row['field_634'],
							'connt'	=>	$row['field_635'],
							'seq'	=>	$row['field_636'],
							'cluet'	=>	$row['field_637'],
							'doomt'	=>	$row['field_638'],
							'vctry'	=>	$row['field_639'],
							'text'	=>	$gameText,
							'textb'	=>	$gameTextBack,
							'ffgid'	=>	$row['field_642'],
							
							'showindb'	=>	$show_in_deckbuilder,
							
							'max' => $max_in_deck,
							'dsize' => $row['field_645'],
							'dopt' => $row['field_646'],
							'dreq' => $row['field_647'],
							'dres' => $row['field_648'],
							'perm' => $row['field_649'],
							'excp' => $row['field_650'],
							'ctotot' => $row['field_652'],
							'creqf'	=> $row['field_654'],
							'band'	=> $row['field_656'],
							'rest'	=> $row['field_655'],
							'wtype'	=> $row['field_657'],
							'setid' => $row['category_id'],
							'setname' => $category['category_name']

							);

		}/**/
		$cards['search'] = $fullSearchPage;

		$cards['count'] = $recordCount;
		$cards['cards'] = $card;
		$this->returnJsonArray( $cards );
	}
}
 ?>