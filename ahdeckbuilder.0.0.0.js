//This creates the ability to declare a vertical button set for use in the and/or button on the icon search
(function( $ ){
  //plugin buttonset vertical
  $.fn.buttonsetv = function() {
    return this.each(function(){
      $(this).buttonset();
      $(this).css({'display': 'table', 'margin-bottom': '7px'});
      $('.ui-button', this).css({'margin': '0px', 'display': 'table-cell'}).each(function(index) {
              if (! $(this).parent().is("div.dummy-row")) {
                  $(this).wrap('<div class="dummy-row" style="display:table-row; " />');
              }
          });
      $('.ui-button:first', this).first().removeClass('ui-corner-left').addClass('ui-corner-top');
      $('.ui-button:last', this).last().removeClass('ui-corner-right').addClass('ui-corner-bottom');
    });
  };
})( jQuery );

//Since IPB 3.X is running prototype, and uses an older jQuery in some block locations have to noconflict newer jQuery
var jQuery_1_9_1 = jQuery.noConflict(true);

//holds everything associated with a deck
var deckObject = new deckContents();
function deckContents(){
	this.saved = false;
	this.deckid = 0;
	this.deckguid = '';
	this.deckname = '';
	this.deckteaser = '';
	this.deckstrategy = '';
	this.deckcampaign = '';
	this.decknotes = '';
	this.deckphysicaltrauma = '';
	this.deckmentaltrauma = '';
	this.deckunspentxp = '';
	this.decktags = '';
	this.cards = [];
	this.faction = [];
	this.restrictedcards = {};
	this.totalCards = 0;
	this.game = 'arkhamhorror';
	this.abbreviation = "ah";
	this.ally = '';
	this.startingHandSize = 5;
	this.arkhamhorror= {};
	this.arkhamhorror.counts = {investigator:0, asset:0, skill:0, event:0, level:0, weakness:0, guardian:0, mystic:0, rogue:0, seeker:0, survivor:0, neutral:0, willpower:0, intellect:0, combat:0, agility:0, wild:0};
	this.arkhamhorror.totals = {cost:0,strength:0,loyalty:0};
	this.arkhamhorror.traitstotrack = {};
	this.arkhamhorror.costs = [];
	this.arkhamhorror.restrictedcards = [];
	this.arkhamhorror.factionColors = {"Guardian":'#C4601E',"Mystic":'#1E8EC4',"Rogue": '#519215',"Seeker": '#a5a49a',"Survivor":'#9221BE', "Neutral":'#CEAB1F'};

	this.arkhamhorror.classList = ['Guardian','Mystic','Rogue','Seeker','Survivor','Neutral'];
	this.arkhamhorror.typeList = ["Investigator","Asset","Event","Skill","Weakness"];
	this.arkhamhorror.series = ['costs'];
}

var ahGlobals = new ahDeckbuilderGlobals();
function ahDeckbuilderGlobals (){
	//for IE8 and lower fallback resizing
	this.widthTimer = null;
	//some constants
	this.whichGame = 'arkhamhorror';
	this.gameAbbreviation = "ah";
	this.deckbuilderURL = 'http://www.cardgamedb.com/index.php/arkhamhorror/arkham-horror-the-card-game-deckbuilder';
	this.deckbuilderTitle = 'CardGameDB.com Arkham Horror Deckbuilder';
	this.deckSharePage = 'http://www.cardgamedb.com/index.php/arkhamhorror/arkham-horror-deck-share';
	this.imagepath = "http://lcg-cdn.fantasyflightgames.com/ahlcg";
	//true if ajax in progress. Prevents spamming of server.
	this.ajaxSearchInProgress = false;
	this.ajaxDeckManipulationInProgress = false;
	this.selectedsets = "";
	this.deckviewType = "";
	this.searchviewType = "";
	this.interfaceViewType = "";
	this.currentlyDisplayedInterfaceChoice = "";
	this.thetabs;
	this.db;
	this.previousText = ""; 
	this.previousName = "";
	this.previousTrait = "";
	this.loadedLocalSave = "";
	this.deckbuildingSearchRules;
	this.deckbuildingSearchRestrictions;
	this.deckbuildingMaxCards;
	this.results = [];
	this.autocomplete = [];
	this.filters;
	this.filters1;
	this.deckname = "";
	this.buttonType = "";
	this.mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
	this.currentlyDisplayedCard = "";
	this.typelist = ["Investigator","Weakness","Asset","Event","Skill"];
	this.classes =  ['Guardian','Mystic','Rogue','Seeker','Survivor','Neutral'];
	this.cardArrayCopy = [];

	this.handimagewidth = 100;
	this.autoupdategraphs = true;
	this.showloyaltoggle = false;
	this.termTemplate = "<span class='ui-autocomplete-term'>%s</span>";
}


jQuery_1_9_1(document).ready(function($) {
	$(".removeindeckbuilder").remove(); //Get rid of any fields or options that don't belong in deckbuilder
	$('#navigationToggle').addClass("expand").hide();	//removes the left nav links and switches the toggle button
	$('#admin_bar').hide();
	$('#left_col').addClass("noSidebar");
	$("#filterType").attr('multiple','multiple'); //changes type filter to be multiple 
	$("#filterType").attr('data-placeholder',"Select a Type"); //adds a placeholder for select2
	$("#filterType").css("width",'223').parent("div.searchTextContent").addClass("setDiv");
	$("#filterType option[value='Any']").remove();
	$("#filterType option[value='Faction']").remove();
	$("#filterType option[value='Title']").remove();
	$("#searchTypeLabel,#searchTypeSelect").removeClass().addClass("search-full");
	$("#searchTypeSelect").css("margin-bottom","10px");
	$("#weaknessSets").attr('multiple','multiple').attr('data-placeholder',"Select a Set");
	
	//sets button type
	if( ahGlobals.mobile) {
		ahGlobals.buttonType = "button-touch";
	} else {
		ahGlobals.buttonType = "button-small";
	}

	ahGlobals.db = TAFFY(cards);
	
	var maxCost = ahGlobals.db().max("costnumeric");
	var setList = ahGlobals.db({wtype:'Basic Weakness'}).distinct("setid","setname");
	$.each(setList, function (k,v){
		$("#weaknessSets").append("<option value='"+v[0]+"'>"+v[1]+"</option>");
	});

	var investigators = $.extend(true, [], ahGlobals.db({type:'Investigator'}).order('name').get());
	var basicWeaknesses =  $.extend(true, [], ahGlobals.db({wtype:'Basic Weakness'}).order('name').get());
	//inserts investigator selections
	$.each(investigators, function (k, v){ 
		$("#investigatorList").append(deckbuilderTemplate(v,'investigatorList'));
	});
	$.each(basicWeaknesses, function (k, v){ 
		$("#weaknessResults").append(deckbuilderTemplate(v,'search'));
	});

	//lazyloads the investigator images
	$("#investigatorList img.lazy").lazyload({         
		container: $("#investigatorDiv"),
		skip_invisible : false
	});

	//lazyloads the investigator images
	$("#weaknessResults img.lazy").lazyload({         
		container: $("#weaknessResults"),
		skip_invisible : false
	});

	//sets up cost headers for cost deckview
	for (var i=0;i<=maxCost;i++){
		$("#deckSectionCost").append('<ul id="deckCardCostUL'+i+'" style="display:none" class="deckCards"><li id="deckCardCost'+i+'" class="deckCardCostHeader"><strong>Cost '+i+' (<span id="deckCardCost'+i+'Count">0</span>)</strong></li></ul>');
	}

	$("#searchUL,#deckContainer").on("contextmenu", "td.tdCardName,td.cardImg", function(event){
		event.preventDefault();
		var myli = $(this).closest('li.cardOption'),
		mycard = myli.data("cardobject");
		//console.log(mycard.fullurl);
		window.open(mycard.fullurl,'_blank');
		return (false);
	});

	$("#addWeakness").click(function(){
		$( "#weaknessHUD" ).dialog("open");
	});

	$("#investigatorList").on("click", "button.view-investigator", function(){
		var $cardid = $(this).data("cardid").toString(),
		$investigatorName = $(this).data("investigator"),
		$set = $(this).data("set"),
		chosenInvestigator,
		showcard=[],
		counter;

		chosenInvestigator = $investigatorName + ' (' + $set + ')';
		chosenInvestigator = chosenInvestigator.replaceAll('&', '&amp;')
					            .replaceAll('"', '&quot;')
					            .replaceAll("'", '&#39;')
					            .replaceAll('<', '&lt;')
					            .replaceAll('>', '&gt;');
		var records = ahGlobals.db([{creqf:chosenInvestigator},{id:$cardid}]).order('num').get();
		for (counter=0;counter<records.length;counter++){
			if (records[counter].type == "Investigator"){
				showcard.push("<div style='width:390px;float:left;padding:5px;'>"+cardViewTemplate(records,records[counter].id)+"</div>");
			} else {
				showcard.push("<div style='width:300px;float:left;padding:5px;'>"+cardViewTemplate(records,records[counter].id)+"</div>");
			}
			
		}

		$( "#investigatorHUD" ).html(showcard.join(" ")+"<div style='clear:both'>&nbsp;</div>");
		$( "#investigatorHUD" ).dialog("open");
		
	});

	//adds click listner adding an investigator to the ddeck
	$("#investigatorList").on("click", "button.button-investigator", function(){
		var $cardid = $(this).data("cardid").toString(),
		$investigatorName = $(this).data("investigator"),
		$set = $(this).data("set"),
		$deckOptions = $(this).data("deckoptions"),
		$deckRequirements = $(this).data("deckrequirements"),
		$deckRestrictions = $(this).data("deckrestrictions"),
		$deckMaxCards = $(this).data("maxdecksize"),
		rules,
		resultingAction =[],
		chosenInvestigator,
		currentFaction = deckObject.faction,
		currentCards;

		chosenInvestigator = $investigatorName + ' (' + $set + ')';
		chosenInvestigator = chosenInvestigator.replaceAll('&', '&amp;')
					            .replaceAll('"', '&quot;')
					            .replaceAll("'", '&#39;')
					            .replaceAll('<', '&lt;')
					            .replaceAll('>', '&gt;');
		ahGlobals.deckbuildingMaxCards = $deckMaxCards;
		$("#maxDeckCards").text(ahGlobals.deckbuildingMaxCards);
		var records = ahGlobals.db([{creqf:chosenInvestigator},{id:$cardid}]).order('name').get();
		//console.log(records);
		$.each(records,function(k,v){
			resultingAction.push(adjustDeckObject ('', parseInt(v.quantity), v));
			
			
		});
		//console.log(resultingAction);
		deckbuildingSearchRulesParse($deckOptions, $deckRestrictions);
		adjustDeckHTML(resultingAction);
		
		filterCards(true);
		toggleSearchView('search');
		sampleHand();
		populateCharts(deckObject,false);
		adjustStatsHTML();
		deckObject.saved = false;
		$('#spanDeckSavedStatus').text("Deck Not Saved").removeClass('decksavegreen').addClass('decksavered');

	});

	function deckbuildingSearchRulesParse(options, restrictions){
		var optionSplit,
		subSplit,
		quantityRange,
		levelRange,
		classOrTrait,
		itemSplit,
		buildrule = [],
		buildrestriction = {},
		output = [],
		combinedrules = [],
		indexOfClass,
		anyOtherPresent = false,
		anyOther = {},
		specificClasses =$.extend(true, [], ahGlobals.classes);

		if (options != "") {
			optionSplit = options.split("|");
			for (o=0;o<optionSplit.length;o++){

				subSplit = optionSplit[o].split(";");
				quantityRange = subSplit[0].split("-");
				levelRange  = subSplit[2].split("-");
				classOrTrait = subSplit[1].split("-");
				if (classOrTrait[0] == 'class'){
					indexOfClass = specificClasses.indexOf(classOrTrait[1]);
					if (indexOfClass !== -1){
						buildrule[o] = {};
						buildrule[o].clss = classOrTrait[1];
						buildrule[o].lvl = {};
						buildrule[o].lvl['>='] = levelRange[0];
						buildrule[o].lvl['<='] = levelRange[1];
						specificClasses.splice(indexOfClass, 1);
					} else if (classOrTrait[1] == "ANY OTHER"){ 
						anyOtherPresent = true;
						anyOther.lvl = {};
						anyOther.lvl['>='] = levelRange[0];
						anyOther.lvl['<='] = levelRange[1];
					}
				} else if (classOrTrait[0] == 'trait'){
					buildrule[o] = {};
					buildrule[o].traits = {};
					buildrule[o].traits['likenocase'] = classOrTrait[1];
					buildrule[o].lvl = {};
					buildrule[o].lvl['>='] = levelRange[0];
					buildrule[o].lvl['<='] = levelRange[1];
					deckObject[ahGlobals.whichGame].traitstotrack[classOrTrait[1]] = 0;
				}
				

			}
		}
		if (anyOtherPresent){
			anyOther.clss = specificClasses;
			buildrule.push(anyOther);
		}
		//console.log(buildrule);
		ahGlobals.deckbuildingSearchRules = buildrule;

		if (restrictions != "") {

				subSplit = restrictions.split(";");
				quantityRange = subSplit[0];
				levelRange  = subSplit[2].split("-");
				classOrTrait = subSplit[1].split("-");
				if (classOrTrait[0] == 'class'){
					buildrestriction.clss['!is'] = classOrTrait[1];
					buildrestriction.lvl = {};
					buildrestriction.lvl['>='] = levelRange[0];
					buildrestriction.lvl['<='] = levelRange[1];
				} else if (classOrTrait[0] == 'trait'){

					buildrestriction.traits = {};
					buildrestriction.traits['!likenocase'] = classOrTrait[1];
					buildrestriction.lvl = {};
					buildrestriction.lvl['>='] = levelRange[0];
					buildrestriction.lvl['<='] = levelRange[1];
				}

		}
		//console.log (buildrestriction);
		ahGlobals.deckbuildingSearchRestrictions = buildrestriction;
		deckHTMLSetup();
	}

	function deckHTMLSetup(){
		$("#deckviewclasses>div").hide();
		$.each(ahGlobals.deckbuildingSearchRules, function(k,v){
			if (typeof v.clss != 'undefined'){
				if ($.isArray(v.clss)){
					$("#deckviewclasses>div").show();
				} else {
					$("#deckviewclasses>div."+v.clss).show();
				}
				
			}
			if (typeof v.traits != 'undefined'){
				//console.log(v.traits['likenocase']);
				$("#deckviewtraits").text(v.traits['likenocase']+": 0");
				$("#decktraitscount").text("0");
			}
		});
	}

	function setAutoComplete (_autocompleteSource){

		$('#quickentry').autocomplete({
		      source: _autocompleteSource,
		      autoFocus: true,
		      select: function( event, ui ) {
		      	var quickquantity = parseInt($('#quickquantity').val());
		      	var quickAction = [];
				quickAction.push(adjustDeckObject ('', quickquantity, ui.item));

				adjustDeckHTML(quickAction);
				deckObject.saved = false;
				$('#spanDeckSavedStatus').text("Deck Not Saved").removeClass('decksavegreen').addClass('decksavered');
				$('#quickquantity').focus().select();
				$(this).val("");
				return false;
		      }
		    })
			.data('ui-autocomplete')._renderItem = function (ul, item) {
			    return $('<li>')
			        .data("item.autocomplete", item)
			        .append("<a>" + item.label + " [" + item.lvl + "]</a>")
			        .appendTo(ul);
			};
	}

	$("#addRandomWeakness").click(function(){
		var weaknessSets = $("#weaknessSets").val(),
		output,
		resultingAction = [],
		randomelement = 0;
		if (weaknessSets){
			output =$.extend(true, [], ahGlobals.db({wtype:{"is":"Basic Weakness"}},{setid:weaknessSets}).order("name aesc").get());
			randomelement = Math.floor(Math.random() * output.length);
			resultingAction.push(adjustDeckObject ('', parseInt(output[randomelement].quantity), output[randomelement]));
			adjustDeckHTML(resultingAction);
		}
		
		//console.log(weaknessSets);
	});

	$("#refreshSampleHand").click(function(){sampleHand();});
	
	//setup of 'loading' screen covering used when on a mobile device
	$( "#loadingmessage" ).dialog({ 
	    autoOpen: false,
	    modal: true,
		closeOnEscape: false,
		dialogClass: 'loadingwait',
		resizable: false,
		draggable: false,
	    height: "auto",
	    show: {effect:"fade",duration:"fast"}
	});

	$( "#settingsdialog" ).dialog({ 
	    autoOpen: false,
	    modal: true,
		closeOnEscape: true,
		resizable: false,
		draggable: false,
	    height: "auto",
	    width: "90%",
	    show: {effect:"fade",duration:"fast"},
	    buttons: {
	        Ok: function() {
	          $( this ).dialog( "close" );
	        }
	    }
	});

	$( "#messagedialog" ).dialog({ 
	    autoOpen: false,
	    modal: true,
		closeOnEscape: true,
		resizable: false,
		draggable: false,
	    height: "auto",
	    show: {effect:"fade",duration:"fast"},
	    buttons: {
	        Ok: function() {
	          $( this ).dialog( "close" );
	        }
	    }
	});
	$( "#investigatorHUD" ).dialog({ 
	    autoOpen: false,
	    modal: true,
		closeOnEscape: true,
		resizable: false,
		draggable: false,
	    height: "auto",
	    width: "80%",
	    show: {effect:"fade",duration:"fast"}
	});

	$( "#weaknessHUD" ).dialog({ 
	    autoOpen: false,
	    modal: true,
		closeOnEscape: true,
		resizable: false,
		draggable: false,
	    height: "auto",
	    width: "80%",
	    show: {effect:"fade",duration:"fast"},
	    open: function(event, ui) {
	     $('#weaknessResults').trigger("scroll");
		},
	    buttons: {
	        Ok: function() {
	          $( this ).dialog( "close" );
	        }
	    }
	});
	
	$('#onoffgraph').on('change',function(){
		docCookies.setItem("AHgraphAutoUpdate", $(this).is(':checked'), Infinity);
		ahGlobals.autoupdategraphs = $(this).is(':checked');
		if (!ahGlobals.autoupdategraphs){
			$('#refreshCharts').show();
		} else {
			$('#refreshCharts').hide();
		}
	});

	if (docCookies.hasItem("AHgraphAutoUpdate")){
		if (docCookies.getItem("AHgraphAutoUpdate")=="false"){
			ahGlobals.autoupdategraphs = false;
			$('#refreshCharts').show();
			$("#onoffgraph").attr('checked', ahGlobals.autoupdategraphs);
		} else if (docCookies.getItem("AHgraphAutoUpdate")=="true"){
			ahGlobals.autoupdategraphs = true;
			$('#refreshCharts').hide();
			$("#onoffgraph").attr('checked', ahGlobals.autoupdategraphs);
		}
		
	} else if (ahGlobals.mobile){
		ahGlobals.autoupdategraphs = false;
		$('#refreshCharts').show();
		$("#onoffgraph").attr('checked', ahGlobals.autoupdategraphs);
	}

	$('#refreshCharts').button().click(function(){
		populateCharts(deckObject,false);
			
	});

	//adds click listner for search results to view card.

	$("#searchUL,#deckContainer,#investigatorList").on("click", "td.tdCardName,td.cardImg,div.cardImg,div.investigatorInfo", function(event,ui){
		var myli = $(this).closest('li.cardOption'),
		myul = $(this).closest('ul'),
		cardID = String(myli.data("cardid")),
		record,
		myposition,
		atposition,
		ofelement,
		showcard;
		$('.cardOptionSelected').removeClass('cardOptionSelected');
		myli.addClass('cardOptionSelected');
		if (ahGlobals.interfaceViewType != "202" && ahGlobals.interfaceViewType != "2T2"){
			if (cardID != ahGlobals.currentlyDisplayedCard){
				record = ahGlobals.db({id:cardID}).order('name').get();
				ahGlobals.currentlyDisplayedSquad = '';
				$('#viewDiv').empty().append(cardViewTemplate(record,cardID));
			}
			ahGlobals.currentlyDisplayedCard = cardID;

		} else if (ahGlobals.interfaceViewType == "2T2"){
			record = ahGlobals.db({id:cardID}).order('name').get();
			if (myul.attr('id')=="searchUL"){
				atposition = "left top";
				mypoisition = "left top";
				ofelement = $("#filterDiv");
			} else {
				mypoisition = "right top";
				atposition = "right top";
				ofelement = $("#deckContainer");
			}
			showcard = cardViewTemplate(record,cardID,"hover");
			$("#cardHUD").html(showcard).show().position({
					collision: "none",
					my: mypoisition,
					at: atposition,
					of: ofelement
				  }).one('click',function(){$("#cardHUD").hide();});
			$("#cardHUD>div>img.lazy").lazyload({
					skip_invisible : false,
					effect : "fadeIn"
				}).trigger('scroll');
		}
	});

	$( "#searchUL,#deckContainer,#investigatorList" ).on("mouseenter mouseleave","td.tdCardName,td.cardImg,div.cardImg,div.investigatorInfo", function(event){
		var showcard,
		myli,
		myul,
		myposition,
		atposition,
		ofelement,
		record;
		if (ahGlobals.interfaceViewType == "202"){
			if (event.type === "mouseenter"){
				myli = $(this).closest('li.cardOption');
				myul = $(this).closest('ul');
				//console.log(myul.attr('id'));
				if (myul.attr('id')=="searchUL" || myul.attr('id')=="investigatorList" ){
					myposition = "left top";
					atposition = "left top";
					ofelement = $("#deckContainer");
				} else {
					/*if (ahGlobals.mobile){
						atposition = "left top";
					} else {
						
						atposition = "right top";
						
					}*/
					atposition = "right top";
					myposition = "right top";
					ofelement = $("#filterDiv");
				}
				cardID = String(myli.data("cardid"));
				record = ahGlobals.db({id:cardID}).order('name').get();
				showcard = cardViewTemplate(record,cardID,"hover");
				//console.log(myposition+"|"+atposition+"|"+ofelement)
				$("#cardHUD").html(showcard).show().position({
					collision: "none",
					my: myposition,
					at: atposition,
					of: ofelement
				  });
				$("#cardHUD>div>img.lazy").lazyload({
					skip_invisible : false,
					effect : "fadeIn"
				}).trigger('scroll');
			}
			if (event.type === "mouseleave"){
				//$( this ).tooltip( "destroy" );
				$("#cardHUD").hide();
			}
		}
	});

	$("#themeDefault").click(function(){
		changeDeckbuilderTheme ("default");
	});
	$("#theme1").click(function(){
		changeDeckbuilderTheme ("flat");
	});
	$("#theme3").click(function(){
		changeDeckbuilderTheme ("theme3","thewall");
	});
	$("#theme4").click(function(){
		changeDeckbuilderTheme ("theme4","rhaegal");
	});
	$("#theme5").click(function(){
		changeDeckbuilderTheme ("theme5","betrayal");
	});
	$("#theme6").click(function(){
		changeDeckbuilderTheme ("theme6","winterfell");
	});
	$("#theme7").click(function(){
		changeDeckbuilderTheme ("theme7","knights");
	});

	if (docCookies.hasItem("AHDeckbuilderTheme")){
		var deckbuilderTheme  = docCookies.getItem("AHDeckbuilderTheme");
		changeDeckbuilderTheme(deckbuilderTheme);
	}

	function changeDeckbuilderTheme (theme,background){
		switch (theme){
			case "flat":
				$("#deckbuilderWrapper").addClass("flat");
				$("#ipboard_body").removeClass('theme3').addClass('ipboard_bodyflat');
				$("#deckbuilderjqueryuicss").attr({href : 'http://www.cardgamedb.com/deckbuilders/css/jquery-ui-1.10.4.flat.css'});
				$("#deckbuilderselect2css").attr({href : 'http://www.cardgamedb.com/deckbuilders/css/select2flat.css'});
				$("#logo>a>img").attr({src : 'http://www.cardgamedb.com/deckbuilders/images/logo-small.png'});
				docCookies.setItem("AHDeckbuilderTheme", "flat", Infinity);
				break;
			case "theme3":
				$("#deckbuilderWrapper").removeClass("flat");
				$("#ipboard_body").removeClass().addClass("theme3 themeBackgroundForthegreatergood");
				$("#deckbuilderjqueryuicss").attr({href : 'http://www.cardgamedb.com/deckbuilders/css/jquery-ui1.10.2.css'});
				$("#deckbuilderselect2css").attr({href : 'http://www.cardgamedb.com/deckbuilders/css/select2.css'});
				//$("#logo>a>img").attr({src : 'http://www.cardgamedb.com/forums/public/style_images/gravity_blue/logo.png'});
				$("#logo>a>img").attr({src : 'http://www.cardgamedb.com/deckbuilders/images/logo-small.png'});
				docCookies.setItem("AHDeckbuilderTheme", "theme3", Infinity);
				break;
			case "theme4":
				$("#deckbuilderWrapper").removeClass("flat");
				$("#ipboard_body").removeClass().addClass("theme3 themeBackgroundHyperawareness");
				$("#deckbuilderjqueryuicss").attr({href : 'http://www.cardgamedb.com/deckbuilders/css/jquery-ui1.10.2.css'});
				$("#deckbuilderselect2css").attr({href : 'http://www.cardgamedb.com/deckbuilders/css/select2.css'});
				$("#logo>a>img").attr({src : 'http://www.cardgamedb.com/deckbuilders/images/logo-small.png'});
				docCookies.setItem("AHDeckbuilderTheme", "theme4", Infinity);
				break;
			case "theme5":
				$("#deckbuilderWrapper").removeClass("flat");
				$("#ipboard_body").removeClass().addClass("theme3 themeBackgroundGhoulpriest");
				$("#deckbuilderjqueryuicss").attr({href : 'http://www.cardgamedb.com/deckbuilders/css/jquery-ui1.10.2.css'});
				$("#deckbuilderselect2css").attr({href : 'http://www.cardgamedb.com/deckbuilders/css/select2.css'});
				$("#logo>a>img").attr({src : 'http://www.cardgamedb.com/deckbuilders/images/logo-small.png'});
				docCookies.setItem("AHDeckbuilderTheme", "theme5", Infinity);
				break;
			case "theme6":
				$("#deckbuilderWrapper").removeClass("flat");
				$("#ipboard_body").removeClass().addClass("theme3 themeBackgroundwinterfell");
				$("#deckbuilderjqueryuicss").attr({href : 'http://www.cardgamedb.com/deckbuilders/css/jquery-ui1.10.2.css'});
				$("#deckbuilderselect2css").attr({href : 'http://www.cardgamedb.com/deckbuilders/css/select2.css'});
				$("#logo>a>img").attr({src : 'http://www.cardgamedb.com/deckbuilders/images/logo-small.png'});
				docCookies.setItem("AHDeckbuilderTheme", "theme6", Infinity);
				break;
			case "theme7":
				$("#deckbuilderWrapper").removeClass("flat");
				$("#ipboard_body").removeClass().addClass("theme3 themeBackgroundknights");
				$("#deckbuilderjqueryuicss").attr({href : 'http://www.cardgamedb.com/deckbuilders/css/jquery-ui1.10.2.css'});
				$("#deckbuilderselect2css").attr({href : 'http://www.cardgamedb.com/deckbuilders/css/select2.css'});
				$("#logo>a>img").attr({src : 'http://www.cardgamedb.com/deckbuilders/images/logo-small.png'});
				docCookies.setItem("AHDeckbuilderTheme", "theme7", Infinity);
				break;
			default:
				$("#deckbuilderWrapper").removeClass("flat");
				$("#ipboard_body").removeClass();
				$("#deckbuilderjqueryuicss").attr({href : 'http://www.cardgamedb.com/deckbuilders/css/jquery-ui1.10.2.css'});
				$("#deckbuilderselect2css").attr({href : 'http://www.cardgamedb.com/deckbuilders/css/select2.css'});
				$("#logo>a>img").attr({src : 'http://www.cardgamedb.com/forums/public/style_images/gravity_blue/logo.png'});
				docCookies.setItem("AHDeckbuilderTheme", "default", Infinity);
		}
	}

	$("#deckbuilderSettings").button().on("click",function(){
    	$( "#settingsdialog" ).dialog("open");
    });

	if (docCookies.hasItem("AHinterfaceViewType")){
		ahGlobals.interfaceViewType  = docCookies.getItem("AHinterfaceViewType");
		changeDeckbuilderView(ahGlobals.interfaceViewType);
	} else {
		var width = $(window).width();
		resizeDeckbuilderInterface (width,false);
	}

	$("#changeDeckbuilderView").on("click", function (){
		changeDeckbuilderView(ahGlobals.currentlyDisplayedInterfaceChoice);
	});

	$("#changeDeckbuilderViewDefault").on("click", function (){
		var texttodisplay = "The default view attempts to show at the most appropriate size for your browser.";
		$("#divDeckViewStyle").empty().html(texttodisplay);
		ahGlobals.currentlyDisplayedInterfaceChoice = "default";
	});

	$("#changeDeckbuilderView2-1-1").on("click", function (){
		var texttodisplay = "The Wide Search view will show your search criteria and results in a double wide box. When in this view all your search results will update in real time.";
		var imgtodisplay  = "<img src='http://www.cardgamedb.com/deckbuilders/arkhamhorror/211view.jpg' style='max-width:700px;max-height:450px;' />";
		$("#divDeckViewStyle").empty().html(texttodisplay+'<br/>'+imgtodisplay);
		ahGlobals.currentlyDisplayedInterfaceChoice = "211";
	});

	$("#changeDeckbuilderView1-1-2").on("click", function (){
		var texttodisplay = "The Wide Deck view will show your deck in a double wide box.";
		var imgtodisplay  = "<img src='http://www.cardgamedb.com/deckbuilders/arkhamhorror/112view.jpg' style='max-width:700px;max-height:450px;' />";
		$("#divDeckViewStyle").empty().html(texttodisplay+'<br/>'+imgtodisplay);
		ahGlobals.currentlyDisplayedInterfaceChoice = "112";
	});

	$("#changeDeckbuilderView1-1-1").on("click", function (){
		var texttodisplay = "The Narrow view will take up the least amount of space in your browser.";
		var imgtodisplay  = "<img src='http://www.cardgamedb.com/deckbuilders/arkhamhorror/111view.jpg' style='max-width:700px;max-height:450px;' />";
		$("#divDeckViewStyle").empty().html(texttodisplay+'<br/>'+imgtodisplay);
		ahGlobals.currentlyDisplayedInterfaceChoice = "111";
	});

	$("#changeDeckbuilderView2-1-2").on("click", function (){
		var texttodisplay = "The Wide Screen view will show both your deck and search results in double wide boxes. Your search results will update in real time.";
		var imgtodisplay  = "<img src='http://www.cardgamedb.com/deckbuilders/arkhamhorror/212view.jpg' style='max-width:700px;max-height:450px;' />";
		$("#divDeckViewStyle").empty().html(texttodisplay+'<br/>'+imgtodisplay);
		ahGlobals.currentlyDisplayedInterfaceChoice = "212";
	});

	$("#changeDeckbuilderView2-0-2").on("click", function (){
		var texttodisplay = "The Wide Deck and Search view will remove the center card image and instead show it when you hover over a card it will also show both your deck and search results in double wide boxes. Your search results will update in real time.";
		var imgtodisplay  = "<img src='http://www.cardgamedb.com/deckbuilders/arkhamhorror/202view.jpg' style='max-width:700px;max-height:450px;' />";
		$("#divDeckViewStyle").empty().html(texttodisplay+'<br/>'+imgtodisplay);
		ahGlobals.currentlyDisplayedInterfaceChoice = "202";
	});

	$("#changeDeckbuilderView2-T-2").on("click", function (){
		var texttodisplay = "This will stack the deckbuilder vertically, useful when using a tablet in portrait orientation.";
		var imgtodisplay  = "<img src='http://www.cardgamedb.com/deckbuilders/arkhamhorror/2T2view.jpg' style='max-width:700px;max-height:450px;' />";
		$("#divDeckViewStyle").empty().html(texttodisplay+'<br/>'+imgtodisplay);
		ahGlobals.currentlyDisplayedInterfaceChoice = "2T2";
	});


	function changeDeckbuilderView(whichview){
		$('#ipboard_body').removeClass("bodywidth630");
		$('#ipbwrapper').removeClass (function (index, css) {
		    return (css.match (/\bwrapperOverride\S+/g) || []).join(' ');
		});
		$('#deckbuilderWrapper').removeClass (function (index, css) {
		    return (css.match (/\bcontainerOverride\S+/g) || []).join(' ');
		});
		if (ahGlobals.mobile){
			$('#ahdb-submit-button').show();
		}
		switch (whichview){
			case "202":
				$('#ipbwrapper').addClass("wrapperOverride202");
				$('#deckbuilderWrapper').addClass("containerOverride202");
				ahGlobals.interfaceViewType = "202";
				docCookies.setItem("AHinterfaceViewType", "202", Infinity);
				break;
			case "2T2":
				$('#ipbwrapper').addClass("wrapperOverride2T2");
				$('#deckbuilderWrapper').addClass("containerOverride2T2");
				
				if (ahGlobals.mobile){
					$('#ipboard_body').addClass("bodywidth630");
				}
				ahGlobals.interfaceViewType = "2T2";
				docCookies.setItem("AHinterfaceViewType", "2T2", Infinity);
				break;
			case "212":
				$('#ipbwrapper').addClass("wrapperOverride212");
				$('#deckbuilderWrapper').addClass("containerOverride212");
				ahGlobals.interfaceViewType = "212";
				docCookies.setItem("AHinterfaceViewType", "212", Infinity);
				break;
			case "111":
				$('#ipbwrapper').addClass("wrapperOverride111");
				$('#deckbuilderWrapper').addClass("containerOverride111");
				ahGlobals.interfaceViewType = "111";
				docCookies.setItem("AHinterfaceViewType", "111", Infinity);
				break;
			case "112":
				$('#ipbwrapper').addClass("wrapperOverride112");
				$('#deckbuilderWrapper').addClass("containerOverride112");
				ahGlobals.interfaceViewType = "112";
				docCookies.setItem("AHinterfaceViewType", "112", Infinity);
				break;
			case "211":
				$('#ipbwrapper').addClass("wrapperOverride211");
				$('#deckbuilderWrapper').addClass("containerOverride211");
				ahGlobals.interfaceViewType = "211";
				docCookies.setItem("AHinterfaceViewType", "211", Infinity);
				break;
			default:
				$('#ipbwrapper').addClass("wrapperOverride202");
				$('#deckbuilderWrapper').addClass("containerOverride202");
				ahGlobals.interfaceViewType = "default";
				docCookies.setItem("AHinterfaceViewType", "default", Infinity);	
		}		
	}

	//adds click listner for + and +2 buttons on search results and the +/- buttons on deck cards
	$("#searchUL,#deckContainer>div>ul.deckCards,#weaknessResults").on("click", "button.addCards", function(e){
		var taffylookup,
			redrawgraphs,
			start,
			mybutton,
			end,
			time,
			quantity,
			card,
			cardID;
		if (ahGlobals.interfaceViewType == "202"){
			e.stopPropagation();
			e.preventDefault();
		}
		if (!ahGlobals.autoupdategraphs){
			taffylookup = false;
			redrawgraphs = false;
		}else{
			taffylookup = true;
			redrawgraphs = true;
		}
		start = new Date().getTime();
		mybutton = $(this),
		resultingAction =[],
		cardID = String(mybutton.data("cardid")),
		card = ahGlobals.db({id:cardID}).order('name').get(),
		quantity = mybutton.data("quantity");
		$.each(card,function(k,v){
			resultingAction.push(adjustDeckObject ('', quantity, v));
			
		});

		adjustDeckHTML(resultingAction);
		sampleHand();
		adjustStatsHTML();
		if (redrawgraphs){
			populateCharts(deckObject,false);
		}

		end = new Date().getTime();
		time = end - start;
		//console.log('Execution time (ms): ' , time);
		deckObject.saved = false;
		$('#spanDeckSavedStatus').text("Deck Not Saved").removeClass('decksavegreen').addClass('decksavered');
	});

	//change listener for all elements in the search box. This gives the live change results of searched cards. Only works on the 'expanded' view 
	$('#filterType,#filterSlot,#filterSanity,#operatorSanity,#filterHealth,#operatorHealth,#filterWild,#operatorWild,#filterAgility,#operatorAgility,#filterCombat,#operatorCombat,#filterIntellect,#operatorIntellect,#filterWillpower,#operatorWillpower,#filterLevel,#operatorLevel,#GuardianFB,#MysticFB,#RogueFB,#SeekerFB,#SurvivorFB,#neutralFB,#filterUnique,#filterCost,#operatorCost,#filterTextField,#orderBy').change(function(){
		//we dont want to constantly filter if the results area is hidden
		if ($("#ahdb-submit-button").is(":hidden")){
			filterCards();
		}
	});

	//manual button to trigger search for cards
	$("#ahdb-submit-button").click( function(){
		if ($("#ahdb-submit-button").is(":visible")){
			$("#searchloadinghide").fadeIn( "fast", function() {
				filterCards();
				$("#searchDiv").addClass("searchAndResultsDivPosition");
				$('#searchandresults').html("&#171; View Search Criteria");
				$("#searchloadinghide").hide();
			  });
			
			
		}
	});

	//Changes the view of the deck to show the cards either by type or cost
	$("#deckOrderBy").change(function(){
		var $value = $(this).val(),
		$deck,
		costslist = [],
		ele;
		if ($value == "Type"){
			$("#deckContainer>div#deckSectionCost").hide();
			$deck = $('#deckContainer>div#deckSectionCost>ul');
			ele = $deck.find('.deckCard').sort(function (a, b) {
	            return  a.dataset.cardname.localeCompare(b.dataset.cardname);
	        });
	        $.each(ele, function(k,v){
	        	var cardtype = v.dataset.cardtype;
	        	$("#deck"+cardtype+" li:first").after(v);

	        });
			$("#deckSection2,#deckSection3").show();

		} else if ($value == "Cost"){
			
			$("#deckSection2,#deckSection3").hide();
			$deck = $('#deckSection2,#deckSection3');
	        $.each($deck.find('.deckCard'),function(k,v){
	        	$("#deckCardCost"+v.dataset.cardcost).after(v);
	        	costslist.push(v.dataset.cardcost);
	        });
	        $.each(costslist, function (k,v){
	        	$("#deckCardCostUL"+v).show();
	        });
	        $("#deckContainer>div#deckSectionCost").show();
		}
	});

	//resets search fields
	$("#inputSearchReset").click( function(e){
		e.preventDefault();
		resetSearch();
	});

	//when a large number of cards are returned in a result it can degrade performance if we add all of those to the DOM at the same time. Instead we add the top 200 cards and then when the scroll box reaches the bottom we add in the next 200, etc...
	$('#resultsDiv').bind('scroll',chk_scroll);

	function chk_scroll(e)
	{
		var elem = $(e.currentTarget),
		subset,
		list = [];
		if (elem[0].scrollHeight - elem.scrollTop() == elem.outerHeight()) 
		{
			if (ahGlobals.results.length>0){
				subset = ahGlobals.results.splice(0,200);
				
				$.each(subset, function(k,v){
					list.push(deckbuilderTemplate(v,'search'));
				});
				$('#searchUL').append(list.join(""));
				$("#searchUL img.lazy").lazyload({         
					container: $("#resultsDiv"),
					skip_invisible : false
				});
			}
		}

	}

	//Prepares various areas for new filter results and calls the filter
	function filterCards(grabcopy){
		if (typeof grabcopy === 'undefined') grabcopy = false;
		var totalResults,
		subset,
		list = [];
		$('#searchUL').empty();
		$("#resultsDiv").scrollTop(0);
		ahGlobals.results = buildFilter();
		if (grabcopy){
			ahGlobals.autocomplete = $.extend(true,[],ahGlobals.results);
			setAutoComplete (ahGlobals.autocomplete);
		}
		totalResults = ahGlobals.results.length;
		subset = ahGlobals.results.splice(0,200);
		$.each(subset, function(k,v){
			list.push(deckbuilderTemplate(v,'search'));
		});
		$('#searchUL').append("<li>Cards Returned: "+totalResults+"</li>").append(list.join(""));
		$("#searchUL img.lazy").lazyload({         
			container: $("#resultsDiv"),
			skip_invisible : false
		});	
		$("#resultsDiv").trigger('scroll');
	}
	
	
	//filters the cards based on the values in the search criteria
	function buildFilter(){
		var filterType = {},
			filterCost = {},
			filterSlot = {},
			filterLevel = {},
			filterWillpower = {},
			filterIntellect = {},
			filterCombat = {},
			filterAgility = {},
			filterWild = {},
			filterHealth = {},
			filterSanity = {},
			filterUnique = {},
			filterSets = {},
			filterText = {},
			filterTrait = {},
			filterName = {},
			filterClass = {},
			textArray = [],
			textRegex,
			regexTextString = "",
			traitArray = [],
			traitRegex,
			regexTraitString = "",
			nameArray = [],
			regexNameString = "",
			orderBy,
			item,
			record,
			uniqueness,
			$inputSelectedOrder = $('#orderBy').val(),
			$inputUnique = $('#filterUnique').val(),
			$inputSlot = $('#filterSlot').val(),
			$inputType = $('#filterType').val(),
			$inputCost = $('#filterCost').val(),
			$inputOperatorCost = $('#operatorCost').val(),
			$inputLevel = $('#filterLevel').val(),
			$inputOperatorLevel = $('#operatorLevel').val(),
			$inputWillpower = $('#filterWillpower').val(),
			$inputOperatorWillpower = $('#operatorWillpower').val(),
			$inputIntellect = $('#filterIntellect').val(),
			$inputOperatorIntellect = $('#operatorIntellect').val(),
			$inputCombat = $('#filterCombat').val(),
			$inputOperatorCombat = $('#operatorCombat').val(),
			$inputAgility = $('#filterAgility').val(),
			$inputOperatorAgility = $('#operatorAgility').val(),
			$inputWild = $('#filterWild').val(),
			$inputOperatorWild = $('#operatorWild').val(),
			$inputHealth = $('#filterHealth').val(),
			$inputOperatorHealth = $('#operatorHealth').val(),
			$inputSanity = $('#filterSanity').val(),
			$inputOperatorSanity = $('#operatorSanity').val(),
			$inputGuardian = $('#GuardianFB').prop('checked'),
			$inputMystic = $('#MysticFB').prop('checked'),
			$inputRogue = $('#RogueFB').prop('checked'),
			$inputSeeker = $('#SeekerFB').prop('checked'),
			$inputSurvivor = $('#SurvivorFB').prop('checked'),
			$inputNeutral = $('#neutralFB').prop('checked'),
			$inputText = $("#filterText").val(),
			$inputTrait = $("#filterTraits").val(),
			$inputName = $("#filterName").val();

		if ($.type( $inputType ) !== "null"){
			filterType.type = $inputType;
		} 


		if ($inputCost != 'Any'){
			filterCost.costnumeric = {};
			if ($inputOperatorCost == 'e'){
				filterCost.cost = {};
				filterCost.cost['=='] = $inputCost;
			}
			else if ($inputOperatorCost == 'gt'){
				filterCost.costnumeric = {};
				filterCost.costnumeric['gt'] = $inputCost;
			}
			else if ($inputOperatorCost == 'lt'){
				filterCost.costnumeric = {};
				filterCost.costnumeric['lt'] = $inputCost;
			}
		} 

		if ($inputLevel != 'Any'){
			filterLevel.lvl = {};
			if ($inputOperatorLevel == 'e'){
				filterLevel.lvl['=='] = $inputLevel;
			}
			else if ($inputOperatorLevel == 'gt'){
				filterLevel.lvl['gt'] = $inputLevel;
			}
			else if ($inputOperatorLevel == 'lt'){
				filterLevel.lvl['lt'] = $inputLevel;
			}
		} 

		if ($inputWillpower != 'Any'){
			filterWillpower.will = {};
			if ($inputOperatorWillpower == 'e'){
				filterWillpower.will['=='] = $inputWillpower;
			}
			else if ($inputOperatorWillpower == 'gt'){
				filterWillpower.will['gt'] = $inputWillpower;
			}
			else if ($inputOperatorWillpower == 'lt'){
				filterWillpower.will['lt'] = $inputWillpower;
			}
		} 

		if ($inputIntellect != 'Any'){
			filterIntellect.int = {};
			if ($inputOperatorIntellect == 'e'){
				filterIntellect.int['=='] = $inputIntellect;
			}
			else if ($inputOperatorIntellect == 'gt'){
				filterIntellect.int['gt'] = $inputIntellect;
			}
			else if ($inputOperatorIntellect == 'lt'){
				filterIntellect.int['lt'] = $inputIntellect;
			}
		} 

		if ($inputCombat != 'Any'){
			filterCombat.cmbt = {};
			if ($inputOperatorCombat == 'e'){
				filterCombat.cmbt['=='] = $inputCombat;
			}
			else if ($inputOperatorCombat == 'gt'){
				filterCombat.cmbt['gt'] = $inputCombat;
			}
			else if ($inputOperatorCombat == 'lt'){
				filterCombat.cmbt['lt'] = $inputCombat;
			}
		} 

		if ($inputAgility != 'Any'){
			filterAgility.agi = {};
			if ($inputOperatorAgility == 'e'){
				filterAgility.agi['=='] = $inputAgility;
			}
			else if ($inputOperatorAgility == 'gt'){
				filterAgility.agi['gt'] = $inputAgility;
			}
			else if ($inputOperatorAgility == 'lt'){
				filterAgility.agi['lt'] = $inputAgility;
			}
		} 

		if ($inputWild != 'Any'){
			filterWild.wild = {};
			if ($inputOperatorWild == 'e'){
				filterWild.wild['=='] = $inputWild;
			}
			else if ($inputOperatorWild == 'gt'){
				filterWild.wild['gt'] = $inputWild;
			}
			else if ($inputOperatorWild == 'lt'){
				filterWild.wild['lt'] = $inputWild;
			}
		} 

		if ($inputHealth != 'Any'){
			filterHealth.hlth = {};
			if ($inputOperatorHealth == 'e'){
				filterHealth.hlth['=='] = $inputHealth;
			}
			else if ($inputOperatorHealth == 'gt'){
				filterHealth.hlth['gt'] = $inputHealth;
			}
			else if ($inputOperatorHealth == 'lt'){
				filterHealth.hlth['lt'] = $inputHealth;
			}
		}

		if ($inputSanity != 'Any'){
			filterSanity.snty = {};
			if ($inputOperatorSanity == 'e'){
				filterSanity.snty['=='] = $inputSanity;
			}
			else if ($inputOperatorSanity == 'gt'){
				filterSanity.snty['gt'] = $inputSanity;
			}
			else if ($inputOperatorSanity == 'lt'){
				filterSanity.snty['lt'] = $inputSanity;
			}
		}

		if ($inputUnique != 'Any'){
			if ($inputUnique == 'NonUnique')
				uniqueness = "N";
			else
				uniqueness = "Y";
			filterUnique.unique = {};
			filterUnique.unique['=='] = uniqueness;

		} 

		if ($inputSlot != 'Any'){
			filterSlot.slot = {};
			filterSlot.slot['=='] = $inputSlot;

		}

		
		if (!$inputGuardian||!$inputMystic||!$inputRogue||!$inputSeeker||!$inputSurvivor||!$inputNeutral){
			filterClass = [];
			if ($inputGuardian){
				filterClass.push({clss:"Guardian"});
			}

			if ($inputMystic){
				filterClass.push({clss:"Mystic"});
			}

			if ($inputRogue){
				filterClass.push({clss:"Rogue"});
			} 

			if ($inputSeeker){
				filterClass.push({clss:"Seeker"});
			}

			if ($inputSurvivor){
				filterClass.push({clss:"Survivor"});
			} 
			if ($inputNeutral){
				filterClass.push({clss:"Neutral"});
			}
			
		}

		if (ahGlobals.selectedsets.length >0){
			
			filterSets = {setid:ahGlobals.selectedsets.replace(/(^,)|(,$)/g, "").split(",")};
		}

		if ($inputSelectedOrder == 'cost'){
			orderBy = "costnumeric asec, name asec";
		} else if ($inputSelectedOrder == 'set'){
			orderBy = "setname asec, num asec";
		} else if ($inputSelectedOrder == 'type'){
			orderBy = "type asec, name asec";
		}  else {
			orderBy = "name asec, lvl asec";
		}

		textArray = $inputText.match(/[a-zA-Z][a-zA-Z]:"(?:\\"|[^"])+"|"(?:\\"|[^"])+"|[+|-]?\w+/g);
		//var regexString = "";
		if (textArray != null){
			for (i=0;i<textArray.length;i++){
				if (textArray[i][0] == '"'){
					item = textArray[i].replace(/["']/g, "");
				} else {
					item = textArray[i];
				}
				//console.log("item",item);
				
				regexTextString +='(?=.*'+item+')';
			}

			//textRegex=new RegExp(regexString,'i');
			textRegex=new RegExp(regexTextString,'i');
			searchfield = $("#filterTextField").val();

			switch(searchfield)
			{
				case "Traits":
				  filterText = {traits:{regex:textRegex}};
				  break;
				case "Name":
				  filterText = {name:{regex:textRegex}};
				  break;
				case "text":
				default:
				  filterText = {text:{regex:textRegex}};
			}
			
		}
		ahGlobals.previousText = $("#filterText").val();
		record =$.extend(true, [], ahGlobals.db(ahGlobals.deckbuildingSearchRules,ahGlobals.deckbuildingSearchRestrictions,filterIntellect,filterCombat,filterAgility,filterWild,filterHealth,filterSanity,filterWillpower,filterLevel,filterSlot,filterType,filterClass,filterUnique,filterText,filterSets, filterCost,{showindb:{"is":"Y"}},{type:{"!is":"Investigator"}},{creqf:{"is":""}}).order(orderBy).get());
		return (record);

	}

	//check and uncheck all the house filter boxes
	$('#checkAll').click(function(){
			      $('.factionCheckboxes:checkbox').filter(":visible").prop('checked', true);
				  filterCards();
			});
	$('#uncheckAll').click(function(){
			      $('.factionCheckboxes:checkbox').filter(":visible").prop('checked', false);
				  filterCards();
			});

	$("#divExportButtons").buttonset();

	$("input[name=radioExport]:radio").change(function () {
		var exportchoice = $('input[name=radioExport]:radio:checked').val();
		$('#taTextExport').text(generateExport(exportchoice));
	});

	//when a tab is clicked we sometimes need to call some functions to populate data.
	function tabfunctions(e,u){
		var uid = u.newTab.attr('id');
		ahGlobals.thetabs = u.newTab.attr('id');
		switch (uid) {
			case "liClear":
				return false;
			break;
			case "liExport":
				var exportchoice = $('input[name=radioExport]:radio:checked').val();
				$('#taTextExport').text(generateExport(exportchoice));
				
				if (ahGlobals.deckname !=''){
					var deckName = html_entity_decode(ahGlobals.deckname,'ENT_QUOTES').replace(/[^0-9a-z]/gi, '');
					$('#offlineName').val(deckName);
				}
			break;
			case "liLoad":
				deckObject.fetchListOfDecks();
				displayFromLocal();
			break;
			case "liSubmit":
				$("#deckSubmitReview").hide();
				deckObject.fetchListOfDecks("Submit");
			break;
			case "liSave":
				
				
			break;
		}
	}

	//calls window resize function only used for IE8 and less
	$(window).resize();

	//click handler for 'Clear Deck' button
	$('#deckClear').click(function(){
		resetDeck(true,true,false);
	}); 

	//click handler for 'Process and Import Deck' button
	$('#import-button').click(
		function(){importdeck();
	});

	//more IE<=8 issues
	if (!$.support.leadingWhitespace) {
		//if ie version < 8 fallback jquery for @media selectors
		$(".purpdiv").css("overflow-y","scroll");
	}
	
	//pretty up select boxes don't do this on mobile as it degrades performance
	if( ahGlobals.mobile ) {
		ahGlobals.filters = $("#decktype");
		
	} else {
		ahGlobals.filters = $("#decktype,#filterSet,#weaknessSets");
		ahGlobals.filters1 = $("#filterTextField,#filterSanity,#operatorSanity,#filterHealth,#operatorHealth,#filterWild,#operatorWild,#filterAgility,#operatorAgility,#filterCombat,#operatorCombat,#filterIntellect,#operatorIntellect,#filterWillpower,#operatorWillpower,#filterLevel,#operatorLevel,#filterSlot,#filterUnique,#filterType,#operatorCost,#filterCost,#selectOrderBy,#selectOrderBy");
		ahGlobals.filters1.select2({ width: 'resolve',minimumResultsForSearch: -1 });
		$("#deckOrderBy").select2({ width: 'resolve',minimumResultsForSearch: -1 });
		$('#checkAll,#uncheckAll').tooltip();

	}
	ahGlobals.filters.select2({ width: 'resolve' });

	//impliments tab feature that shows hidden areas such as deck save and submission
	var deckTabs = $( "#tabs-buttons" ).tabs({ active: false,beforeActivate: function(event, ui) {
   		tabfunctions(event,ui);
   	}, collapsible: true, show: { effect: "blind", duration: 300 },hide: { effect: "blind", duration: 300 } });
	
	//disables 'Save', 'Load/Delete Saved', and 'Submit' tabs if you are not logged in
	if (ipb.vars['member_id'] == 0){
		deckTabs.tabs({ disabled: [ 0, 1, 2 ] });
	}
	
	//calls addSets when new set is selected
	$("#filterSet").change(function(){addSets($(this));});
	
	//Game Text Box filter field listener
	$("#filterText").change(function(){
		clearTimeout($(this).data('timeout'));
		if (ahGlobals.previousText.trim() != $(this).val().trim()){
			if ($("#ahdb-submit-button").is(":hidden")){
				$(this).data('timeout', setTimeout(filterCards, 500));
			}
	
		}

	}).keyup( function () {
		// fire the above change event after every letter
		$(this).change();
	});
	
	//disables drag to select text on all cards.
	$("#searchUL,.deckCards").disableSelection();

	//adds click listener to update and save deck buttons
	$("#buttonUpdateDeck,#buttonSaveNewDeck").click(function(i){deckObject.saveDeck(i);});

	//adds click listener on any matching element to remove that set from the selection
	$(".removeSelectedSet").on("click", function(){
		thiset = $(this).data("thisset");
		setDeleteSetClick(thiset );
	});

	//add click listener for cancel submission button and clears out the deck preview.
	$("#submitCancel").on("click", function(){
		$("#submitReviewDeck,#submitReviewDeckName,#submitReviewDeckFormat,#submitReviewDeckType,#submitReviewDeckTeaser,#submitReviewDeckStrategy").empty();
		$("#deckSubmitReview").hide();
	});
	

	//adds click listner for button which saves a copy as a text file on local machine
	$("#buttonSaveOffline").on("click",  function(){
		var textdeck;
		if ($('#offlineName').val() == ""){
			showMessage('deckExportMessage','error','You must name your deck to export it.');
			return false;
		} else if (deckObject.totalCards < 1) {
			showMessage('deckExportMessage','error','Your deck is empty. A deck myst have at least 1 card to export it.');
			return false;
		} else {
			exporttype = $('#fileType option:selected').val();
			if ( exporttype== 'txt'){
				textdeck = generateExport('text');
			} else if (exporttype == 'o8d'){
				textdeck = generateExport('octgn');
			}
			$('#dcont').val(textdeck);
			$('#decksave').submit();
		}
	
	});
	
	//deletes deck from local storage
	$("#loadDeck").on("click","#savedLocalStorage>li>div>img.deleteLocal", function(){
		deleteFromLocal($(this).data("lskey"));
	});
	//loads deck fromlocal storage
	$("#loadDeck").on("click","#savedLocalStorage>li>div>span.loadLocal", function(){
		loadFromLocal($(this).data("lskey"));
	});

	//click listeners for deck view change buttons. sets cookies so the view preference is remembered.
	$("#deckview1").click(function(){
		setViewType("deckview","deckview1","remove");
		docCookies.setItem("deckview", "deckview1", Infinity);
	});
	$("#deckview2").click(function(){
		setViewType("deckview","deckview2","remove");
		docCookies.setItem("deckview", "deckview2", Infinity);
	});
	$("#deckview3").click(function(){
		setViewType("deckview","deckview3","remove");
		docCookies.setItem("deckview", "deckview3", Infinity);
	});
	$("#searchview1").click(function(){
		setViewType("searchUL","searchview1","remove");
		docCookies.setItem("searchView", "searchview1", Infinity);
	});
	$("#searchview2").click(function(){
		setViewType("searchUL","searchview2","add");
		docCookies.setItem("searchView", "searchview2", Infinity);
	});
	$("#searchview3").click(function(){
		setViewType("searchUL","searchview3","add");
		docCookies.setItem("searchView", "searchview3", Infinity);
	});

	function setViewType(section,type,action) {
		$('#'+type).siblings().removeClass("blue").addClass("grey");
		$('#'+type).removeClass("grey").addClass("blue");
		var classesToAdd="",classesToRemove="",mobileClass="";
		if (type == "searchview1"){
			classesToRemove = "smalldisplay noset ";
			if (ahGlobals.mobile)
				classesToRemove += " smalldisplay-touch";
		} else if (type == "searchview2") {
			classesToAdd = "smalldisplay";
			classesToRemove = "noset";
			if (ahGlobals.mobile)
				classesToAdd += " smalldisplay-touch";
		} else if (type == "deckview1") {
			classesToRemove = "smalldisplay noset";
			if (ahGlobals.mobile)
				classesToRemove += " smalldisplay-touch";
		} else if (type == "deckview2") {
			classesToRemove = "noset";
			classesToAdd = "smalldisplay";
			if (ahGlobals.mobile)
				classesToAdd += " smalldisplay-touch";
		} else if (type == "deckview3") {
			classesToAdd = "smalldisplay noset ";
			if (ahGlobals.mobile)
				classesToAdd += " smalldisplay-touch";
		} else if (type == "searchview3") {
			classesToAdd = "smalldisplay noset ";
			if (ahGlobals.mobile)
				classesToAdd += " smalldisplay-touch";
		}

		if (section == "searchUL"){
			$('#searchUL').addClass(classesToAdd);
			$('#searchUL').removeClass(classesToRemove);
			$("#resultsDiv").trigger('scroll');
		} else if (section == "deckview"){
			$('.deckCards').addClass(classesToAdd);
			$('.deckCards').removeClass(classesToRemove);
		}
		
	}
	//Checks the cookies on page load and sets the appropriate view.
	if (docCookies.getItem("searchView") == "searchview2"){
		setViewType("searchUL","searchview2","add");
	}
	if (docCookies.getItem("searchView") == "searchview3"){
		setViewType("searchUL","searchview3","add");
	}
	if (docCookies.getItem("searchView") == "searchview1" || !docCookies.hasItem("searchView")){
		setViewType("searchUL","searchview1","remove");
	}
	if (docCookies.getItem("deckview") == "deckview1" || !docCookies.hasItem("deckview")){
		setViewType("deckview","deckview1","remove");
	}
	if (docCookies.getItem("deckview") == "deckview2"){
		setViewType("deckview","deckview2","remove");
	}
	if (docCookies.getItem("deckview") == "deckview3"){
		setViewType("deckview","deckview3","remove");
	}

	//switches from viewing search criteria to search results (if that area is small)
	$('#searchandresults').click(function(){

		if ($("#searchDiv").hasClass("searchAndResultsDivPosition")){
			$("#searchDiv").removeClass("searchAndResultsDivPosition");
			$('#searchandresults').html("View Search Results &#187;");
		} else {
	   		$("#searchDiv").addClass("searchAndResultsDivPosition");
			$('#searchandresults').html("&#171; View Search Criteria");
			$("#resultsDiv").trigger('scroll');
		}
	});

	//click listener for saving a deck to local storage
	$('#savedecklocalstorage').click(function(){
		saveToLocal();
	});

	//when a set is selected from dropdown shows it in the list and disables the dropdown element
	function addSets(selement){
		var sindex = selement[0].selectedIndex;
		if (sindex  != 0){
			sOption = selement.find(':selected');
			hasSubcats = sOption.data('hassubcats');
			relatedSets = sOption.data('relatedsets').toString();
			if (hasSubcats){
				d = relatedSets.slice(0, -1).split(",");
				for (var e = 0; e < d.length; e++) {
					$("#set-" + d[e]).attr('disabled', 'disabled');
					if (!ahGlobals.selectedsets.include(d[e] + ",")) ahGlobals.selectedsets += d[e] + ",";
					if (!$("#dispset-" + d[e]).is(':visible')) $("#dispset-" + d[e]).show();
				}
			} else {
				$("#dispset-" + relatedSets ).show();
				sOption.attr('disabled', 'disabled');
				if (!ahGlobals.selectedsets.include(relatedSets + ",")) ahGlobals.selectedsets += relatedSets + ",";
			}
		}
		if ($("#ahdb-submit-button").is(":hidden")){
			filterCards();
		}
	}

	//removes a set from selected status and enables selection again
	function setDeleteSetClick(a) {
		$("#dispset-" + a).hide();
		$("#set-" + a).removeAttr('disabled');
		ahGlobals.selectedsets = ahGlobals.selectedsets.replace(a + ",", "");
		if ($("#ahdb-submit-button").is(":hidden")){
			filterCards();
		}
	}

	//an object is passed in listing the action and cards that have changed in the deck. This then updates all the html associated with
	// those items. The deckObject has always been updated before this is called.
	function adjustDeckHTML (action){
		var $deckView = $("#deckOrderBy").val(),
		traitsText = [];
		$('#totalCount').text(deckObject.totalCards);
		$('#assetCount').text(deckObject[ahGlobals.whichGame].counts.asset);
		$('#skillCount').text(deckObject[ahGlobals.whichGame].counts.skill);
		$('#eventCount').text(deckObject[ahGlobals.whichGame].counts.event);
		
		$('#deckviewclasses>div.Guardian').text(deckObject[ahGlobals.whichGame].counts.guardian);
		$('#deckviewclasses>div.Mystic').text(deckObject[ahGlobals.whichGame].counts.mystic);
		$('#deckviewclasses>div.Rogue').text(deckObject[ahGlobals.whichGame].counts.rogue);
		$('#deckviewclasses>div.Survivor').text(deckObject[ahGlobals.whichGame].counts.survivor);
		$('#deckviewclasses>div.Seeker').text(deckObject[ahGlobals.whichGame].counts.seeker);
		$('#deckviewclasses>div.Neutral').text(deckObject[ahGlobals.whichGame].counts.neutral);

		$.each(deckObject[ahGlobals.whichGame].traitstotrack, function(k,v){
			traitsText.push(k.replace(".", "")+": "+v); 
		});
		$("#deckviewtraits").text(traitsText.join(", "));

		$.each(action, function (k,v){
			
			if (v.action == 'new'){
				if (v.card[0].deckquantity == 0) return false;

				if (v.card[0].creqf == "" && v.card[0].type != "Investigator" ){
					cardHTML = deckbuilderTemplate(v.card[0], 'deck');
				} else if (v.card[0].type == "Investigator") {
					cardHTML = deckbuilderTemplate(v.card[0], 'investigator');
				} else {
					cardHTML = deckbuilderTemplate(v.card[0], 'required');
				}

				$("#deckCardCostUL"+v.card[0].costnumeric).show();
				if ($deckView == "Type"){
					if (v.card[0].wtype == 'Weakness'){
						$("#deckWeakness li:eq(0)").after(cardHTML);
					} else if (v.card[0].wtype == 'Basic Weakness'){
						$("#deckWeakness").append(cardHTML);
					} else {
						if (v.card[0].creqf == ""){
							$("#deck"+v.card[0].type).append(cardHTML);
						} else {
							$("#deck"+v.card[0].type+" li:eq(0)").after(cardHTML);
						}
						
					}
					
				} else {
					if (v.card[0].type == 'Investigator'){
						$("#deck"+v.card[0].type).append(cardHTML);
					} else if (v.card[0].wtype == 'Weakness'){
						$("#deckWeakness li:eq(0)").after(cardHTML);
					} else if (v.card[0].wtype == 'Basic Weakness'){
						$("#deckWeakness").append(cardHTML);
					} else {
						$("#deckCardCost"+v.card[0].costnumeric).after(cardHTML);
					}
					
				}
				
				
				if (v.card[0].deckquantity >= v.card[0].max){
					$('#dcab'+v.card[0].id).addClass('grey');
				}
				if (v.card[0].type != 'Investigator' && v.card[0].wtype != 'Weakness' && v.card[0].wtype != 'Basic Weakness'){
					$("#deckCardCost"+v.card[0].cost+"Count").text(deckObject[ahGlobals.whichGame].costs[v.card[0].cost]);
				}
				
				
			} else if (v.action == 'adjust'){
				$("#deckcardcount"+v.card[0].id).text(v.card[0].deckquantity);
				if (v.card[0].deckquantity >= v.card[0].max){
					$('#dcab'+v.card[0].id).addClass('grey');
				} else {
					$('#dcab'+v.card[0].id).removeClass('grey');
				}
				if (v.card[0].type != 'Investigator' && v.card[0].wtype != 'Weakness' && v.card[0].wtype != 'Basic Weakness'){
					$("#deckCardCost"+v.card[0].cost+"Count").text(deckObject[ahGlobals.whichGame].costs[v.card[0].cost]);
				}
			} else if (v.action == 'removed'){
				$("#dc"+v.card[0].id).remove();
				if (deckObject[ahGlobals.whichGame].costs[v.card[0].cost] <= 0){
					$("#deckCardCost"+v.card[0].cost).hide();
				}
				if (v.card[0].type != 'Investigator' && v.card[0].wtype != 'Weakness' && v.card[0].wtype != 'Basic Weakness'){
					$("#deckCardCost"+v.card[0].cost+"Count").text(deckObject[ahGlobals.whichGame].costs[v.card[0].cost]);
				}
				
			}
		});
		var countByCardName = count(deckObject.cards, function(item) { return item.name; });
		//console.log (countByCardName);
		$(".deckCard").removeClass("problemCards");
		$.each(countByCardName, function(k,v){
			var cardLIIDs = '';
			if (v.totalquantity>2){
				cardLIIDs = v.cardids.join(',');
				$(cardLIIDs).addClass("problemCards");
			}
		});
	}

	count = function(ary, classifier) {
	    return ary.reduce(function(counter, item) {
	    	var card = item;
	        var p = (classifier || String)(item);
	        if (counter.hasOwnProperty(p)){
	        	counter[p].totalquantity = counter[p].totalquantity+1*card.deckquantity;
	        	counter[p].cardids.push('#dc'+card.id);
	        } else {
	        	counter[p] = {totalquantity : 1*card.deckquantity, cardids:['#dc'+card.id]};
	        }

	        return counter;
	    }, {});
	};

	//This  updates any summary information in the HTML 
	function adjustStatsHTML(){
		var restrictedtext = "",
		restrictedcount = 0,
		restrictedcolor = 'black',
		classCountList = "";

		$.each(deckObject.restrictedcards, function(k,v){
			restrictedcount++;
			restrictedtext += k+'\n';
		});
		$.each(deckObject[ahGlobals.whichGame].classList,function (k, v){
			var stripClass = v.toLowerCase().replace(/ /g, '').replace(/'/g, '');
			$("#"+stripClass+"Count").text(deckObject[ahGlobals.whichGame].counts[stripClass]);
        });

		if (restrictedcount>1){
			restrictedtext = "Possible Restricted List Issue!\n"+restrictedtext;
			restrictedcolor = 'red';
		} 
		$('#willpowerCount').text(deckObject[ahGlobals.whichGame].counts["willpower"]);
		$('#intellectCount').text(deckObject[ahGlobals.whichGame].counts["intellect"]);
		$('#combatCount').text(deckObject[ahGlobals.whichGame].counts["combat"]);
		$('#agilityCount').text(deckObject[ahGlobals.whichGame].counts["agility"]);
		$('#wildCount').text(deckObject[ahGlobals.whichGame].counts["wild"]);
		$('#totalLevels').text(deckObject[ahGlobals.whichGame].counts["level"]);
		$('#restrictedContainer').text("Restricted Cards:\n"+restrictedtext).css('color', restrictedcolor);
	}

	//This adds or subtracts a card from the deckObject and also updates any other totals that are being tracked in the object
	function adjustDeckObject (actionType, quantity, cardObject){
		var arrayIndex,
		maxCopiesAllowed,
		returnresults = {action:'',card:[]};

		var deckCard = deckObject.cards.filter(function (el, ix) {
            if (el.id === cardObject.id) {
                arrayIndex = ix;
                return el.id == cardObject.id;
            }
        });

		if ($.isNumeric(cardObject.max)){
			maxCopiesAllowed = cardObject.max;
		} else {
			maxCopiesAllowed = 2;
			cardObject.max = 2;
		}

        if (deckCard.length >0){//card already in deck
        	if (deckCard[0].deckquantity>=maxCopiesAllowed && quantity>0) return(false);
        	if (quantity+deckCard[0].deckquantity>maxCopiesAllowed){
				quantity = maxCopiesAllowed-deckCard[0].deckquantity;
			}
			deckCard[0].deckquantity += quantity;
			if (deckCard[0].deckquantity <=0){
				deckObject.cards.splice(arrayIndex,1 );
            	returnresults.action = 'removed';
            	delete deckObject.restrictedcards[deckCard[0].name+ ' '+ deckCard[0].setname];
			} else {
				returnresults.action = 'adjust';
			}
			returnresults.card.push(deckCard[0]);
        } else {//card not in deck
        	if (quantity > maxCopiesAllowed){
        		quantity = maxCopiesAllowed;
        	}
        	cardObject.deckquantity = quantity;
        	deckObject.cards.push(cardObject);
        	returnresults.action = 'new';
        	returnresults.card.push(cardObject);
        }
        if (cardObject.wtype != 'Weakness' && cardObject.wtype != 'Basic Weakness'){
        	deckObject[ahGlobals.whichGame].counts[cardObject.type.toLowerCase().replace(/ /g, '').replace(/-/g, '')] += quantity;
        } else {
        	deckObject[ahGlobals.whichGame].counts['weakness'] += quantity;
        }
        

                    
        if (cardObject.type != 'Investigator' && cardObject.wtype != 'Weakness' && cardObject.wtype != 'Basic Weakness'){
        	deckObject[ahGlobals.whichGame].counts[cardObject.clss.toLowerCase().replace(/ /g, '').replace(/-/g, '')] += quantity;
        	if (typeof deckObject[ahGlobals.whichGame].costs[cardObject.costnumeric] === 'undefined'){
            	deckObject[ahGlobals.whichGame].costs[cardObject.costnumeric] = 0;
            }
            deckObject[ahGlobals.whichGame].costs[cardObject.costnumeric] += quantity;

            if ($.isNumeric(cardObject.will)){
            	deckObject[ahGlobals.whichGame].counts["willpower"] += quantity*cardObject.will;
            }
            if ($.isNumeric(cardObject.int)){
            	deckObject[ahGlobals.whichGame].counts["intellect"] += quantity*cardObject.int;
            }
            if ($.isNumeric(cardObject.cmbt)){
            	deckObject[ahGlobals.whichGame].counts["combat"] += quantity*cardObject.cmbt;
            }
            if ($.isNumeric(cardObject.agi)){
            	deckObject[ahGlobals.whichGame].counts["agility"] += quantity*cardObject.agi;
            }
            if ($.isNumeric(cardObject.wild)){
            	deckObject[ahGlobals.whichGame].counts["wild"] += quantity*cardObject.wild;
            }
            if ($.isNumeric(cardObject.lvl)){
            	deckObject[ahGlobals.whichGame].counts["level"] += quantity*cardObject.lvl;
            }

        	if (cardObject.ctotot == "Y") {
        		deckObject.totalCards += quantity;
        	}
        	var traitsArray = cardObject.traits.split(". ");
        	$.each(traitsArray, function(k,v){
        		if (v.substring(v.length-1) != "."){
        			v += ".";
        		}
        		if (typeof deckObject[ahGlobals.whichGame].traitstotrack[v] != "undefined"){
        			deckObject[ahGlobals.whichGame].traitstotrack[v] += quantity;
        		}
        	});
        }
		
		return returnresults;

	}


	//goes to server and gets list of saved decks and displays them for either the private save or the deck submit
	deckContents.prototype.fetchListOfDecks = function (xfetchListOfDecks){
		if(typeof(xfetchListOfDecks)==='undefined') xfetchListOfDecks = "Load";
		if(!ahGlobals.ajaxDeckManipulationInProgress && ipb.vars['member_id'] != 0){
			var surl= ipb.vars['base_url']+'app=ccs&module=ajax&section=ahdeckbuilder&do=loadSavedDecks',
			formData = [];
			formData.push({name: 'fPage', value: 'deckbuilder'});
			formData.push({name: 'fgame', value: ahGlobals.whichGame});
			formData.push({name: 'md5check', value: ipb.vars['secure_hash']});
			$.ajax({
				type: "POST",
				url: surl,
				beforeSend: function ( xhr ) {
					$("#deck"+xfetchListOfDecks).empty();
					$("#deck"+xfetchListOfDecks).append('<li><img src="'+deckbuilderGlobals.image_path +'/images/dbload.gif"></li><li>Loading Saved Decks...</li>');
					ahGlobals.ajaxDeckManipulationInProgress = true;
				},
				data: formData,
				dataType: "json"
			}).done(function( msg ) {
			  	if (msg.status=='success'){
			  		if (msg.count >0){
				  		showMessage('deck'+xfetchListOfDecks+'Message','success',"Decks Successfully Loaded!");
				  		$("#deck"+xfetchListOfDecks).empty();
				  		$.each(msg.decks, function (k, v){
				  			if (xfetchListOfDecks=="Load"){
				  				var deckImage = '';
				  				$.each(v.investigator, function (key,_card){
				  					if (_card.imgf.startsWith('http://') || _card.imgf.startsWith('https://')){
										deckImage = '<img id="deckImage'+v.id+'" data-image="'+_card.imgf+'" style="max-height:65px;" src="'+_card.imgf+'" />';
									} else {
										deckImage = '<img id="deckImage'+v.id+'" data-image="'+ahGlobals.imagepath+'/'+_card.imgf+'" style="max-height:65px;" src="'+ahGlobals.imagepath+'/tn_'+_card.imgf+'" />';
									}

				  				});
								$("#deckLoad").append('<li id="liUserDeck'+v.id+'"><div style="display: table-cell;width: 80px;vertical-align: middle;">'+deckImage+'</div><div style="display: table-cell;width: 310px;vertical-align: top;padding: 3px;"><span style="cursor:pointer;font-weight:bold;font-size:large;" id="spanUserDeck'+v.id+'"></span><br /><abbr style="" class="published" title="'+v.ISO8601+'">'+v.updated+'</abbr></div><div style="display: table-cell;width:80px;"><img id="imgLinkDeck'+v.id+'" style="cursor:pointer;margin-right:5px;width:32px;" src="'+deckbuilderGlobals.image_path +'/images/link.png"><img id="imgDeleteDeck'+v.id+'" style="cursor:pointer;margin-right:5px;" src="'+deckbuilderGlobals.image_path +'/images/delete-icon.png"></div></li>');

								$("#spanUserDeck"+v.id).text(html_entity_decode(v.name,'ENT_QUOTES'));
								var deckspan = $("#spanUserDeck"+v.id)[0];
		    					$.data(deckspan , "identifiers", { guid: v.guid, id: v.id });
								$("#spanUserDeck"+v.id).click(function(){
									if (ahGlobals.mobile){
										$( "#loadingmessage" ).dialog("open").promise().done(function() {
										loadDeck(v.id,v.guid);
										});
									} else {
										loadDeck(v.id,v.guid);
									}
								});
								var deckdeleteimage = $("#imgDeleteDeck"+v.id)[0];
								$.data(deckdeleteimage , "identifiers", { guid: v.guid, id: v.id });
								$("#imgDeleteDeck"+v.id).click(function(){
									deleteDeck(v.id,v.guid);
								});
								var decklinkimage = $("#imgLinkDeck"+v.id)[0];
								$.data(decklinkimage , "identifiers", { guid: v.guid, id: v.id });
								$("#imgLinkDeck"+v.id).click(function(){
									linkDeck(v.id,v.guid);
								});
								$("#deckImage"+v.id).click(function(){
									
									$('<div />').html('<img src="'+$(this).data("image")+'" />').dialog({
								         minHeight: 400,
								         width: 450,
								         modal: true,
								         draggable: false,
								         resizable: false});
								});

							}
							if (xfetchListOfDecks=="Submit"){
								$("#deckSubmit").append('<li><span style="cursor:pointer" id="spanUserDeckSubmit'+v.id+'"></span><abbr style="float:right;" class="published" title="'+v.ISO8601+'">'+v.updated+'</abbr></li>');
			
								$("#spanUserDeckSubmit"+v.id).text(html_entity_decode(v.name,'ENT_QUOTES'));
								var deckspan = $("#spanUserDeckSubmit"+v.id)[0];
		    						$.data(deckspan , "identifiers", { guid: v.guid, id: v.id });
								$("#spanUserDeckSubmit"+v.id).click(function(){
									//console.log($.data(deckspan , "identifiers").guid);
									checkSubmitDeck(v.id,v.guid);
								});
							}
							
						});
					} else {
						showMessage('deck'+xfetchListOfDecks+'Message','error',"You have no saved decks!");
						$("#deck"+xfetchListOfDecks).empty();
						$("#deck"+xfetchListOfDecks).append('<li>No saved decks were found.</li>');
							
					}
			  	} else if (msg.status=='error'){

			  		showMessage('deck'+xfetchListOfDecks+'Message','error',msg.errorMessage);
			  	}
				
			}).fail(function (jqXHR, textStatus){
				//console.log(textStatus);
				//console.log(jqXHR);
			}).always(function (jqXHR,textStatus){ahGlobals.ajaxDeckManipulationInProgress = false;});
		}
	}
	
	//When a ceck is chosen to be submitted this shows a preview of it and asks for the user to confirm they want to submit
	function checkSubmitDeck(did,dguid){
		if(!ahGlobals.ajaxDeckManipulationInProgress && ipb.vars['member_id'] != 0){
			var surl= ipb.vars['base_url']+'app=ccs&module=ajax&section=ahdeckbuilder&do=checkforsubmission',
			formData = [];
			formData.push({name: 'fPage', value: 'deckbuilder'});
			formData.push({name: 'fgame', value: ahGlobals.whichGame});
			formData.push({name: 'md5check', value: ipb.vars['secure_hash']});
			formData.push({name: 'did', value: did});
			formData.push({name: 'dguid', value: dguid});
			$.ajax({
				type: "POST",
				url: surl,
				beforeSend: function ( xhr ) {
					ahGlobals.ajaxDeckManipulationInProgress = true;
				},
				data: formData,
				dataType: "json"
			}).done(function( msg ) {
				if (msg.status=='success'){
					$('#submitReviewDeckName').text(html_entity_decode(msg.deck[0].name,'ENT_QUOTES'));
					$('#submitReviewDeckFormat').text(msg.deck[0].format);
					$("#submitReviewDeckTeaser").text(html_entity_decode(msg.deck[0].teaser,'ENT_QUOTES').replace(/<br\s*\/?>/gi,'\n'));
					$("#submitReviewDeckStrategy").text(html_entity_decode(msg.deck[0].strategy,'ENT_QUOTES').replace(/<br\s*\/?>/gi,'\n'));
					$('#submitReviewDeckType').text(msg.deck[0].tags);

						var cardlist = [],
						combinedtxt = '',
						reviewCards = msg.deck[0].deckContents;
						$.each (reviewCards,function (k,v){
							var cardSection = '',
							cardLevel = '';
							if (v.wtype == ""){
								cardSection = v.type;
								if (v.type !="Investigator"){
									cardLevel = " ["+v.lvl+"] "
								}
							} else {
								cardSection = "Weakness";
							}
							if (typeof cardlist[cardSection] == "undefined"){
								cardlist[cardSection] = '';
							}
							cardlist[cardSection] += v.quantity+"x "+v.name+" "+cardLevel+" ("+v.set+")<br />";

						});
						$.each(ahGlobals.typelist,function(k,ty){
								combinedtxt  += '<br />'+ty+': <br />';
								if (typeof cardlist[ty] != "undefined")
									combinedtxt  +=cardlist[ty];
								else
									combinedtxt.innerHTML += '<br />';
						});
						combinedtxt = html_entity_decode(combinedtxt,'ENT_QUOTES');
						$('#submitReviewDeck').html(combinedtxt);
						$('#submitConfirm').off( "click" );
						$('#submitConfirm').data("did", msg.deck[0].deckid);
						$('#submitConfirm').data("dguid", msg.deck[0].deckGUID);
						$('#submitConfirm').one("click",function(){
							submitDeck($(this).data("did"),$(this).data("dguid"));
						});
						if (msg.deck[0].deckid = deckObject.deckid && !deckObject.saved){
							showMessage('deckSubmitMessage','error','This deck appears to be open and unsaved in the deckbuilder. If you submit it now it will not have any recent changes.');
						} else {
							showMessage('deckSubmitMessage','success',msg.message);
						}

					$('#deckSubmitReview').show();
				} else if (msg.status=='error'){
			  		showMessage('deckSubmitMessage','error',msg.errorMessage);
			  	}
			}).fail(function (jqXHR, textStatus){
				//console.log(textStatus);
				//console.log(jqXHR);
			}).always(function (jqXHR,textStatus){ahGlobals.ajaxDeckManipulationInProgress = false;}); /**/
		}
	}

	//submits a deck to be displayed publically on the website.
	function submitDeck(did,dguid){
		
		if(!ahGlobals.ajaxDeckManipulationInProgress && ipb.vars['member_id'] != 0){
			var surl= ipb.vars['base_url']+'app=ccs&module=ajax&section=ahdeckbuilder&do=submit',
			formData = [];
			formData.push({name: 'fPage', value: 'deckbuilder'});
			formData.push({name: 'fgame', value: ahGlobals.whichGame});
			formData.push({name: 'md5check', value: ipb.vars['secure_hash']});
			formData.push({name: 'did', value: did});
			formData.push({name: 'dguid', value: dguid});
			$.ajax({
				type: "POST",
				url: surl,
				beforeSend: function ( xhr ) {
					ahGlobals.ajaxDeckManipulationInProgress = true;
				},
				data: formData,
				dataType: "json"
			}).done(function( msg ) {
				if (msg.status=='success'){
					showMessage('deckSubmitMessage','success',msg.message);
					$( "#messagedialog" ).dialog("open");
				} else if (msg.status=='error'){
			  		showMessage('deckSubmitMessage','error',msg.errorMessage);
			  	}
			}).fail(function (jqXHR, textStatus){
				//console.log(textStatus);
				//console.log(jqXHR);
			}).always(function (jqXHR,textStatus){ahGlobals.ajaxDeckManipulationInProgress = false;}); /**/
		}
	}

	//displays a link on the screen a user can use to share a private deck.
	function linkDeck(did,dguid){
		prompt("Copy the link to your deck", ahGlobals.deckSharePage+"?p="+ipb.vars['member_id']+"&deck="+dguid+"&c="+did);
	}

	//deletes a saved deck
	function deleteDeck(did,dguid){
		if(!ahGlobals.ajaxDeckManipulationInProgress && ipb.vars['member_id'] != 0){
			var surl= ipb.vars['base_url']+'app=ccs&module=ajax&section=ahdeckbuilder&do=delete',
			formData = [];
			formData.push({name: 'fPage', value: 'deckbuilder'});
			formData.push({name: 'fgame', value: ahGlobals.whichGame});
			formData.push({name: 'md5check', value: ipb.vars['secure_hash']});
			formData.push({name: 'did', value: did});
			formData.push({name: 'dguid', value: dguid});
			$.ajax({
				type: "POST",
				url: surl,
				beforeSend: function ( xhr ) {
					ahGlobals.ajaxDeckManipulationInProgress = true;
				},
				data: formData,
				dataType: "json"
			}).done(function( msg ) {
				if (msg.status=='success'){
					$('#liUserDeck'+msg.deckID).fadeOut(400,function(){this.remove()});
					showMessage('deckLoadMessage','success',msg.message);
				} else if (msg.status=='error'){
			  		showMessage('deckLoadMessage','error',msg.errorMessage);
			  	}
			}).fail(function (jqXHR, textStatus){
			}).always(function (jqXHR,textStatus){ahGlobals.ajaxDeckManipulationInProgress = false;}); 
		}
	}
	
	//loads an online saved deck
	function loadDeck(did,dguid){
		if(!ahGlobals.ajaxDeckManipulationInProgress && ipb.vars['member_id'] != 0){
			var surl= ipb.vars['base_url']+'app=ccs&module=ajax&section=ahdeckbuilder&do=loadDeck',
			formData = [];
			formData.push({name: 'fPage', value: 'deckbuilder'});
			formData.push({name: 'fgame', value: ahGlobals.whichGame});
			formData.push({name: 'md5check', value: ipb.vars['secure_hash']});
			formData.push({name: 'did', value: did});
			formData.push({name: 'dguid', value: dguid});
			$.ajax({
				type: "POST",
				url: surl,
				beforeSend: function ( xhr ) {
					ahGlobals.ajaxDeckManipulationInProgress = true;
				},
				data: formData,
				dataType: "json"
			}).done(function( msg ) {
			  	if (msg.status=='success'){
			  		var resultingAction =[],
			  		myid = "",
			  		mytags,
			  		searcharray = [],
			  		returnedDeck,
			  		returnedInvestigator,
			  		deckrecords,
			  		deckinvestigator,
			  		mystrat,
			  		myteaser,
			  		mycampaign,
			  		mynotes,
			  		myPhysicalTrauma,
			  		myMentalTrauma,
			  		myUnspentXP,
			  		deckOptions,
			  		deckRestrictions,
			  		mydname;

			  		returnedDeck = msg.deck[0];
			  		resetDeck(true,false,false);
			  		mystrat = html_entity_decode(returnedDeck.strategy,'ENT_QUOTES');
			  		myteaser = html_entity_decode(returnedDeck.teaser,'ENT_QUOTES');
			  		mydname = html_entity_decode(returnedDeck.name,'ENT_QUOTES');
			  		mycampaign = html_entity_decode(returnedDeck.campaign,'ENT_QUOTES');
			  		mynotes = html_entity_decode(returnedDeck.notes,'ENT_QUOTES');
			  		myPhysicalTrauma = html_entity_decode(returnedDeck.physicaltrauma,'ENT_QUOTES');
			  		myMentalTrauma = html_entity_decode(returnedDeck.mentaltrauma,'ENT_QUOTES');
			  		myUnspentXP = html_entity_decode(returnedDeck.unspentxp,'ENT_QUOTES');
			  		mytags = returnedDeck.tags.split(',');

			  		ahGlobals.deckname = mydname;

			  		$("#decktype").select2("val", "");
			  		$("#decktype").select2("val", mytags ); 
			  		$('#deckName').val(mydname );
			  		$('#textareaStrategy').val(mystrat);
			  		$('#textareaTeaser').val(myteaser);
			  		$('#spanDeckName').text(html_entity_decode(returnedDeck.name,'ENT_QUOTES'));
			  		$('#campaignName').val(html_entity_decode(returnedDeck.campaign,'ENT_QUOTES'));
					$('#campaignNotes').val(mynotes);
					$('#physicalTrauma').val(myPhysicalTrauma)
					$('#mentalTrauma').val(myMentalTrauma)
					$('#unspentExperience').val(myUnspentXP)

					var start = new Date().getTime();

					$.each(returnedDeck['deckContents'], function(k,v){
						if (v.type == "Investigator"){
							returnedInvestigator = v.id;
						} else {
							searcharray.push(v.id);
						}
					});
					resultingAction = [];
					deckinvestigator = ahGlobals.db({id:returnedInvestigator}).order('name').get();
					$.each(deckinvestigator,function(k,v){
						resultingAction.push(adjustDeckObject ('', 1, v));
						deckOptions = v.dopt;
						deckRestrictions = v.dres;
						ahGlobals.deckbuildingMaxCards = v.dsize;
					});
					deckbuildingSearchRulesParse(deckOptions, deckRestrictions);

					deckrecords = ahGlobals.db({id:searcharray}).order('name').get();
					$.each(deckrecords,function(k,v){
						deckquantity = returnedDeck.deckContents[v.id].quantity;
						resultingAction.push(adjustDeckObject ('', parseInt(deckquantity), v));
					});
					$("#maxDeckCards").text(ahGlobals.deckbuildingMaxCards);
					
					adjustDeckHTML(resultingAction);
					//finished all deck insertion
					var end = new Date().getTime();
					var time = end - start;
					//console.log('Execution time (ms): ' , time);
					
			  		deckObject.deckid = returnedDeck.deckid;
			  		deckObject.deckguid = returnedDeck.deckGUID;
			  		deckObject.saved = true;
					deckObject.deckname = mydname;
			  		deckObject.deckteaser = myteaser;
			  		deckObject.deckstrategy = mystrat;
			  		deckObject.deckcampaign = mycampaign;
			  		deckObject.decknotes = mynotes;
			  		deckObject.deckphysicaltrauma = myPhysicalTrauma;
			  		deckObject.deckmentaltrauma = myMentalTrauma;
			  		deckObject.deckunspentxp = myUnspentXP;
					sampleHand();
					adjustStatsHTML();
					populateCharts(deckObject,false);
					initializeCards();
					toggleSearchView('search');
			  		$('#spanDeckSavedStatus').text("Deck Successfully Loaded").removeClass('decksavered').addClass('decksavegreen');
			  		$('#buttonUpdateDeck').prop("disabled", false);
					selectedTabIndex = deckTabs.tabs( "option", "active");
					deckTabs.find( ".ui-tabs-nav li:eq("+selectedTabIndex+") a" ).trigger('click');
			  	} else if (msg.status=='error'){
			  		showMessage('deckLoadMessage','error',msg.errorMessage);
			  	}
				
			}).fail(function (jqXHR, textStatus){
				//console.log(textStatus);
				//console.log(jqXHR);
			}).always(function (jqXHR,textStatus){ahGlobals.ajaxDeckManipulationInProgress = false;$( "#loadingmessage" ).dialog("close")}); /**/
		}
	}

	//saves or updates a deck
	deckContents.prototype.saveDeck = function (xsaveDeck){
		var elementClicked = xsaveDeck.target.id,
		surl,
		saveDGuid,
		saveDID,
		factioninfo,
		deckjsontext,
		deckinvestigatortext,
		$deckName = $('#deckName').val().stripScripts().stripTags(),
		$deckTeaser = $('#textareaTeaser').val().stripScripts().stripTags(),
		$deckStrategy = $('#textareaStrategy').val().stripScripts().stripTags(),
		$deckCampaign = $('#campaignName').val().stripScripts().stripTags(),
		$deckNotes = $('#campaignNotes').val().stripScripts().stripTags(),
		$deckPhysicalTrauma = $('#physicalTrauma').val().stripScripts().stripTags(),
		$deckMentalTrauma = $('#mentalTrauma').val().stripScripts().stripTags(),
		$deckUnspentXP = $('#unspentExperience').val().stripScripts().stripTags(),
		formData = [],
		deckjson = {},
		deckclass = '',
		deckInvestigator = {};
		if (elementClicked == "buttonUpdateDeck"){
			surl= ipb.vars['base_url']+'app=ccs&module=ajax&section=ahdeckbuilder&do=update';
			saveDGuid = deckObject.deckguid;
			saveDID = deckObject.deckid;
		} else if (elementClicked == "buttonSaveNewDeck"){
			surl= ipb.vars['base_url']+'app=ccs&module=ajax&section=ahdeckbuilder&do=save';
			saveDGuid = ""
			saveDID = 0;
		} else {
			showMessage('deckSaveMessage','error','Error Unable to Save.');
			return(false);
		}

		if (this.saved && $deckName==this.deckname && $deckTeaser==this.deckteaser && $deckStrategy == this.deckstrategy && $deckCampaign == this.deckcampaign && $deckNotes == this.decknotes && $deckPhysicalTrauma == this.deckphysicaltrauma && $deckMentalTrauma == this.deckmentaltrauma && $deckUnspentXP == this.deckunspentxp){
			showMessage('deckSaveMessage','error','This deck has not been changed since the last time it was saved.');
			return(false);
		}
		if (!navigator.onLine){
			showMessage('deckSaveMessage','error','No Internet connection detected. Please connect to the internet to save this deck.');
			return(false);
		}
		if (this.totalCards <= 0){
			showMessage('deckSaveMessage','error','Your deck is empty. Please add cards to your deck to save it.');
			return(false);
		}
		if ($deckName.length<=0){
			showMessage('deckSaveMessage','error','You must name your deck in order to save it.');
			return(false);
		}
		if (!$("#decktype option:selected").length){
			showMessage('deckSaveMessage','error','At least one deck type must be selected.');
			return(false);
		}

		$.each(deckObject.cards,function(k,v){
			deckjson[v.id]={"id":v.id,"quantity":v.deckquantity,"name":v.name,"set":v.setname,"type":v.type,"wtype":v.wtype,"lvl":v.lvl};
			if (v.type == 'Investigator'){
				deckInvestigator[v.id]={"id":v.id,"name":v.name,"set":v.setname,"type":v.type,"imgf":v.imgf,"imgb":v.imgb};
				deckclass = v.clss;
			}
		});
		deckjsontext = Object.toJSON(deckjson);
		deckinvestigatortext = Object.toJSON(deckInvestigator);

		formData.push({name: 'fUpdate', value: elementClicked });
		formData.push({name: 'fPage', value: 'deckbuilder'});
		formData.push({name: 'fgame', value: ahGlobals.whichGame});
		formData.push({name: 'dGUID', value: saveDGuid });
		formData.push({name: 'dPK', value: saveDID });
		formData.push({name: 'dname', value: $deckName});
		formData.push({name: 'dteaser', value: $deckTeaser});
		formData.push({name: 'dstrat', value: $deckStrategy});
		formData.push({name: 'md5check', value: ipb.vars['secure_hash']});
		formData.push({name: 'dtype', value: $('#decktype').val().join()});
		formData.push({name: 'dcontent', value: deckjsontext });
		formData.push({name: 'dinvestigator', value: deckinvestigatortext });
		formData.push({name: 'dclass', value: deckclass });
		formData.push({name: 'dcampaign', value: $deckCampaign });
		formData.push({name: 'dnotes', value: $deckNotes });
		formData.push({name: 'dphysicaltrauma', value: $deckPhysicalTrauma });
		formData.push({name: 'dmentaltrauma', value: $deckMentalTrauma });
		formData.push({name: 'dunspentxp', value: $deckUnspentXP });
		//console.log(formData);

		if(!ahGlobals.ajaxDeckManipulationInProgress){
		//if (0==1){
			$.ajax({
				type: "POST",
				url: surl,
				beforeSend: function ( xhr ) {
					
					$('#spanDeckSavedStatus').text("!Save In Progress!");
					ahGlobals.ajaxDeckManipulationInProgress = true;
				},
				data: formData,
				dataType: "json"
			}).done(function( msg ) {
			  	if (msg.status=='success'){
			  		$('#spanDeckSavedStatus').text("Deck Saved!").removeClass('decksavered').addClass('decksavegreen');
			  		$('#spanDeckName').text(html_entity_decode(msg.deckName,'ENT_QUOTES'));
					ahGlobals.deckname = msg.deckName;
			  		showMessage('deckSaveMessage','success',"Deck Saved!");
			  		deckObject.saved = true;
			  		deckObject.deckid = msg.deckID;
			  		deckObject.deckguid = msg.deckGUID;
			  		deckObject.deckname = $deckName;
			  		deckObject.deckteaser = $deckTeaser;
			  		deckObject.deckstrategy = $deckStrategy;
			  		deckObject.deckcampaign = $deckCampaign;
			  		deckObject.decknotes = $deckNotes;
			  		deckObject.deckphysicaltrauma = $deckPhysicalTrauma;
			  		deckObject.deckmentaltrauma = $deckMentalTrauma;
			  		deckObject.deckunspentxp = $deckUnspentXP;
			  		$('#buttonUpdateDeck').prop("disabled", false);
			  		$( "#messagedialog" ).dialog("open");
			  	} else if (msg.status=='error'){
			  		
			  		$('#spanDeckSavedStatus').text("Deck Not Saved!").removeClass('decksavegreen').addClass('decksavered');
			  		showMessage('deckSaveMessage','error',msg.errorMessage);
			  		deckObject.saved = false;
			  	}
				
			}).fail(function (jqXHR, textStatus){
				//console.log(textStatus);
				//console.log(jqXHR);
			}).always(function (jqXHR,textStatus){ahGlobals.ajaxDeckManipulationInProgress = false;}); /**/
			
		}
	}

	//Saves a deck to local storage
	function saveToLocal(_deckName){
		var factioninfo,
		saveLocalDeckObject = {},
		deckname,
		successful;
		if ($('#deckName').val().length<=0){
			showMessage('deckSaveMessage','error','You must name your deck in order to save it.');
			return(false);
		}
		if (deckObject.totalCards <= 0){
			showMessage('deckSaveMessage','error','Your deck is empty. Please add cards to your deck to save it.');
			return(false);
		}

		if (window.localStorage) {
			saveLocalDeckObject.deckContents = {};
			$.each(deckObject.cards,function(k,v){
				saveLocalDeckObject.deckContents[v.id]={"id":v.id,"deckquantity":v.deckquantity,"type":v.type};
			});

			deckname = $('#deckName').val().stripScripts().stripTags().replaceAll('|','')
			deckname = deckname.replaceAll('&', '&amp;')
					            .replaceAll('"', '&quot;')
					            .replaceAll("'", '&#39;')
					            .replaceAll('<', '&lt;')
					            .replaceAll('>', '&gt;');
			if (ahGlobals.loadedLocalSave != ""){
				if (ahGlobals.loadedLocalSave.indexOf(ahGlobals.whichGame+'|'+deckname) != -1)
					deleteFromLocal(ahGlobals.loadedLocalSave);
			}
			saveLocalDeckObject.name = deckname;
			saveLocalDeckObject.savetime = new Date().getTime();
			successful = true;
			try {
			    localStorage.setItem(ahGlobals.whichGame+'|'+deckname+'|'+saveLocalDeckObject.savetime,JSON.stringify(saveLocalDeckObject));
			} catch(e) {
				if (e.name === 'QUOTA_EXCEEDED_ERR' || e.name === 'NS_ERROR_DOM_QUOTA_REACHED') {
					showMessage('deckSaveMessage','error','Your Local Storage for this website is currently full.');
					return(false);
				} else {
					//displayFromLocal();
				}
				successful = false;
			}
			if (successful){
				showMessage('deckSaveMessage','success',"Deck saved to your browsers storage!");
			}
			displayFromLocal();
			
		} else {
			showMessage('deckSaveMessage','error','Your browser does not support local storage.');
			return(false);
		}

	}

	//loads a deck from local storage
	function loadFromLocal(_key){
		_key = _key.replaceAll('&', '&amp;')
		            .replaceAll('"', '&quot;')
		            .replaceAll("'", '&#39;')
		            .replaceAll('<', '&lt;')
		            .replaceAll('>', '&gt;');
		var deckquantity;
		if (window.localStorage) {
			resetDeck(true,false,false);
			var start = new Date().getTime();
			ahGlobals.loadedLocalSave = _key;
			var localSavedDeck = localStorage.getItem(_key),
			resultingAction =[],
			searcharray = [],
			returnedInvestigator,
			deckinvestigator,
			returnedDeck;
			returnedDeck = JSON.parse(localSavedDeck);
			//console.log(returnedDeck);
			$('#deckName').val(html_entity_decode(returnedDeck.name,'ENT_QUOTES'));
			$('#spanDeckName').text(html_entity_decode(returnedDeck.name,'ENT_QUOTES'));

			$.each(returnedDeck['deckContents'], function(k,v){
				if (v.type == "Investigator"){
					returnedInvestigator = v.id;
				} else {
					searcharray.push(v.id);
				}
			});
			resultingAction = [];
			deckinvestigator = ahGlobals.db({id:returnedInvestigator}).order('name').get();
			$.each(deckinvestigator,function(k,v){
				resultingAction.push(adjustDeckObject ('', 1, v));
				deckOptions = v.dopt;
				deckRestrictions = v.dres;
				ahGlobals.deckbuildingMaxCards = v.dsize;
			});
			deckbuildingSearchRulesParse(deckOptions, deckRestrictions);

			deckrecords = ahGlobals.db({id:searcharray}).order('name').get();

			$.each(deckrecords,function(k,v){
				deckquantity = returnedDeck.deckContents[v.id].deckquantity;
				resultingAction.push(adjustDeckObject ('', parseInt(deckquantity), v));
				if (v.type == 'Investigator'){
					deckOptions = v.dopt;
					deckRestrictions = v.dres;
					ahGlobals.deckbuildingMaxCards = v.dsize;
				}
			});
			$("#maxDeckCards").text(ahGlobals.deckbuildingMaxCards);

			adjustDeckHTML(resultingAction);
			//finished all deck insertion
			sampleHand();
			adjustStatsHTML();
			populateCharts(deckObject,false);
			initializeCards();
			toggleSearchView('search');
			$('#spanDeckSavedStatus').text("Deck Successfully Loaded").removeClass('decksavered').addClass('decksavegreen');
			selectedTabIndex = deckTabs.tabs( "option", "active");
			deckTabs.find( ".ui-tabs-nav li:eq("+selectedTabIndex+") a" ).trigger('click');
			var end = new Date().getTime();
			var time = end - start;
			//console.log('Execution time (ms): ' , time);
			$('html, body').animate({
                        scrollTop: $("#deckbuilderWrapper").offset().top
                    }, 500);
		}
	}

	//delete a deck from local storage
	function deleteFromLocal(_key){
		_key = _key.replaceAll('&', '&amp;')
		            .replaceAll('"', '&quot;')
		            .replaceAll("'", '&#39;')
		            .replaceAll('<', '&lt;')
		            .replaceAll('>', '&gt;');
		if (window.localStorage) {
			localStorage.removeItem(_key);
			displayFromLocal();
		}
	}

	//Show list decks in local storage
	function displayFromLocal(){
		var localDeckExists = false,
		displayObject = [],
		displaytime,
		key;
		$('#savedLocalStorage').empty();
		if (window.localStorage) {
			for(key in localStorage) {
				keyArray = key.split("|");
				if (keyArray[0] == ahGlobals.whichGame){
					displaytime =  new Date(parseInt(keyArray[2])).toLocaleString();
					displayObject.push({time:keyArray[2],html:'<li><div style="display: table-cell;width: 345px;vertical-align: middle;"><span style="cursor:pointer;font-weight:bold;font-size:large;" class="loadLocal" data-lskey="'+key+'">'+keyArray[1]+ '</span><br />'+displaytime+'</div><div style="display: table-cell;width:40px;"><img class="deleteLocal" data-lskey="'+key+'" style="cursor:pointer;margin-right:5px;" src="'+deckbuilderGlobals.image_path +'/images/delete-icon.png"></div></li>'});
					localDeckExists = true;
					
				}
			}
			if (localDeckExists){
				displayObject.sort(dynamicSort('time'));
				$.each(displayObject.reverse(),function(){
					$("#savedLocalStorage").append(this.html);
				});
			} else {
				$("#savedLocalStorage").append("<li>No Locally Saved Decks Found.</li>");
			}
			
		}
	}

	//Shows or hides the Warlord selection area
	function toggleSearchView(showWhat){
		if (showWhat == 'search'){
			$("#investigatorDiv").fadeOut(500,function(){
				$("#searchandresultsDiv").fadeIn(500,function(){
					$("#resultsDiv").trigger('scroll');
				});
			});
		} else {
			$("#searchandresultsDiv").fadeOut(500,function(){$("#investigatorDiv").fadeIn(500);});
		}
	}
	
	//generates sample hand
	function sampleHand(){
		var startingHandSize = 7,
		i;
		if (deckObject.startingHandSize != 0){
			startingHandSize = deckObject.startingHandSize;
		}
		ahGlobals.cardArrayCopy = [];
		$.each (deckObject.cards,function (k,v){
			if (v.type != 'Investigator')
			{
				for (i=0;i<v.deckquantity;i++)
						ahGlobals.cardArrayCopy.push(v.name +' ['+v.lvl+'] ('+v.setname+')');
			}
		});	
		
		var hand = shuffle(ahGlobals.cardArrayCopy).splice(0,startingHandSize).join("<br />");
		$('#sampleHand').html(hand);	
	
	}

	$("#drawMoreSampleHand").on("click",function(){
		if (ahGlobals.cardArrayCopy.length){
			var drawncard = ahGlobals.cardArrayCopy.pop();
			$("#sampleHand").prepend(drawncard+"<br />");
		}
		
	})

	//generates text file content and url and popup bbcode
	function generateExport(bbcodeType){
		if (deckObject.totalCards > 0){
			var cardlist = [],
			markdownref = '',
			markdownnumber = 1,
			combinedtxt  = '';
			factionOCTGN = '';
			$('#taPopupBBCode').show();
			$('#taUrlBBCode').show();
			$('#spUrlBBCode,#spPopupBBCode').text("Copy and Paste the below code.");
			var combinedtxt  = '';
			if (bbcodeType == "text"){
				combinedtxt += 'Deck Created with '+ahGlobals.deckbuilderTitle+'\n\n';
			}else if (bbcodeType == "markdown") {
				combinedtxt += 'Deck Created with ['+ahGlobals.deckbuilderTitle+']('+ahGlobals.deckbuilderURL+')\n\n';
			} else {
				combinedtxt += 'Deck Created with  '+'[URL='+ahGlobals.deckbuilderURL+']'+ahGlobals.deckbuilderTitle+'[/URL]\n\n';
			}

			combinedtxt += '\nTotal Cards: ('+deckObject.totalCards+')\n';

			$.each (deckObject.cards,function (k,v){
				var cardSection = '',
				cardLevel = '';
				if (v.wtype == ""){
					cardSection = v.type;
					if (v.type !="Investigator"){
						cardLevel = "["+v.lvl+"] ";
					}
				} else {
					cardSection = "Weakness";
				}
				if (typeof cardlist[cardSection] == "undefined"){
					cardlist[cardSection] = '';
				}
				if (bbcodeType == "popup"){
					cardlist[cardSection] += v.deckquantity+"x [ah]"+html_entity_decode(v.name,'ENT_QUOTES')+" ("+v.setname+")[/ah] \n";
				}  else if (bbcodeType == "markdown") {

					cardlist[cardSection] += v.deckquantity+"x ["+html_entity_decode(v.name,'ENT_QUOTES')+" "+cardLevel+"("+v.setname+")] ["+markdownnumber+"]  \n";
					markdownref += "["+markdownnumber+"]: "+v.fullurl+" ("+ v.name+") \n";
					markdownnumber++;
				}
				else if (bbcodeType == "text"){
					cardlist[cardSection] += v.deckquantity+"x "+html_entity_decode(v.name,'ENT_QUOTES')+" "+cardLevel+"("+v.setname+")\n";
				}
				else{
					cardlist[cardSection] +=  v.deckquantity+"x [URL="+v.fullurl+"]"+html_entity_decode(v.name,'ENT_QUOTES')+" "+cardLevel+"("+v.setname+")[/URL]\n";
				}


			});
			deckObject[ahGlobals.whichGame].typeList.each(function(ty){
							
				combinedtxt  += '\n'+ty+': ('+deckObject[ahGlobals.whichGame].counts[ty.toLowerCase().replace(/ /g, '').replace(/-/g, '')]+')  \n';
				if (typeof cardlist[ty] != "undefined"){
					combinedtxt  +=cardlist[ty];
				 } else { 
					combinedtxt += '\n';
				}
				
			});

			if (bbcodeType == 'markdown'){
				combinedtxt += "\n\n"+markdownref;
			}
					
			if (bbcodeType == "popup"){
				return (combinedtxt);
			} else if (bbcodeType == "text"){
				return (combinedtxt);
			} else if (bbcodeType == 'octgn') {
				return (octgnXMLs+factionOCTGN+combinedtxt+octgnXMLe);
			} else {
				return (combinedtxt);
			}
		} else {
			return("Decks must have at least one card to export.");
		}
	}

	$('#fileinput').change(readSingleFile);

	function readSingleFile(evt) {
        //Retrieve the first (and only!) File from the FileList object
        var f = evt.target.files[0],
        XML= true,
        xmlDoc; 

        if (f) {
			var r = new FileReader();
			r.onload = function(e) { 
				var contents = e.target.result;
				$('#taImportDeck').val(contents);

			}
			r.readAsText(f);
			

        } else { 
          $("#divImportIssues").text("Failed to load file.");
        }

	}

	//Parses some text to attempt to import a deck
	function importdeck(){
		//resetDeck(false,false)
		var isXML = true,
		importSide = '',
		issues = "",
		matched = false,
		factionmatched = false,
		foundCards = {},
		foundInvestigator = {},
		foundWeakness = {},
		foundFactions = [],
		resultingAction = [],
		importOK = true,
		allyfaction = '',
		searcharray = [],
		start1 = new Date().getTime(),
		deckrecords,
		investigatorsFound = 0,
		deckpossiblecards;


			
		$.each($('#taImportDeck').val().split(/\n/), function(i, line){
			line = line.trim();
		   if(line && line.length){
			  matched = false;
			  
			  if(line.match(/\dx\s\[url\="?(.*?)"?\](.*?)\((.*?)\)\[\/url\]/gi)){
					var myregex = /(\d+)x\s\[url\="?(.*?)"?\](.*?)\s\((.*?)\)\[\/url\]/gi;
					var match = myregex.exec(line);
					var sname = htmlentities(match[3],"ENT_QUOTES");
					var sset = htmlentities(match[4],"ENT_QUOTES");
					var squantity = parseInt(match[1]);
					matched = true;
			   }/* */

			   if(line.match(/([0-9]+)x\s(?!\[url=)(.*?)\s\((.*?)\)/gi)){
					var myregex = /([0-9]+)x\s(?!\[url=)(.*?)\s\((.*?)\)/gi;
					var match = myregex.exec(line);
//					console.log(match);
					var sname = htmlentities(match[2],"ENT_QUOTES").replaceAll('&amp;quot;', '&quot;');
					var sset = htmlentities(match[3],"ENT_QUOTES");
					var squantity = parseInt(match[1]);
					matched = true;
			   }

			   //console.log(line);
			   if (matched){
			   	   if (sname.substring(sname.length-1) == "]"){ //this card has a level
			   	   		var slevel = {lvl:sname.substring(sname.length-2,sname.length-1)};
			   	   		sname = sname.substring(0,sname.length-4);
			   	   } else {
			   	   		var slevel = {};
			   	   }
				   var searched = $.extend(true, {}, ahGlobals.db({name:sname},{setname:sset},slevel).first());
				   if (searched){
				   		if (searched.type == 'Investigator' || searched.wtype == 'Weakness'  || searched.wtype == 'Basic Weakness'){
				   			if (searched.type == 'Investigator'){
				   				searched.deckquantity = 1;
				   				investigatorsFound++;
				   				foundInvestigator["card"]=searched;
				   				foundInvestigator["ID"] = searched.id;
				   				foundInvestigator["dopt"] = searched.dopt;
				   				foundInvestigator["dres"] = searched.dres;
				   				foundInvestigator["dsize"] = searched.dsize;
				   				foundInvestigator["relatedcardsSearchString"] =  searched.name + ' (' + searched.setname + ')'.replaceAll('&', '&amp;')
						            .replaceAll('"', '&quot;')
						            .replaceAll("'", '&#39;')
						            .replaceAll('<', '&lt;')
						            .replaceAll('>', '&gt;');
				   			} else {
				   				searched.deckquantity = 1;
				   				foundWeakness[searched.id]=searched;
				   			}

				   				
				   		} else {
				   			searched.deckquantity = squantity;
				   			foundCards[searched.id]=searched;
				   		}

					} else {
						issues += sname+ ' ('+sset+') x'+squantity+' could not be found.\n';
					}
				}

		   }
		});
			
		
		//console.log('Found Investigator: ' ,foundInvestigator);
		//console.log('Found Weakness: ' ,foundWeakness);
		//console.log('Found Cards: ' ,foundCards);
		if (investigatorsFound > 1){
			issues = 'Multiple Investigators found. Only one Investigator may be in a deck. Import Aborted\n'+issues;
			importOK = false;
		} else if (investigatorsFound < 1 ){
			issues = 'No Investigators Found. An Investigator must be listed in order to import a deck. Import Aborted\n'+issues;
			importOK = false;
		} else {
			
		}

		if (importOK){
			resetDeck(true,false,false);
			deckbuildingSearchRulesParse(foundInvestigator["dopt"], foundInvestigator["dres"]);
			$("#maxDeckCards").text(foundInvestigator["dsize"]);
			$.each(foundCards, function(k,v){
				if (v.creqf == ""){
					deckpossiblecards = ahGlobals.db(ahGlobals.deckbuildingSearchRules,{id:k}).order('name').get();
					searcharray.push(v.id);
				}
				
			});
			$.each(foundWeakness, function(k,v){
				if (v.wtype == "Basic Weakness"){
					searcharray.push(v.id);
				} else if (v.wtype == "Weakness"){

				}
				
			});
			var requiredforInvestigator = ahGlobals.db([{creqf:foundInvestigator["relatedcardsSearchString"]}]).order('name').get();
			$.each(requiredforInvestigator, function(k,v){
				searcharray.push(v.id);
				
			});
			searcharray.push(foundInvestigator["ID"]);

			resultingAction = [];

			deckrecords = ahGlobals.db({id:searcharray}).order('name').get();
			$.each(deckrecords,function(k,v){
				var deckquantity;
				if (typeof foundCards[v.id] == "object"){
					deckquantity = foundCards[v.id].deckquantity;
				} else {
					deckquantity = v.max;
				}
				
				resultingAction.push(adjustDeckObject ('', parseInt(deckquantity), v));
			});
			adjustDeckHTML(resultingAction);

			sampleHand();
			adjustStatsHTML();
			populateCharts(deckObject,false);
			initializeCards();
			toggleSearchView('search');
		}
		$('#divImportIssues').text(issues);


		var end1 = new Date().getTime();
		var time1 = end1 - start1;
		//console.log('total Execution time (ms): ' , time1);

	}

	function populateCharts(deckObjectToParse,clearCharts){
		if (clearCharts){
			var deckStatsArray = [['Card Values',{'type': 'string', 'role': 'tooltip', 'p': {'html': true}}, 'Cost']];
			deckStatsArray.push(["0",createCustomDataStatsTooltip(0,0),0]);
			var dataStats = google.visualization.arrayToDataTable(deckStatsArray);
			new google.visualization.ColumnChart(document.getElementById('chart4')).
				draw(dataStats,
					{title:"Costs",
					chartArea: {'width': '70%', 'height': '70%'},
					titleTextStyle:{fontSize: 18},
					vAxis: {viewWindowMode: "explicit", viewWindow:{ min: 0 }},
					focusTarget: 'category',
					tooltip: { isHtml: true },
					width:550, height:400,
					legend: {position: 'top',maxLines:3},
					vAxis: {title: "Number of Cards"},
					hAxis: {title: "Card Costs"}}
				);

			var dataCardTypes = google.visualization.arrayToDataTable([
				['Type', 'Number of Cards'],
				['-', 0]
			]);
			var dataClasses = google.visualization.arrayToDataTable([
				['Classes', 'Number of Cards'],
				['-', 0]
			]);
			new google.visualization.PieChart(document.getElementById('chart5')).
				draw(dataCardTypes, {title:"Card Types",
									chartArea: {'width': '90%', 'height': '70%'},
									sliceVisibilityThreshold:0,
									titleTextStyle:{fontSize: 18},
									width:300,
									legend:{position: 'top', maxLines:'7'}});
			new google.visualization.PieChart(document.getElementById('chart6')).
				draw(dataClasses, {title:"Icons",
									chartArea: {'width': '90%', 'height': '70%'},
									sliceVisibilityThreshold:0,
									titleTextStyle:{fontSize: 18},
									width:300,
									legend:{position: 'top', maxLines:'7'}});
		} else {

			var costlength = deckObjectToParse[ahGlobals.whichGame].costs.length;
			var deckStatsArray = [['Card Values',{'type': 'string', 'role': 'tooltip', 'p': {'html': true}}, 'Cost']];
			var maxChartStatsLength = costlength
			var _costs;
			for(i=0;i<maxChartStatsLength;i++){

				if(typeof deckObjectToParse[ahGlobals.whichGame].costs[i]!=='undefined' && deckObjectToParse[ahGlobals.whichGame].costs[i]!=null){
					_costs = deckObjectToParse[ahGlobals.whichGame].costs[i];
				} else {
					_costs = 0;
				}
				deckStatsArray.push([i.toString(),createCustomDataStatsTooltip(i,_costs),_costs]);
			}
			if (deckStatsArray.length <= 1){
				deckStatsArray.push(["0",createCustomDataStatsTooltip(0,0),0]);
			}
			var dataStats = google.visualization.arrayToDataTable(deckStatsArray);
			var dataClasses = google.visualization.arrayToDataTable([
				['Classes', 'Number of Cards'],
				['Guardian', deckObjectToParse[ahGlobals.whichGame].counts.guardian],
				['Mystic', deckObjectToParse[ahGlobals.whichGame].counts.mystic],
				['Rogue', deckObjectToParse[ahGlobals.whichGame].counts.rogue],
				['Seeker', deckObjectToParse[ahGlobals.whichGame].counts.seeker],
				['Survivor', deckObjectToParse[ahGlobals.whichGame].counts.survivor],
				['Neutral', deckObjectToParse[ahGlobals.whichGame].counts.neutral]
			]);

			var dataCardTypes = google.visualization.arrayToDataTable([
				['Type', 'Number of Cards'],

				['Event', deckObjectToParse[ahGlobals.whichGame].counts.event],
				['Asset', deckObjectToParse[ahGlobals.whichGame].counts.asset],
				['Skill', deckObjectToParse[ahGlobals.whichGame].counts.skill]
			]);

			new google.visualization.ColumnChart(document.getElementById('chart4')).
				draw(dataStats,
					{title:"Costs",
					chartArea: {'width': '80%', 'height': '70%'},
					titleTextStyle:{fontSize: 18},
					vAxis: {viewWindowMode: "explicit", viewWindow:{ min: 0 }},
					colors: [
					'#2EC1D1', 
					'#D12E41', 
					'#5E2059',
					'#42B023',
					'#E8C91A',
					'#D6A93E',
					'#CDC2C0'
					],
					focusTarget: 'category',
					tooltip: { isHtml: true },
					width:550, height:400,
					legend: {position: 'top',maxLines:3},
					vAxis: {title: "Number of Cards"},
					hAxis: {title: "Card Costs"}}
				);

			new google.visualization.PieChart(document.getElementById('chart5')).
				draw(dataCardTypes, {title:"Card Types",
									chartArea: {'width': '90%', 'height': '70%'},
									sliceVisibilityThreshold:0,
									titleTextStyle:{fontSize: 18},
									width:300,
									legend:{position: 'top', maxLines:'7'}});
			new google.visualization.PieChart(document.getElementById('chart6')).
				draw(dataClasses, {colors: [
										'#236da8', 
										'#4e4483', 
										'#165f3a',
										'#bc882d',
										'#93222c',
										'#383834'
									],
									title:"Classes",
									chartArea: {'width': '90%', 'height': '70%'},
									sliceVisibilityThreshold:0,
									titleTextStyle:{fontSize: 18},
									width:300,
									legend:{position: 'top', maxLines:'7'}});
			
		}

	}


	function createCustomDataStatsTooltip(_tot,_cst){
		return '<div style="padding:5px 5px 5px 5px;color:black;">' +
		'<table id="medals_layout">'+
		'<tr><td><div style="height: 1em;width: 1em;background:#D12E41;float: left;"></div>&nbsp;'+_cst+' cards with '+_tot+' Cost</td></tr>'+
		'</table>' + 
		'</div>';
	}

	//removes everything from deck and starts over.
	function resetDeck(closeTabs,showInvestigatorChoice,resetSearchOptions){
		if(typeof(closeTabs)==='undefined') closeTabs = true;
		if(typeof(showInvestigatorChoice)==='undefined') showInvestigatorChoice = true;
		if(typeof(resetSearchOptions)==='undefined') resetSearchOptions = true;
		ahGlobals.deckname = "";
		deckObject = new deckContents;
		$("#deckName").val("");
		$("#textareaTeaser").val("");
		$("#textareaStrategy").val("");
		$("#decktype").select2("val", "");
		$("#campaignName").val("");
		$("#campaignNotes").val("");
		$("#physicalTrauma").val("0");
		$("#mentalTrauma").val("0");
		$("#unspentExperience").val("0");
		$("#quickentry").val("");
		$("#maxDeckCards").text("0");
		$("#deckviewclasses>div").text("0");
		$("#deckInvestigator li:not(:first)").remove();
		$("#deckWeakness li:not(:first)").remove();
		$("#deckAsset li:not(:first)").remove();
		$("#deckSkill li:not(:first)").remove();
		$("#deckEvent li:not(:first)").remove();
		$("#deckSectionCost>ul.deckCards>li.deckCard").remove();
		$("#deckSectionCost>ul.deckCards").hide();
		adjustStatsHTML();
		populateCharts(null,true);
		$("span[id$='Count']").text("0");
		if (!closeTabs){
		
		} else {
			selectedTabIndex = deckTabs.tabs( "option", "active");
			deckTabs.find( ".ui-tabs-nav li:eq("+selectedTabIndex+") a" ).trigger('click');
		}
		if (showInvestigatorChoice){
			toggleSearchView('investigator');
		}
		$('#spanDeckSavedStatus').text("Deck Not Saved!").removeClass('decksavegreen').addClass('decksavered');
		$('#spanDeckName').text("New Unsaved Deck");
		$('#sampleHand').text("");

		$('#buttonUpdateDeck').prop("disabled", true);
		if (resetSearchOptions){
			resetSearch();
		}
	}

	//resets search
	function resetSearch(){

		$('#formQuickSearch')[0].reset();
		//setFactionCheckboxes(deckObject.faction,deckObject.ally)
		ahGlobals.filters.select2("val", ""); 
		if( !ahGlobals.mobile ) {
			ahGlobals.filters1.select2("val", ""); 
		}
		$("#filterSet option").removeAttr('disabled');
		ahGlobals.selectedsets = '';
		$('#filteredSets li').hide();
		filterCards(true);
	}

	//shows initial cards in search results
	function initializeCards(){
		filterCards(true);
	}

	//shows a message in a specified area
	function showMessage(area,type,message){
		$('#'+area).removeClass('ui-state-error ui-state-successmsg');
		if (type == "error"){
			$('#'+area).addClass('ui-state-error').text(message);
		} else {
			$('#'+area).addClass('ui-state-successmsg').text(message);
		}
		$("#messagedialogText").text(message);
	}

	function resizeDeckbuilderInterface (docWidth,forcesize){
		if (docWidth >= 1530) {
			
			changeDeckbuilderView("212");
		} else if (docWidth < 1530 && docWidth>=1200) {
			changeDeckbuilderView("202");
		} else if (docWidth < 1200 ) {
			changeDeckbuilderView("111");
		}
	}

/*	if (!$.support.leadingWhitespace) {
		//if ie version < 8 fallback jquery for @media selectors

		$(window).resize(function() {
			if (ahGlobals.widthTimer != null) 
				clearTimeout(ahGlobals.widthTimer);
			ahGlobals.widthTimer = setTimeout(function(){
				var width = $(window).width();
				resizeDeckbuilderInterface (width,false);
			}, 500);

		});
	}*/
	$('#hiddenLoading').fadeOut();
});

function zeroFill( number, width )
{
  width -= number.toString().length;
  if ( width > 0 )
  {
    return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
  }
  return number + ""; // always return a string
}

//Fisher-Yates Shuffle to randomize array
shuffle = function(o){
	for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	return o;
};

String.prototype.convertToBBCode = function(){
	var italics = '(<em class=["\']bbc[\'"]>|<i>)'
	return this.replace(new RegExp(italics,'gi'), "[i]").replace(/<br \/>/gi, "[br]");
}

//the following two functions strip Scripts and Tags from user input. This is done firstly because we dont want tags or scripts in input
// and it is also the first line of security to prevent xss attacks (not a very good one but better than nothing).
Function.prototype.stripScripts = function(exec){
    var scripts = '';
    var text = this.replace(/<script[^>]*>([\s\S]*?)<\/script>/gi, function(all, code){
        scripts += code + '\n';
        return '';
    });
    if (exec === true) Browser.exec(scripts);
    else if (typeOf(exec) == 'function') exec(scripts, text);
    return text;
};

String.prototype.replaceAll = function(str1, str2, ignore) 
{
	return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
};


Function.prototype.stripTags = function (whitelist) {
  whitelist= (((whitelist|| "") + "").toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('');
  var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
    commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
  return this.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
    return whitelist.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
  });
}

function dynamicSort(property) {
   	return function (a,b) {
   		return (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
	}
}

//Some older browsers do not have the array filter method. This adds it in.
if (!Array.prototype.filter)
{
  Array.prototype.filter = function(fun /*, thisp */)
  {
    "use strict";
 
    if (this == null)
      throw new TypeError();
 
    var t = Object(this);
    var len = t.length >>> 0;
    if (typeof fun != "function")
      throw new TypeError();
 
    var res = [];
    var thisp = arguments[1];
    for (var i = 0; i < len; i++)
    {
      if (i in t)
      {
        var val = t[i]; // in case fun mutates this
        if (fun.call(thisp, val, i, t))
          res.push(val);
      }
    }
 
    return res;
  };
}

if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
    "use strict";
    if (this == null) {
      throw new TypeError();
    }
    var t = Object(this);
    var len = t.length >>> 0;
 
    if (len === 0) {
      return -1;
    }
    var n = 0;
    if (arguments.length > 1) {
      n = Number(arguments[1]);
      if (n != n) { // shortcut for verifying if its NaN
        n = 0;
      } else if (n != 0 && n != Infinity && n != -Infinity) {
        n = (n > 0 || -1) * Math.floor(Math.abs(n));
      }
    }
    if (n >= len) {
      return -1;
    }
    var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
    for (; k < len; k++) {
      if (k in t && t[k] === searchElement) {
        return k;
      }
    }
    return -1;
  }
}

if (!String.prototype.startsWith) {
  String.prototype.startsWith = function(searchString, position) {
    position = position || 0;
    return this.indexOf(searchString, position) === position;
  };
}


