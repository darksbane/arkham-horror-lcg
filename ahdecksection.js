deckbuilderGlobals.rawdeck = deckbuilderGlobals.rawdeck.replace('&#39;','|sq|');
deckbuilderGlobals.rawdeck = html_entity_decode(deckbuilderGlobals.rawdeck,'ENT_QUOTES')
deckbuilderGlobals.rawdeck = deckbuilderGlobals.rawdeck.replace('|sq|','&#39;');

deckbuilderGlobals.rawinvestigator = deckbuilderGlobals.rawinvestigator.replace('&#39;','|sq|');
deckbuilderGlobals.rawinvestigator = html_entity_decode(deckbuilderGlobals.rawinvestigator,'ENT_QUOTES')
deckbuilderGlobals.rawinvestigator = deckbuilderGlobals.rawinvestigator.replace('|sq|','&#39;');

var deckCards = [];
var ahGlobals = new ahDeckbuilderGlobals;
function ahDeckbuilderGlobals (){
	//for IE8 and lower fallback resizing
	this.widthTimer = null;
	//some constants
	this.whichGame = 'arkhamhorror';
	this.gameAbbreviation = "ah";
	this.deckbuilderURL = 'http://www.cardgamedb.com/index.php/arkhamhorror/arkham-horror-the-card-game-deckbuilder';
	this.deckbuilderTitle = 'CardGameDB.com Arkham Horror Deckbuilder';
	this.deckSharePage = 'http://www.cardgamedb.com/index.php/arkhamhorror/arkham-horror-deck-share';
	this.imagepath = "http://lcg-cdn.fantasyflightgames.com/ahlcg";
	//true if ajax in progress. Prevents spamming of server.
	this.ajaxSearchInProgress = false;
	this.ajaxDeckManipulationInProgress = false;
	this.viewType = "";
	this.thetabs;
	this.db;
	this.previousText = ""; 
	this.previousName = "";
	this.previousTrait = "";
	this.results = [];
	this.filters;
	this.filters1;
	this.deckname = "";
	this.buttonType = "";
	this.moble = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
	this.currentlyDisplayedCard = "";
	this.deck = jQuery.parseJSON(deckbuilderGlobals.rawdeck);
	this.investigator = jQuery.parseJSON(deckbuilderGlobals.rawinvestigator);
	this.deckclass = deckbuilderGlobals.deckclass;
	this.db;
	this.sticky;
	this.hoverTimer;
	this.currentCardDisplay;
	this.cardArrayCopy = [];
	this.deckbuildingSearchRules;
	this.deckbuildingMaxCards;
	this.classes = ['Guardian','Mystic','Rogue','Seeker','Survivor','Neutral'];
}


jQuery(document).ready(function($) {
	var surl,formData;
	if (deckbuilderGlobals.viewType=='share'){
		if (location.search != ""){
			qs = location.search.toLowerCase().toQueryParams();
			if (qs.p.match(/^[0-9]+$/) && qs.deck.match(/^(ahdeck_)[0-9a-zA-Z]+$/)){
				surl= ipb.vars['base_url']+'app=ccs&module=ajax&section=ahdeckbuilder&do=share';
				formData = [];
				formData.push({name: 'fPage', value: 'deckshare'});
				formData.push({name: 'fgame', value: 'arkhamhorror'});
				formData.push({name: 'md5check', value: ipb.vars['secure_hash']});
				formData.push({name: 'pid', value: qs.p});
				formData.push({name: 'did', value: qs.c});
				formData.push({name: 'dguid', value: qs.deck});
				$.ajax({
					type: "POST",
					url: surl,
					beforeSend: function ( xhr ) {
						//ajaxDeckManipulationInProgress = true;
					},
					data: formData,
					dataType: "json"
				}).done(function( msg ) {
					if (msg.status=='success'){
						$("#dName").text(html_entity_decode(msg.name,'ENT_QUOTES').replace(/<br\s*\/?>/gi,'\n'));
						$("#offlineName").val(html_entity_decode(msg.name,'ENT_QUOTES').replace(/<br\s*\/?>/gi,'\n'))
						$("#dMember").text(msg.member);
						var tags = msg.tags.split(',');
						$("#dTags").html("<span class='ipsTag'>"+tags.join("</span><span class='ipsTag'>")+"</span>");
						$("#dTeaser").text(html_entity_decode(msg.teaser,'ENT_QUOTES').replace(/<br\s*\/?>/gi,'\n'));
						$("#dStrat").text(html_entity_decode(msg.strategy,'ENT_QUOTES').replace(/<br\s*\/?>/gi,'\n'));

						ahGlobals.deck = msg.deckContents;
						ahGlobals.deckclass = msg.deckClass;
						processPage ();
					} else if (msg.status=='error'){
						console.log(msg);
						$("#divDeckLoading").empty();
						$("#divDeckLoading").text(msg.errorMessage);
					}
				}).fail(function (jqXHR, textStatus){

				}).always(function (jqXHR,textStatus){});
			} else {
				$("#divDeckLoading").empty();
				$("#divDeckLoading").text("The requested deck could not be found.");
			}
		}
	} else {
		processPage ();
	}

	function processPage (){
		ahGlobals.db = TAFFY(cards);
	
		$('#sortType').click(function(){
			$('#dContent2').html(section.typeLI);
			$('#dContent').quicksand( $('#dContent2 li'), {
				  duration: 800,
				  easing: 'easeInOutQuad'
				}, function() {
				// callback code
				addHover();
				sticky.destroy();
				sticky = $('#shareDeckContainer').stickem({offset:90});
			} );
		});
		
		$('#sortCost').click(function(){
			$('#dContent2').html(section.costLI);
			//$('.'+deckObject[whichGame].faction.toLowerCase().replace(/ /g, '').replace("-","")).hide()
			$('#dContent').quicksand( $('#dContent2 li'), {
				  duration: 800,
				  easing: 'easeInOutQuad'
				}, function() {
				// callback code
				addHover();
				sticky.destroy();
				sticky = $('#shareDeckContainer').stickem({offset:90});
			} );
		});
		
		$('#sortName').click( function(){
			$('#dContent2').html(section.nameLI);
			//$('.'+deckObject[whichGame].faction.toLowerCase().replace(/ /g, '').replace("-","")).hide()
			$('#dContent').quicksand( $('#dContent2 li'), {
				  duration: 800,
				  easing: 'easeInOutQuad'
				}, function() {
				// callback code
				addHover();
				sticky.destroy();
				sticky = $('#shareDeckContainer').stickem({offset:90});
			} );
			
		});
	
		$('#viewDeckList').click(function(){
			$('#deckSpoiler').fadeOut('fast', function() {
				$('.deck').fadeIn('fast');
				sticky = $('#shareDeckContainer').stickem({offset:90});
			});
		});
		
		$('#viewDeckSpoiler').click(function(){
			$('.deck').fadeOut('fast', function() {
				$('#deckSpoiler').fadeIn('fast');
			});
			
		});
		
		$("#exportOCTGN").on("click",  function(){
				$('#fileType').val('o8d');
				$('#dcont').val(section.octgnCache);
				$('#decksave').submit()
		});
		
		$("#exportText").on("click",  function(){
				$('#fileType').val('txt');
				$('#dcont').val(section.saveTextCache);
				$('#decksave').submit()
		});
		
		$('#reloadSample').click(function(){
			$('#sampleHand').html(sampleHand(deckObject,deckObject.startingHandSize ));
		});
	
		$('#drawone').click(function(){
			if (ahGlobals.cardArrayCopy.length>0){
				var cardDrawn =ahGlobals.cardArrayCopy.pop();
		
				$('#sampleHand').prepend(cardDrawn+"<br />");
			}
		});

		deckObject.class = ahGlobals.deckclass;

		var searcharray = [],
		searchinvestigator,
		deckoptions = '',
		deckrestrictions = ''
		deckinvestigator = '';
		$.each(ahGlobals.deck, function(k,v){
			if (v.type == "Investigator"){
				searchinvestigator = v.id;
			}
			searcharray.push(v.id);
		});
		deckinvestigator = ahGlobals.db({id:searchinvestigator}).order('name').get();
		$.each(deckinvestigator,function(k,v){
        		deckObject.investigator.push(v);
        		deckoptions = v.dopt;
        		deckrestrictions = v.dres;
		});
		deckbuildingSearchRulesParse(deckoptions, deckrestrictions)
		//resultingAction = [];
		deckrecords = ahGlobals.db({id:searcharray}).order('name').get();
		$.each(deckrecords,function(k,v){
			v.deckquantity = parseInt(ahGlobals.deck[v.id].quantity);
			deckObject.cards.push(v);
			if (v.ctotot == 'Y'){
				deckObject.totalCards += v.deckquantity;
			}

			if (v.wtype != 'Weakness' && v.wtype != 'Basic Weakness'){
	        	deckObject[ahGlobals.whichGame].counts[v.type.toLowerCase().replace(/ /g, '').replace("-","")] += v.deckquantity;
	        } else {
	        	deckObject[ahGlobals.whichGame].counts['weakness'] += v.deckquantity;
	        }
			if (typeof deckObject[ahGlobals.whichGame].costs[v.costnumeric] === 'undefined'){
            	deckObject[ahGlobals.whichGame].costs[v.costnumeric] = 0;
            }

			

			if (v.type != 'Investigator' && v.wtype != 'Weakness' && v.wtype != 'Basic Weakness'){
	        	deckObject[ahGlobals.whichGame].counts[v.clss.toLowerCase().replace(/ /g, '').replace(/-/g, '')] += v.deckquantity;
	        	if (typeof deckObject[ahGlobals.whichGame].costs[v.costnumeric] === 'undefined'){
	            	deckObject[ahGlobals.whichGame].costs[v.costnumeric] = 0;
	            }
	            deckObject[ahGlobals.whichGame].costs[v.costnumeric] += v.deckquantity;

	            if ($.isNumeric(v.will)){
	            	deckObject[ahGlobals.whichGame].counts["willpower"] += v.deckquantity*v.will;
	            }
	            if ($.isNumeric(v.int)){
	            	deckObject[ahGlobals.whichGame].counts["intellect"] += v.deckquantity*v.int;
	            }
	            if ($.isNumeric(v.cmbt)){
	            	deckObject[ahGlobals.whichGame].counts["combat"] += v.deckquantity*v.cmbt;
	            }
	            if ($.isNumeric(v.agi)){
	            	deckObject[ahGlobals.whichGame].counts["agility"] += v.deckquantity*v.agi;
	            }
	            if ($.isNumeric(v.wild)){
	            	deckObject[ahGlobals.whichGame].counts["wild"] += v.deckquantity*v.wild;
	            }
	            if ($.isNumeric(v.lvl)){
	            	deckObject[ahGlobals.whichGame].counts["level"] += v.deckquantity*v.lvl;
	            }

	        	var traitsArray = v.traits.split(". ");
	        	$.each(traitsArray, function(ke,va){
	        		if (va.substring(va.length-1) != "."){
	        			va += ".";
	        		}
	        		if (typeof deckObject[ahGlobals.whichGame].traitstotrack[va] != "undefined"){
	        			deckObject[ahGlobals.whichGame].traitstotrack[va] += v.deckquantity;
	        		}
	        	});/**/
	        }

			deckCards.push(v);
		});

		
		$('#totalCards').text(deckObject.totalCards);

		//var dispdeck= new Object;
	
		//var start = new Date().getTime();
	
		section = new buildSection();
		section.build(deckObject);
		$('#dFaction').html(section.faction); 
		$('#dContent').empty();
		$('#deckSpoiler').html("<ul>"+section.spoilerView+"</ul>");
		$('#dContent').html(section.typeLI);
		$('#dWeakness').html(section.weaknessLI);
		$('#dInvestigator').html(section.investigatorLI);
		addHover(true);
		$('#sampleHand').html(sampleHand(deckObject,deckObject.startingHandSize ));
		$("#cardDisplay").empty();
		
		$('#exportTextArea').val(section.urlCache);
		deckHTMLSetup();
		adjustStatsHTML();
		$("#divDeckLoading").fadeOut(500,function(){
			cl.hide();
			$("#shareDeckContainer").fadeIn(500,function(){
				sticky = $('#shareDeckContainer').stickem({offset:90});
				handleHover(deckObject.investigator[0],'Investigator','');
				populateCharts(deckObject,false);
			});
		});
		
		
			
	
	}


	function adjustStatsHTML(){
		var restrictedtext = "",
		restrictedcount = 0,
		restrictedcolor = 'black',
		classCountList = "";

		$.each(deckObject.restrictedcards, function(k,v){
			restrictedcount++;
			restrictedtext += k+'\n';
		});

		$.each(deckObject[ahGlobals.whichGame].classList,function (k, v){
			var stripClass = v.toLowerCase().replace(/ /g, '').replace(/'/g, '');
			$("#"+stripClass+"Count").text(deckObject[ahGlobals.whichGame].counts[stripClass])
        });

		if (restrictedcount>1){
			restrictedtext = "Possible Restricted List Issue!\n"+restrictedtext;
			restrictedcolor = 'red';
		} 
		$('#willpowerCount').text(deckObject[ahGlobals.whichGame].counts["willpower"]);
		$('#intellectCount').text(deckObject[ahGlobals.whichGame].counts["intellect"]);
		$('#combatCount').text(deckObject[ahGlobals.whichGame].counts["combat"]);
		$('#agilityCount').text(deckObject[ahGlobals.whichGame].counts["agility"]);
		$('#wildCount').text(deckObject[ahGlobals.whichGame].counts["wild"]);
		$('#totalLevels').text(deckObject[ahGlobals.whichGame].counts["level"]);
		$('#restrictedContainer').text("Restricted Cards:\n"+restrictedtext).css('color', restrictedcolor);
		$('#deckviewclasses>div.Guardian').text(deckObject[ahGlobals.whichGame].counts.guardian);
		$('#deckviewclasses>div.Mystic').text(deckObject[ahGlobals.whichGame].counts.mystic);
		$('#deckviewclasses>div.Rogue').text(deckObject[ahGlobals.whichGame].counts.rogue);
		$('#deckviewclasses>div.Survivor').text(deckObject[ahGlobals.whichGame].counts.survivor);
		$('#deckviewclasses>div.Seeker').text(deckObject[ahGlobals.whichGame].counts.seeker);
		$('#deckviewclasses>div.Neutral').text(deckObject[ahGlobals.whichGame].counts.neutral);
	}
	
	function deckbuildingSearchRulesParse(options, restrictions){
		var optionSplit,
		subSplit,
		quantityRange,
		levelRange,
		classOrTrait,
		itemSplit,
		buildrule = [],
		output = [],
		combinedrules = [],
		indexOfClass,
		anyOtherPresent = false,
		anyOther = {},
		specificClasses =$.extend(true, [], ahGlobals.classes);

		if (options != "") {
			optionSplit = options.split("|");
			for (o=0;o<optionSplit.length;o++){

				subSplit = optionSplit[o].split(";");
				quantityRange = subSplit[0].split("-");
				levelRange  = subSplit[2].split("-");
				classOrTrait = subSplit[1].split("-");
				if (classOrTrait[0] == 'class'){
					indexOfClass = specificClasses.indexOf(classOrTrait[1]);
					if (indexOfClass !== -1){
						buildrule[o] = {};
						buildrule[o].clss = classOrTrait[1];
						buildrule[o].lvl = {};
						buildrule[o].lvl['>='] = levelRange[0];
						buildrule[o].lvl['<='] = levelRange[1];
						specificClasses.splice(indexOfClass, 1);
					} else if (classOrTrait[1] == "ANY OTHER"){ 
						anyOtherPresent = true;
						anyOther.lvl = {};
						anyOther.lvl['>='] = levelRange[0];
						anyOther.lvl['<='] = levelRange[1];
					}
				} else if (classOrTrait[0] == 'trait'){
					buildrule[o] = {};
					buildrule[o].traits = {};
					buildrule[o].traits['likenocase'] = classOrTrait[1];
					buildrule[o].lvl = {};
					buildrule[o].lvl['>='] = levelRange[0];
					buildrule[o].lvl['<='] = levelRange[1];
					deckObject[ahGlobals.whichGame].traitstotrack[classOrTrait[1]] = 0;
				}
				

			}
		}
		if (anyOtherPresent){
			anyOther.clss = specificClasses
			buildrule.push(anyOther);
		}
		console.log(buildrule);
		
		ahGlobals.deckbuildingSearchRules = buildrule;
		
	}

	function deckHTMLSetup(){
		$("#deckviewclasses>div").hide();
		$.each(ahGlobals.deckbuildingSearchRules, function(k,v){
			if (typeof v.clss != 'undefined'){
				if ($.isArray(v.clss)){
					$("#deckviewclasses>div").show();
				} else {
					$("#deckviewclasses>div."+v.clss).show();
				}
				
			}
			if (typeof v.traits != 'undefined'){
				console.log(v.traits['likenocase']);
				console.log(deckObject[ahGlobals.whichGame].traitstotrack[v.traits['likenocase']]);
				$("#deckviewtraits").text(v.traits['likenocase']+": "+deckObject[ahGlobals.whichGame].traitstotrack[v.traits['likenocase']]);
				$("#decktraitscount").text("0");
			}
		});
	}

	function addHover (initial) {

		$('.investigatorHover,.assetHover,.eventHover,.skillHover,.weaknessHover').on('mouseenter',function() {
			var myEle = $(this),
			cid = myEle.data("cid"),
			ctype = myEle.data("type"),
			passRecord = myEle.data("card"),
			cimg = myEle.data("img"),
			Record;
			if (typeof passRecord != "object"){
				Record = ahGlobals.db({id:cid.toString()}).order('name').get();
				passRecord = Record[0];
			}
			ahGlobals.hoverTimer = setTimeout(function(){handleHover(passRecord,ctype,cimg);}, 500);
		});
		$('.investigatorHover,.assetHover,.eventHover,.skillHover,.weaknessHover').on('touchstart',function(e) {
			var myEle = $(this),
			cid = myEle.data("cid"),
			ctype = myEle.data("type"),
			passRecord = myEle.data("card"),
			cimg = myEle.data("img"),
			Record;
			if (typeof passRecord != "object"){
				Record = ahGlobals.db({id:cid.toString()}).order('name').get();
				passRecord = Record[0];
			}
			if (ahGlobals.currentCardDisplay != cid){
				e.preventDefault();
				handleHover(passRecord,ctype,cimg)
			}else {
			
			}
		});
		$('.investigatorHover,.assetHover,.eventHover,.skillHover,.weaknessHover').mouseleave( function(){
			if(ahGlobals.hoverTimer ){
				clearTimeout(ahGlobals.hoverTimer );
			}
		});

	}
	function handleHover(record,ctype,cimg){
		var itemImageValue;
			if (ahGlobals.currentCardDisplay != record.id){
				ahGlobals.currentCardDisplay = record.id;
				$("#cardDisplay").empty();
				$("#cardDisplay").append(cardViewTemplate([record],record.id)); 
				sticky.destroy();
				sticky = $('#shareDeckContainer').stickem({offset:90});
			}
		
	}
	
	//can clear charts or populate them with data
	function populateCharts(deckObjectToParse,clearCharts){
		if (clearCharts){
			var deckStatsArray = [['Card Values',{'type': 'string', 'role': 'tooltip', 'p': {'html': true}}, 'Cost']];
			deckStatsArray.push(["0",createCustomDataStatsTooltip(0,0),0]);
			var dataStats = google.visualization.arrayToDataTable(deckStatsArray);
			new google.visualization.ColumnChart(document.getElementById('chart4')).
				draw(dataStats,
					{title:"Costs",
					chartArea: {'width': '70%', 'height': '70%'},
					titleTextStyle:{fontSize: 18},
					vAxis: {viewWindowMode: "explicit", viewWindow:{ min: 0 }},
					focusTarget: 'category',
					tooltip: { isHtml: true },
					width:550, height:400,
					legend: {position: 'top',maxLines:3},
					vAxis: {title: "Number of Cards"},
					hAxis: {title: "Card Costs"}}
				);

			var dataCardTypes = google.visualization.arrayToDataTable([
				['Type', 'Number of Cards'],
				['-', 0]
			]);
			var dataClasses = google.visualization.arrayToDataTable([
				['Classes', 'Number of Cards'],
				['-', 0]
			]);
			new google.visualization.PieChart(document.getElementById('chart5')).
				draw(dataCardTypes, {title:"Card Types",
									chartArea: {'width': '90%', 'height': '70%'},
									sliceVisibilityThreshold:0,
									titleTextStyle:{fontSize: 18},
									width:300,
									legend:{position: 'top', maxLines:'7'}});
			new google.visualization.PieChart(document.getElementById('chart6')).
				draw(dataClasses, {title:"Icons",
									chartArea: {'width': '90%', 'height': '70%'},
									sliceVisibilityThreshold:0,
									titleTextStyle:{fontSize: 18},
									width:300,
									legend:{position: 'top', maxLines:'7'}});
		} else {

			var costlength = deckObjectToParse[ahGlobals.whichGame].costs.length;
			var deckStatsArray = [['Card Values',{'type': 'string', 'role': 'tooltip', 'p': {'html': true}}, 'Cost']];
			var maxChartStatsLength = costlength
			var _costs;
			for(i=0;i<maxChartStatsLength;i++){

				if(typeof deckObjectToParse[ahGlobals.whichGame].costs[i]!=='undefined' && deckObjectToParse[ahGlobals.whichGame].costs[i]!=null){
					_costs = deckObjectToParse[ahGlobals.whichGame].costs[i];
				} else {
					_costs = 0;
				}
				deckStatsArray.push([i.toString(),createCustomDataStatsTooltip(i,_costs),_costs]);
			}
			if (deckStatsArray.length <= 1){
				deckStatsArray.push(["0",createCustomDataStatsTooltip(0,0),0]);
			}
			var dataStats = google.visualization.arrayToDataTable(deckStatsArray);
			var dataClasses = google.visualization.arrayToDataTable([
				['Classes', 'Number of Cards'],
				['Guardian', deckObjectToParse[ahGlobals.whichGame].counts.guardian],
				['Mystic', deckObjectToParse[ahGlobals.whichGame].counts.mystic],
				['Rogue', deckObjectToParse[ahGlobals.whichGame].counts.rogue],
				['Seeker', deckObjectToParse[ahGlobals.whichGame].counts.seeker],
				['Survivor', deckObjectToParse[ahGlobals.whichGame].counts.survivor],
				['Neutral', deckObjectToParse[ahGlobals.whichGame].counts.neutral]
			]);

			var dataCardTypes = google.visualization.arrayToDataTable([
				['Type', 'Number of Cards'],

				['Event', deckObjectToParse[ahGlobals.whichGame].counts.event],
				['Asset', deckObjectToParse[ahGlobals.whichGame].counts.asset],
				['Skill', deckObjectToParse[ahGlobals.whichGame].counts.skill]
			]);

			new google.visualization.ColumnChart(document.getElementById('chart4')).
				draw(dataStats,
					{title:"Costs",
					chartArea: {'width': '80%', 'height': '70%'},
					titleTextStyle:{fontSize: 18},
					vAxis: {viewWindowMode: "explicit", viewWindow:{ min: 0 }},
					colors: [
					'#2EC1D1', 
					'#D12E41', 
					'#5E2059',
					'#42B023',
					'#E8C91A',
					'#D6A93E',
					'#CDC2C0'
					],
					focusTarget: 'category',
					tooltip: { isHtml: true },
					width:600, height:400,
					legend: {position: 'top',maxLines:3},
					vAxis: {title: "Number of Cards"},
					hAxis: {title: "Card Costs"}}
				);

			new google.visualization.PieChart(document.getElementById('chart5')).
				draw(dataCardTypes, {title:"Card Types",
									chartArea: {'width': '90%', 'height': '70%'},
									sliceVisibilityThreshold:0,
									titleTextStyle:{fontSize: 18},
									width:300,
									legend:{position: 'top', maxLines:'7'}});
			new google.visualization.PieChart(document.getElementById('chart6')).
				draw(dataClasses, {colors: [
										'#236da8', 
										'#4e4483', 
										'#165f3a',
										'#bc882d',
										'#93222c',
										'#383834'
									],
									title:"Classes",
									chartArea: {'width': '90%', 'height': '70%'},
									sliceVisibilityThreshold:0,
									titleTextStyle:{fontSize: 18},
									width:300,
									legend:{position: 'top', maxLines:'7'}});
			
		}

	}

	function createCustomDataStatsTooltip(_tot,_cst){
		return '<div style="padding:5px 5px 5px 5px;color:black;">' +
		'<table id="medals_layout">'+
		'<tr><td><div style="height: 1em;width: 1em;background:#2ec1d1;float: left;"></div>&nbsp;'+_cst+' cards with '+_tot+' Cost</td></tr>'+
		'</table>' + 
		'</div>';
	}

	//generates sample hand
	function sampleHand(){
		var startingHandSize = 7,
		i;
		if (deckObject.startingHandSize != 0){
			startingHandSize = deckObject.startingHandSize;
		}
		ahGlobals.cardArrayCopy = [];
		$.each (deckObject.cards,function (k,v){
			if (v.type != 'Investigator')
			{
				for (i=0;i<v.deckquantity;i++)
						ahGlobals.cardArrayCopy.push(v.name +' ['+v.lvl+'] ('+v.setname+')');
			}
		});	
		
		var hand = shuffle(ahGlobals.cardArrayCopy).splice(0,startingHandSize).join("<br />");
		$('#sampleHand').html(hand);	
	
	}


});


//holds everything associated with a deck
var deckObject = new deckContents;
function deckContents(){
	this.saved = false;
	this.deckid = 0;
	this.deckguid = '';
	this.deckname = '';
	this.deckteaser = '';
	this.deckstrategy = '';
	this.decktags = '';
	this.cards = [];
	this.class = [];
	this.investigator = [];
	this.restrictedcards = {};
	this.totalCards = 0;
	this.game = 'arkhamhorror';
	this.abbreviation = "ah";
	this.ally = '';
	this.startingHandSize = 5;
	this.arkhamhorror= new Object();
	this.arkhamhorror.counts = {investigator:0, asset:0, skill:0, event:0, level:0, weakness:0, guardian:0, mystic:0, rogue:0, seeker:0, survivor:0, neutral:0, willpower:0, intellect:0, combat:0, agility:0, wild:0};
	this.arkhamhorror.totals = {cost:0,strength:0,loyalty:0};
	this.arkhamhorror.traitstotrack = {};
	this.arkhamhorror.costs = [];
	this.arkhamhorror.restrictedcards = [];
	this.arkhamhorror.factionColors = {"Guardian":'#C4601E',"Mystic":'#1E8EC4',"Rogue": '#519215',"Seeker": '#a5a49a',"Survivor":'#9221BE', "Neutral":'#CEAB1F'};

	this.arkhamhorror.classList = ['Guardian','Mystic','Rogue','Seeker','Survivor','Neutral'];
	this.arkhamhorror.typeList = ["Investigator","Asset","Event","Skill","Weakness"];
	this.arkhamhorror.series = ['costs'];
}

deckContents.prototype.populate = function(obj){
    for(var o in obj){
        this[o] = obj[o];
    }
};
	
function dynamicSort(property) {
   	return function (a,b) {
   		return (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
	}
}


function toQueryParams(separator) {
    var match = this.strip().match(/([^?#]*)(#.*)?$/);
    if (!match) return { };

    return match[1].split(separator || '&').inject({ }, function(hash, pair) {
      if ((pair = pair.split('='))[0]) {
        var key = decodeURIComponent(pair.shift()),
            value = pair.length > 1 ? pair.join('=') : pair[0];
            
        if (value != undefined) value = decodeURIComponent(value);

        if (key in hash) {
          if (!Object.isArray(hash[key])) hash[key] = [hash[key]];
          hash[key].push(value);
        }
        else hash[key] = value;
      }
      return hash;
    });
}



//Fisher-Yates Shuffle to randomize array
shuffle = function(o){
	for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	return o;
};

if (!String.prototype.startsWith) {
  String.prototype.startsWith = function(searchString, position) {
    position = position || 0;
    return this.indexOf(searchString, position) === position;
  };
}

