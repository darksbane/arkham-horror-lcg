
if (!Array.prototype.forEach) {
	Array.prototype.forEach = function (fn, scope) {
		'use strict';
		var i, len;
		for (i = 0, len = this.length; i < len; ++i) {
			if (i in this) {
				fn.call(scope, this[i], i, this);
			}
		}
	};
}
location.querystring = (function() {
	// The return is a collection of key/value pairs
	var queryStringDictionary = {};
	// Gets the query string, starts with '?'
	var querystring = decodeURI(location.search);
	// document.location.search is empty if no query string
	if (!querystring) {
		return {};
	}
	// Remove the '?' via substring(1)
	querystring = querystring.substring(1);
	// '&' seperates key/value pairs
	var pairs = querystring.split("&");
	// Load the key/values of the return collection
	for (var i = 0; i < pairs.length; i++) {
		var keyValuePair = pairs[i].split("=");
		queryStringDictionary[keyValuePair[0]] 
				= keyValuePair[1];
	}
	// toString() returns the key/value pairs concatenated
	queryStringDictionary.toString = function() {
		if (queryStringDictionary.length == 0) {
			return "";
		}
		var toString = "?";
		for (var key in queryStringDictionary) {
			toString += key + "=" + 
				queryStringDictionary[key];
		}
		return toString;
	};
	// Return the key/value dictionary
	return queryStringDictionary;
})();
		
//This creates the ability to declare a vertical button set for use in the and/or button on the icon search
(function( $ ){
  //plugin buttonset vertical
  $.fn.buttonsetv = function() {
    return this.each(function(){
      $(this).buttonset();
      $(this).css({'display': 'table', 'margin-bottom': '7px'});
      $('.ui-button', this).css({'margin': '0px', 'display': 'table-cell'}).each(function(index) {
              if (! $(this).parent().is("div.dummy-row")) {
                  $(this).wrap('<div class="dummy-row" style="display:table-row; " />');
              }
          });
      $('.ui-button:first', this).first().removeClass('ui-corner-left').addClass('ui-corner-top');
      $('.ui-button:last', this).last().removeClass('ui-corner-right').addClass('ui-corner-bottom');
    });
  };
})( jQuery );

var jQuery_1_9_1 = $.noConflict(true);
var results = [];
var advanced = false;
var filterTypeSelect2 ;
jQuery_1_9_1 (document).ready(function($) {

	var searchExists = false;
	
	/*$('#filterSet').attr('multiple','multiple').css("width","95%").attr('placeholder', 'Choose Set to Filter');
	$("select#filterSet option[value='0']").remove(); 
	$('#filterSet').select2({closeOnSelect: false});*/
	$("#filterType").attr('multiple','multiple'); //changes type filter to be multiple 
	$("#filterType").css("width",'575');
	$("#filterType option[value='Any']").remove();

	var filterTypeSelect2 = $("#filterType");
	filterTypeSelect2.select2();

	searchExists = prefillSearch();
	$("#inputSearchReset").click( function(e){
		e.preventDefault();
		resetSearch();
	});
	function resetSearch(){
		$('#formQuickSearch')[0].reset();
		filterTypeSelect2.select2("val", ""); 
	}

	$('#divMoreSearchButton').click(function(){ $('#divMoreSearchButton').hide();$('#divMoreSearch').slideDown();advanced=true; });
	$('#divLessSearchButton').click(function(){ $('#divMoreSearchButton').show();$('#divMoreSearch').slideUp();advanced=false;  });
	$('#divFactions').buttonset();
	$("#filterText").autocomplete({
	  source: cards,
	  minLength: 2,
	  open: function(e,ui) {
	            var acData = $(this).data('uiAutocomplete');
	            //var styledTerm = anGlobals.termTemplate.replace(/%s/gi, acData.term);

	            acData
	                .menu
	                .element
	                .find('a')
	                .each(function() {
	                    var me = $(this),
	                    keywords = acData.term.split(' ').join('|'),
	                    mytext = me.text(),
	                    finalhtml = '';
	                    mysearchitem = mytext.split('|*|');
						finalhtml = mysearchitem[0].replace(new RegExp("(" + keywords + ")", "gi"), '<strong>$1</strong>')+mysearchitem[1];
	                    me.html(finalhtml);
	                });
	      },
	  select: function( event, ui ) {
		      	console.log(ui);
		      	console.log(event);
		      	results = [ui.item];
				displayCards (0);
		      	$("#divResults").slideDown('fast');
		   }
	}).keypress(function(e) {
	    if(e.which == 13) {
	    	$("#filterName").autocomplete( "close" );
	        filterCards();
		    $("#divResults").slideDown('fast');
	    }
	}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	  return $( "<li>" )
		.append( "<a>" + item.label + "|*|(" + item.setname+ ")</a>" )
		.appendTo( ul );
	};
	db = TAFFY(cards);
	var encounterList = db({deck:'S'}).order("encounter").distinct("encounter");
	console.log (encounterList);
	$.each(encounterList, function (k,v){
		$("#filterEncounter").append("<option value='"+v+"'>"+v+"</option>");
	});

	$('#filterVictory,#operatorVictory,#filterClue,#operatorClue,#filterShroud,#operatorShroud,#filterHorror,#operatorHorror,#filterDamage,#operatorDamage,#filterEvade,#operatorEvade,#filterFight,#operatorFight,#filterEncounter,#filterSet,#filterType,#filterSlot,#filterSanity,#operatorSanity,#filterHealth,#operatorHealth,#filterWild,#operatorWild,#filterAgility,#operatorAgility,#filterCombat,#operatorCombat,#filterIntellect,#operatorIntellect,#filterWillpower,#operatorWillpower,#filterLevel,#operatorLevel,#GuardianFB,#MysticFB,#RogueFB,#SeekerFB,#SurvivorFB,#neutralFB,#filterUnique,#filterCost,#operatorCost,#filterTextField,#orderBy').change(function(){

			filterCards();
			$("#divResults").slideDown('fast');
	});
	$("#filterTraits,#filterText").keyup( function (e) {
		// fire the above change event after every letter
		if (e.which == 13) {
			$("#ahdb-submit-button").trigger('click');
		}
	});

	$("#aHideResults").click( function(event){
		event.preventDefault();
		$("#divResults").slideUp('fast');
		
	});
	$("#ahdb-submit-button").click( function(){
		filterCards();
		$("#divResults").slideDown('fast');
		
	});
	$('#searchUL').on('click','.buttonNextCards', function(){
		//console.log($(this))
		displayCards ($(this).data('start')+20);
	    $('html,body').animate({
			scrollTop: $("#divResults").offset().top
      		}, 900, 'swing');
	});
	$('#searchUL').on('click','.buttonPrevCards', function(){
		
		displayCards ($(this).data('start')-20);
		$('html, body').animate({
	        scrollTop: $("#searchUL").offset().top
	    }, 600);
	});
	$("#searchUL").on("click",'input[type="text"]', function () {
	   $(this).select();
	});

	$('#checkAll').click(function(){
		  $('.factionCheckboxes:checkbox').prop('checked', true);
		  $('#divFactions').buttonset("refresh");
		  filterCards()
	}).tooltip();
	$('#uncheckAll').click(function(){
		  $('.factionCheckboxes:checkbox').prop('checked', false);
		  $('#divFactions').buttonset("refresh");
		  filterCards()
	}).tooltip();


	function filterCards(){
		
		results = buildFilter();
		displayCards (0)

	}
	
	function displayCards (startRecord){
		var quantity = results.length;
		var list = [];
		var subset = [];
		var selectView = $('#selectView').val(),
		getview;
		$('#searchUL').empty();

		//$("#divResults").scrollTop(0);


		


		subset = results.slice(startRecord,startRecord+20);
		if (selectView=='image'){
			getview = 'spoiler';
		} else {
			getview = selectView;
		}
		subset.forEach(function(element,index,array){

			list.push(cardSpoilerTemplate(element,getview,'med_'));
		});
		if (selectView=="spoiler"){
			display = list.join("");
			$('#searchUL').removeClass('imageonly');
		} else if (selectView=="image"){
			display = list.join("");
			$('#searchUL').addClass('imageonly');
		} else if (selectView=="list"){
			$('#searchUL').removeClass('imageonly');
			display = "<li><table id='tableSearchResults'><thead><tr><th style='width:28%'>Name</th><th style='width:17%'>Class/Encounter</th><th style='width:10%'>Type</th><th style='width:5%'>Cost</th><th style='width:16%'>Stats</th><th style='width:4%'>Text</th><th style='width:25%'>Set</th></thead><tbody></tr>"+list.join("")+"</tbody></table></li>";
		}
		var navigation = pagedCardsNavigation (startRecord,quantity )
		var searchLinkItems = [];
		searchLinkItems.push('advanced='+advanced);
		searchLinkItems.push('tx='+escape($('#filterText').val()));
		searchLinkItems.push('txf='+escape($('#filterTextField').val()));
		searchLinkItems.push('or='+escape($('#selectOrderBy').val()));
		searchLinkItems.push('vw='+escape($('#selectView').val()));
		if (advanced) {
			if ($('#filterSet').val()!="0")	
				searchLinkItems.push('set='+escape($('#filterSet').val()));
			if ($('#filterUnique').val()!="Any")	
				searchLinkItems.push('uni='+escape($('#filterUnique').val()));
			if ($('#filterEncounter').val()!="Any")	
				searchLinkItems.push('ens='+escape($('#filterEncounter').val()));
			if ($('#filterSlot').val()!="Any")	
				searchLinkItems.push('slt='+escape($('#filterSlot').val()));
			if ($('#filterType').val()!="Any")	
				searchLinkItems.push('typ='+escape($('#filterType').val()));
			var factions= "";
			if ($('#GuardianFB').prop('checked')){
				factions +='gu';
			}
			if ($('#MysticFB').prop('checked')){
				factions +='my';
			}
			if ($('#RogueFB').prop('checked')){
				factions +='ro';
			}
			if ($('#SeekerFB').prop('checked')){
				factions +='se';
			}
			if ($('#SurvivorFB').prop('checked')){
				factions +='su';
			}
			if ($('#neutralFB').prop('checked')){
				factions +='nu';
			}
			searchLinkItems.push('f='+escape(factions));

			if ($('#filterCost').val() != 'Any'){
				if ($('#operatorCost').val() == 'e')
					searchLinkItems.push('cst='+escape('e'+$('#filterCost').val()));
				else if ($('#operatorCost').val() == 'gt')
					searchLinkItems.push('cst='+escape('g'+$('#filterCost').val()));
				else if ($('#operatorCost').val() == 'lt')
					searchLinkItems.push('cst='+escape('l'+$('#filterCost').val()));
			} 
			if ($('#filterLevel').val() != 'Any'){
				if ($('#operatorLevel').val() == 'e')
					searchLinkItems.push('lvl='+escape('e'+$('#filterLevel').val()));
				else if ($('#operatorCost').val() == 'gt')
					searchLinkItems.push('lvl='+escape('g'+$('#filterLevel').val()));
				else if ($('#operatorCost').val() == 'lt')
					searchLinkItems.push('lvl='+escape('l'+$('#filterLevel').val()));
			} 
			if ($('#filterWillpower').val() != 'Any'){
				if ($('#operatorWillpower').val() == 'e')
					searchLinkItems.push('wlp='+escape('e'+$('#filterWillpower').val()));
				else if ($('#operatorWillpower').val() == 'gt')
					searchLinkItems.push('wlp='+escape('g'+$('#filterWillpower').val()));
				else if ($('#operatorWillpower').val() == 'lt')
					searchLinkItems.push('wlp='+escape('l'+$('#filterWillpower').val()));
			} 
			if ($('#filterIntellect').val() != 'Any'){
				if ($('#operatorIntellect').val() == 'e')
					searchLinkItems.push('int='+escape('e'+$('#filterIntellect').val()));
				else if ($('#operatorIntellect').val() == 'gt')
					searchLinkItems.push('int='+escape('g'+$('#filterIntellect').val()));
				else if ($('#operatorIntellect').val() == 'lt')
					searchLinkItems.push('int='+escape('l'+$('#filterIntellect').val()));
			} 
			if ($('#filterCombat').val() != 'Any'){
				if ($('#operatorCombat').val() == 'e')
					searchLinkItems.push('cmb='+escape('e'+$('#filterCombat').val()));
				else if ($('#operatorCombat').val() == 'gt')
					searchLinkItems.push('cmb='+escape('g'+$('#filterCombat').val()));
				else if ($('#operatorCombat').val() == 'lt')
					searchLinkItems.push('cmb='+escape('l'+$('#filterCombat').val()));
			}
			if ($('#filterAgility').val() != 'Any'){
				if ($('#operatorAgility').val() == 'e')
					searchLinkItems.push('agi='+escape('e'+$('#filterAgility').val()));
				else if ($('#operatorAgility').val() == 'gt')
					searchLinkItems.push('agi='+escape('g'+$('#filterAgility').val()));
				else if ($('#operatorAgility').val() == 'lt')
					searchLinkItems.push('agi='+escape('l'+$('#filterAgility').val()));
			}
			if ($('#filterWild').val() != 'Any'){
				if ($('#operatorWild').val() == 'e')
					searchLinkItems.push('wld='+escape('e'+$('#filterWild').val()));
				else if ($('#operatorWild').val() == 'gt')
					searchLinkItems.push('wld='+escape('g'+$('#filterWild').val()));
				else if ($('#operatorWild').val() == 'lt')
					searchLinkItems.push('wld='+escape('l'+$('#filterWild').val()));
			}
			if ($('#filterHealth').val() != 'Any'){
				if ($('#operatorHealth').val() == 'e')
					searchLinkItems.push('hlh='+escape('e'+$('#filterHealth').val()));
				else if ($('#operatorHealth').val() == 'gt')
					searchLinkItems.push('hlh='+escape('g'+$('#filterHealth').val()));
				else if ($('#operatorHealth').val() == 'lt')
					searchLinkItems.push('hlh='+escape('l'+$('#filterHealth').val()));
			}
			if ($('#filterSanity').val() != 'Any'){
				if ($('#operatorSanity').val() == 'e')
					searchLinkItems.push('snt='+escape('e'+$('#filterSanity').val()));
				else if ($('#operatorSanity').val() == 'gt')
					searchLinkItems.push('snt='+escape('g'+$('#filterSanity').val()));
				else if ($('#operatorSanity').val() == 'lt')
					searchLinkItems.push('snt='+escape('l'+$('#filterSanity').val()));
			}
			if ($('#filterFight').val() != 'Any'){
				if ($('#operatorFight').val() == 'e')
					searchLinkItems.push('fig='+escape('e'+$('#filterFight').val()));
				else if ($('#operatorFight').val() == 'gt')
					searchLinkItems.push('fig='+escape('g'+$('#filterFight').val()));
				else if ($('#operatorFight').val() == 'lt')
					searchLinkItems.push('fig='+escape('l'+$('#filterFight').val()));
			}
			if ($('#filterEvade').val() != 'Any'){
				if ($('#operatorEvade').val() == 'e')
					searchLinkItems.push('evd='+escape('e'+$('#filterEvade').val()));
				else if ($('#operatorEvade').val() == 'gt')
					searchLinkItems.push('evd='+escape('g'+$('#filterEvade').val()));
				else if ($('#operatorEvade').val() == 'lt')
					searchLinkItems.push('evd='+escape('l'+$('#filterEvade').val()));
			}
			if ($('#filterDamage').val() != 'Any'){
				if ($('#operatorDamage').val() == 'e')
					searchLinkItems.push('dmg='+escape('e'+$('#filterDamage').val()));
				else if ($('#operatorDamage').val() == 'gt')
					searchLinkItems.push('dmg='+escape('g'+$('#filterDamage').val()));
				else if ($('#operatorDamage').val() == 'lt')
					searchLinkItems.push('dmg='+escape('l'+$('#filterDamage').val()));
			}
			if ($('#filterHorror').val() != 'Any'){
				if ($('#operatorHorror').val() == 'e')
					searchLinkItems.push('hor='+escape('e'+$('#filterHorror').val()));
				else if ($('#operatorHorror').val() == 'gt')
					searchLinkItems.push('hor='+escape('g'+$('#filterHorror').val()));
				else if ($('#operatorHorror').val() == 'lt')
					searchLinkItems.push('hor='+escape('l'+$('#filterHorror').val()));
			}
			if ($('#filterShroud').val() != 'Any'){
				if ($('#operatorShroud').val() == 'e')
					searchLinkItems.push('shd='+escape('e'+$('#filterShroud').val()));
				else if ($('#operatorShroud').val() == 'gt')
					searchLinkItems.push('shd='+escape('g'+$('#filterShroud').val()));
				else if ($('#operatorShroud').val() == 'lt')
					searchLinkItems.push('shd='+escape('l'+$('#filterShroud').val()));
			}
			if ($('#filterClue').val() != 'Any'){
				if ($('#operatorClue').val() == 'e')
					searchLinkItems.push('clu='+escape('e'+$('#filterClue').val()));
				else if ($('#operatorClue').val() == 'gt')
					searchLinkItems.push('clu='+escape('g'+$('#filterClue').val()));
				else if ($('#operatorClue').val() == 'lt')
					searchLinkItems.push('clu='+escape('l'+$('#filterClue').val()));
			}
			if ($('#filterVictory').val() != 'Any'){
				if ($('#operatorVictory').val() == 'e')
					searchLinkItems.push('vct='+escape('e'+$('#filterVictory').val()));
				else if ($('#operatorVictory').val() == 'gt')
					searchLinkItems.push('vct='+escape('g'+$('#filterVictory').val()));
				else if ($('#operatorVictory').val() == 'lt')
					searchLinkItems.push('vct='+escape('l'+$('#filterVictory').val()));
			}
			
		}
		searchLinkItemsString = searchLinkItems.join("&");
		var searchlink = "";
		if (quantity >0){
			searchlink = ' Link to this Search: <input type="text" value="http://www.cardgamedb.com/index.php/arkhamhorror/arkham-horror-the-card-game?&'+searchLinkItemsString+'" style="background:transparent;border:0px;width:580px;" />';
		
		}
		$('#searchUL').append("<li><div style='width:170px;float:left'><strong>Cards Returned: "+quantity +"</strong></div><div style='width:720px;float:left'> "+searchlink+"</div></li>").append("<li style='padding-top:30px;'>"+navigation +"</li>").append(display ).append("<li style='clear:both;'>"+navigation +"</li>");
		if ($('#selectView').val()=="list"){
			$('.tooltipGameText').tooltip({
				  track: true
				,show: {duration: 300}
				,content: function () {
					  return $(this).prop('title');
				  }});
		}

	}

	function pagedCardsNavigation (start,total){
		if (total <= 20 && total >0)
			return "Records 1-"+total;
		else if (total <= 0)
			return "";

		var nextPage = '<button class="buttonNextCards ipsButton_secondary" data-start="'+start+'">Next Page</button>';
		var prevPage = '<button class="buttonPrevCards ipsButton_secondary" data-start="'+start+'">Prev Page</button>';
		if (start == 0)
			return nextPage+ " Records 1-20";
			
		var displayEnd = start+20;	
		var displayStart = start+1;
		if (total-start <= 20)
			return prevPage+ " Records "+displayStart +"-"+total;
		
		return (prevPage+nextPage+ " Records "+displayStart +"-"+displayEnd );


	}		


	function buildFilter(){
		var filterType = {},
			filterCost = {},
			filterSlot = {},
			filterLevel = {},
			filterWillpower = {},
			filterIntellect = {},
			filterCombat = {},
			filterAgility = {},
			filterWild = {},
			filterHealth = {},
			filterSanity = {},
			filterUnique = {},
			filterSets = {},
			filterText = {},
			filterTrait = {},
			filterName = {},
			filterClass = {},
			filterEncounter = {},
			filterFight = {},
			filterEvade = {},
			filterDamage = {},
			filterHorror = {},
			filterShroud= {},
			filterClue = {},
			filterVictory = {},
			textArray = [],
			textRegex,
			regexTextString = "",
			traitArray = [],
			traitRegex,
			regexTraitString = "",
			nameArray = [],
			regexNameString = "",
			orderBy,
			item,
			record,
			uniqueness,
			$inputSelectedOrder = $('#orderBy').val(),
			$inputUnique = $('#filterUnique').val(),
			$inputSlot = $('#filterSlot').val(),
			$inputType = $('#filterType').val(),
			$inputCost = $('#filterCost').val(),
			$inputOperatorCost = $('#operatorCost').val(),
			$inputLevel = $('#filterLevel').val(),
			$inputOperatorLevel = $('#operatorLevel').val(),
			$inputWillpower = $('#filterWillpower').val(),
			$inputOperatorWillpower = $('#operatorWillpower').val(),
			$inputIntellect = $('#filterIntellect').val(),
			$inputOperatorIntellect = $('#operatorIntellect').val(),
			$inputCombat = $('#filterCombat').val(),
			$inputOperatorCombat = $('#operatorCombat').val(),
			$inputAgility = $('#filterAgility').val(),
			$inputOperatorAgility = $('#operatorAgility').val(),
			$inputWild = $('#filterWild').val(),
			$inputOperatorWild = $('#operatorWild').val(),
			$inputHealth = $('#filterHealth').val(),
			$inputOperatorHealth = $('#operatorHealth').val(),
			$inputSanity = $('#filterSanity').val(),
			$inputOperatorSanity = $('#operatorSanity').val(),
			$inputGuardian = $('#GuardianFB').prop('checked'),
			$inputMystic = $('#MysticFB').prop('checked'),
			$inputRogue = $('#RogueFB').prop('checked'),
			$inputSeeker = $('#SeekerFB').prop('checked'),
			$inputSurvivor = $('#SurvivorFB').prop('checked'),
			$inputNeutral = $('#neutralFB').prop('checked'),
			$inputText = $("#filterText").val(),
			$inputTrait = $("#filterTraits").val(),
			$inputName = $("#filterName").val(),
			$inputEncounter = $("#filterEncounter").val(),
			$inputFight = $('#filterFight').val(),
			$inputOperatorFight = $('#operatorFight').val(),
			$inputEvade = $('#filterEvade').val(),
			$inputOperatorEvade = $('#operatorEvade').val(),
			$inputDamage = $('#filterDamage').val(),
			$inputOperatorDamage = $('#operatorDamage').val(),
			$inputHorror = $('#filterHorror').val(),
			$inputOperatorHorror = $('#operatorHorror').val(),
			$inputShroud = $('#filterShroud').val(),
			$inputOperatorShroud = $('#operatorShroud').val(),
			$inputClue = $('#filterClue').val(),
			$inputOperatorClue = $('#operatorClue').val(),
			$inputVictory = $('#filterVictory').val(),
			$inputOperatorVictory = $('#operatorVictory').val(),
			$inputSet = $('#filterSet').val();

		if ($.type( $inputType ) !== "null"){
			filterType.type = $inputType;
		} 


		if ($inputCost != 'Any'){
			filterCost.costnumeric = {};
			if ($inputOperatorCost == 'e')
				filterCost.costnumeric['=='] = $inputCost;
			else if ($inputOperatorCost == 'gt')
				filterCost.costnumeric['gt'] = $inputCost;
			else if ($inputOperatorCost == 'lt')
				filterCost.costnumeric['lt'] = $inputCost;
		} 

		if ($inputUnique != 'Any'){
			if ($inputUnique == 'NonUnique')
				uniqueness = "N";
			else
				uniqueness = "Y";
			filterUnique.unique = {};
			filterUnique.unique['=='] = uniqueness;

		} 
		if ($inputEncounter != 'Any'){
			filterEncounter.encounter= $inputEncounter;

		} 

		if ($inputFight != 'Any'){
			filterFight.fght = {};
			if ($inputOperatorFight == 'e'){
				filterFight.fght['=='] = $inputFight;
			}
			else if ($inputOperatorFight == 'gt'){
				filterFight.fght['gt'] = $inputFight;
			}
			else if ($inputOperatorFight == 'lt'){
				filterFight.fght['lt'] = $inputFight;
			}
		} 
		if ($inputEvade != 'Any'){
			filterEvade.evade = {};
			if ($inputOperatorEvade == 'e'){
				filterEvade.evade['=='] = $inputEvade;
			}
			else if ($inputOperatorEvade == 'gt'){
				filterEvade.evade['gt'] = $inputEvade;
			}
			else if ($inputOperatorEvade == 'lt'){
				filterEvade.evade['lt'] = $inputEvade;
			}
		} 
		if ($inputDamage != 'Any'){
			filterDamage.dmg = {};
			if ($inputOperatorDamage == 'e'){
				filterDamage.dmg['=='] = $inputDamage;
			}
			else if ($inputOperatorDamage == 'gt'){
				filterDamage.dmg['gt'] = $inputDamage;
			}
			else if ($inputOperatorDamage == 'lt'){
				filterDamage.dmg['lt'] = $inputDamage;
			}
		} 
		if ($inputHorror != 'Any'){
			filterHorror.horr = {};
			if ($inputOperatorHorror == 'e'){
				filterHorror.horr['=='] = $inputHorror;
			}
			else if ($inputOperatorHorror == 'gt'){
				filterHorror.horr['gt'] = $inputHorror;
			}
			else if ($inputOperatorHorror == 'lt'){
				filterHorror.horr['lt'] = $inputHorror;
			}
		}

		if ($inputShroud != 'Any'){
			filterShroud.shrd = {};
			if ($inputOperatorShroud == 'e'){
				filterShroud.shrd['=='] = $inputShroud;
			}
			else if ($inputOperatorShroud == 'gt'){
				filterShroud.shrd['gt'] = $inputShroud;
			}
			else if ($inputOperatorShroud == 'lt'){
				filterShroud.shrd['lt'] = $inputShroud;
			}
		}

		if ($inputClue != 'Any'){
			filterClue.clue = {};
			if ($inputOperatorClue == 'e'){
				filterClue.clue['=='] = $inputClue;
			}
			else if ($inputOperatorClue == 'gt'){
				filterClue.clue['gt'] = $inputClue;
			}
			else if ($inputOperatorClue == 'lt'){
				filterClue.clue['lt'] = $inputClue;
			}
		}

		if ($inputVictory != 'Any'){
			filterVictory.vctry = {};
			if ($inputOperatorVictory == 'e'){
				filterVictory.vctry['=='] = $inputVictory;
			}
			else if ($inputOperatorVictory == 'gt'){
				filterVictory.vctry['gt'] = $inputVictory;
			}
			else if ($inputOperatorVictory == 'lt'){
				filterVictory.vctry['lt'] = $inputVictory;
			}
		}

		if ($inputLevel != 'Any'){
			filterLevel.lvl = {};
			if ($inputOperatorLevel == 'e'){
				filterLevel.lvl['=='] = $inputLevel;
			}
			else if ($inputOperatorLevel == 'gt'){
				filterLevel.lvl['gt'] = $inputLevel;
			}
			else if ($inputOperatorLevel == 'lt'){
				filterLevel.lvl['lt'] = $inputLevel;
			}
		} 

		if ($inputWillpower != 'Any'){
			filterWillpower.will = {};
			if ($inputOperatorWillpower == 'e'){
				filterWillpower.will['=='] = $inputWillpower;
			}
			else if ($inputOperatorWillpower == 'gt'){
				filterWillpower.will['gt'] = $inputWillpower;
			}
			else if ($inputOperatorWillpower == 'lt'){
				filterWillpower.will['lt'] = $inputWillpower;
			}
		} 

		if ($inputIntellect != 'Any'){
			filterIntellect.int = {};
			if ($inputOperatorIntellect == 'e'){
				filterIntellect.int['=='] = $inputIntellect;
			}
			else if ($inputOperatorIntellect == 'gt'){
				filterIntellect.int['gt'] = $inputIntellect;
			}
			else if ($inputOperatorIntellect == 'lt'){
				filterIntellect.int['lt'] = $inputIntellect;
			}
		} 

		if ($inputCombat != 'Any'){
			filterCombat.cmbt = {};
			if ($inputOperatorCombat == 'e'){
				filterCombat.cmbt['=='] = $inputCombat;
			}
			else if ($inputOperatorCombat == 'gt'){
				filterCombat.cmbt['gt'] = $inputCombat;
			}
			else if ($inputOperatorCombat == 'lt'){
				filterCombat.cmbt['lt'] = $inputCombat;
			}
		} 

		if ($inputAgility != 'Any'){
			filterAgility.agi = {};
			if ($inputOperatorAgility == 'e'){
				filterAgility.agi['=='] = $inputAgility;
			}
			else if ($inputOperatorAgility == 'gt'){
				filterAgility.agi['gt'] = $inputAgility;
			}
			else if ($inputOperatorAgility == 'lt'){
				filterAgility.agi['lt'] = $inputAgility;
			}
		} 

		if ($inputWild != 'Any'){
			filterWild.wild = {};
			if ($inputOperatorWild == 'e'){
				filterWild.wild['=='] = $inputWild;
			}
			else if ($inputOperatorWild == 'gt'){
				filterWild.wild['gt'] = $inputWild;
			}
			else if ($inputOperatorWild == 'lt'){
				filterWild.wild['lt'] = $inputWild;
			}
		} 

		if ($inputHealth != 'Any'){
			filterHealth.hlth = {};
			if ($inputOperatorHealth == 'e'){
				filterHealth.hlth['=='] = $inputHealth;
			}
			else if ($inputOperatorHealth == 'gt'){
				filterHealth.hlth['gt'] = $inputHealth;
			}
			else if ($inputOperatorHealth == 'lt'){
				filterHealth.hlth['lt'] = $inputHealth;
			}
		}

		if ($inputSanity != 'Any'){
			filterSanity.snty = {};
			if ($inputOperatorSanity == 'e'){
				filterSanity.snty['=='] = $inputSanity;
			}
			else if ($inputOperatorSanity == 'gt'){
				filterSanity.snty['gt'] = $inputSanity;
			}
			else if ($inputOperatorSanity == 'lt'){
				filterSanity.snty['lt'] = $inputSanity;
			}
		}
		if ($inputSlot != 'Any'){
			filterSlot.slot = {};
			filterSlot.slot['=='] = $inputSlot;

		}
		
		if (!$inputGuardian||!$inputMystic||!$inputRogue||!$inputSeeker||!$inputSurvivor||!$inputNeutral){
			filterClass = [];
			if ($inputGuardian){
				filterClass.push({clss:"Guardian"});
			}

			if ($inputMystic){
				filterClass.push({clss:"Mystic"});
			}

			if ($inputRogue){
				filterClass.push({clss:"Rogue"});
			} 

			if ($inputSeeker){
				filterClass.push({clss:"Seeker"});
			}

			if ($inputSurvivor){
				filterClass.push({clss:"Survivor"});
			} 
			if ($inputNeutral){
				filterClass.push({clss:"Neutral"});
			}
			
		}

		if ($inputSet != "0"){
			
			filterSets = {setid:$inputSet};
		}

		if ($inputSelectedOrder == 'Cost'){
			orderBy = "costnumeric asec, name asec";
		} else {
			orderBy = "name asec";
		}

		textArray = $inputText.match(/[a-zA-Z][a-zA-Z]:"(?:\\"|[^"])+"|"(?:\\"|[^"])+"|[+|-]?\w+/g);

		if (textArray != null){
			for (i=0;i<textArray.length;i++){
				if (textArray[i][0] == '"'){
					item = textArray[i].replace(/["']/g, "")
				} else {
					item = textArray[i];
				}
				console.log("item",item);
				
				regexTextString +='(?=.*'+item+')';
			}

			//textRegex=new RegExp(regexString,'i');
			textRegex=new RegExp(regexTextString,'i');
			searchfield = $("#filterTextField").val();

			switch(searchfield)
			{
				case "Traits":
				  filterText = {traits:{regex:textRegex}};
				  break;
				case "Name":
				  filterText = {name:{regex:textRegex}};
				  break;
				case "text":
				default:
				  filterText = {text:{regex:textRegex}};
			}
			
		}

		record =$.extend(true, [], db(filterVictory,filterClue,filterShroud,filterHorror,filterDamage,filterEvade,filterFight,filterEncounter,filterIntellect,filterCombat,filterAgility,filterWild,filterHealth,filterSanity,filterWillpower,filterLevel,filterSlot,filterType,filterClass,filterUnique,filterText,filterSets, filterCost).order(orderBy).get());
		return (record);
		
	}

	function prefillSearch(){
		var exists = false;
		
			if (typeof(location.querystring["tx"]) !== "undefined"){
				$('#filterText').val(location.querystring["tx"]);
				exists = true;
			}
			if (typeof(location.querystring["or"]) !== "undefined"){
				$('#selectOrderBy').val(location.querystring["or"]);
				exists = true;
			}
			if (typeof(location.querystring["vw"]) !== "undefined"){
				$('#selectView').val(location.querystring["vw"]);
				exists = true;
			}
			if (typeof(location.querystring["set"]) !== "undefined"){
				$('#filterSet').val(decodeURIComponent(location.querystring["set"]));
				exists = true;
			}
			if (typeof(location.querystring["uni"]) !== "undefined"){
				$('#filterUnique').val(location.querystring["uni"]);
				exists = true;
			}
			if (typeof(location.querystring["ens"]) !== "undefined"){
				$('#filterEncounter').val(location.querystring["ens"]);
				exists = true;
			}
			if (typeof(location.querystring["typ"]) !== "undefined"){
				var typeFilterQuerystringValue = location.querystring["typ"].split('%2C')
				$('#filterType').val(typeFilterQuerystringValue).trigger("change");
				//$("#filterType").select2("val", location.querystring["typ"]); 
				exists = true;
			}
			if (typeof(location.querystring["f"]) !== "undefined"){
				exists = true;
				if (location.querystring["f"].indexOf("gu")===-1)
					$('#GuardianFB').prop('checked',false);
				if (location.querystring["f"].indexOf("my")===-1)
					$('#MysticFB').prop('checked',false);
				if (location.querystring["f"].indexOf("ro")===-1)
					$('#RogueFB').prop('checked',false);
				if (location.querystring["f"].indexOf("se")===-1)
					$('#SeekerFB').prop('checked',false);
				if (location.querystring["f"].indexOf("su")===-1)
					$('#SurvivorFB').prop('checked',false);
				if (location.querystring["f"].indexOf("nu")===-1)
					$('#neutralFB').prop('checked',false);

			}

			if (typeof(location.querystring["cst"]) !== "undefined"){
				if(location.querystring["cst"][0]=='e')
					$('#operatorCost').val("e");
				if(location.querystring["cst"][0]=='l')
					$('#operatorCost').val("lt");
				if(location.querystring["cst"][0]=='g')
					$('#operatorCost').val("gt");
				$('#filterCost').val(location.querystring["cst"].substring(1));
				exists = true;
			}

			if (typeof(location.querystring["lvl"]) !== "undefined"){
				if(location.querystring["lvl"][0]=='e')
					$('#operatorLevel').val("e");
				if(location.querystring["lvl"][0]=='l')
					$('#operatorLevel').val("lt");
				if(location.querystring["lvl"][0]=='g')
					$('#operatorLevel').val("gt");
				$('#filterLevel').val(location.querystring["lvl"].substring(1));
				exists = true;
			}

			if (typeof(location.querystring["wlp"]) !== "undefined"){
				if(location.querystring["wlp"][0]=='e')
					$('#operatorWillpower').val("e");
				if(location.querystring["wlp"][0]=='l')
					$('#operatorWillpower').val("lt");
				if(location.querystring["wlp"][0]=='g')
					$('#operatorWillpower').val("gt");
				$('#filterWillpower').val(location.querystring["wlp"].substring(1));
				exists = true;
			}
			if (typeof(location.querystring["int"]) !== "undefined"){
				if(location.querystring["int"][0]=='e')
					$('#operatorIntellect').val("e");
				if(location.querystring["int"][0]=='l')
					$('#operatorIntellect').val("lt");
				if(location.querystring["int"][0]=='g')
					$('#operatorIntellect').val("gt");
				$('#filterIntellect').val(location.querystring["int"].substring(1));
				exists = true;
			}
			if (typeof(location.querystring["cmb"]) !== "undefined"){
				if(location.querystring["cmb"][0]=='e')
					$('#operatorCombat').val("e");
				if(location.querystring["cmb"][0]=='l')
					$('#operatorCombat').val("lt");
				if(location.querystring["cmb"][0]=='g')
					$('#operatorCombat').val("gt");
				$('#filterCombat').val(location.querystring["cmb"].substring(1));
				exists = true;
			}
			if (typeof(location.querystring["agi"]) !== "undefined"){
				if(location.querystring["agi"][0]=='e')
					$('#operatorAgility').val("e");
				if(location.querystring["agi"][0]=='l')
					$('#operatorAgility').val("lt");
				if(location.querystring["agi"][0]=='g')
					$('#operatorAgility').val("gt");
				$('#filterAgility').val(location.querystring["agi"].substring(1));
				exists = true;
			}
			if (typeof(location.querystring["wld"]) !== "undefined"){
				if(location.querystring["wld"][0]=='e')
					$('#operatorWild').val("e");
				if(location.querystring["wld"][0]=='l')
					$('#operatorWild').val("lt");
				if(location.querystring["wld"][0]=='g')
					$('#operatorWild').val("gt");
				$('#filterWild').val(location.querystring["wld"].substring(1));
				exists = true;
			}
			if (typeof(location.querystring["hlh"]) !== "undefined"){
				if(location.querystring["hlh"][0]=='e')
					$('#operatorHealth').val("e");
				if(location.querystring["hlh"][0]=='l')
					$('#operatorHealth').val("lt");
				if(location.querystring["hlh"][0]=='g')
					$('#operatorHealth').val("gt");
				$('#filterHealth').val(location.querystring["hlh"].substring(1));
				exists = true;
			}
			if (typeof(location.querystring["snt"]) !== "undefined"){
				if(location.querystring["snt"][0]=='e')
					$('#operatorSanity').val("e");
				if(location.querystring["snt"][0]=='l')
					$('#operatorSanity').val("lt");
				if(location.querystring["snt"][0]=='g')
					$('#operatorSanity').val("gt");
				$('#filterSanity').val(location.querystring["snt"].substring(1));
				exists = true;
			}
			if (typeof(location.querystring["fig"]) !== "undefined"){
				if(location.querystring["fig"][0]=='e')
					$('#operatorFight').val("e");
				if(location.querystring["fig"][0]=='l')
					$('#operatorFight').val("lt");
				if(location.querystring["fig"][0]=='g')
					$('#operatorFight').val("gt");
				$('#filterFight').val(location.querystring["fig"].substring(1));
				exists = true;
			}
			if (typeof(location.querystring["evd"]) !== "undefined"){
				if(location.querystring["evd"][0]=='e')
					$('#operatorEvade').val("e");
				if(location.querystring["evd"][0]=='l')
					$('#operatorEvade').val("lt");
				if(location.querystring["evd"][0]=='g')
					$('#operatorEvade').val("gt");
				$('#filterEvade').val(location.querystring["evd"].substring(1));
				exists = true;
			}
			if (typeof(location.querystring["dmg"]) !== "undefined"){
				if(location.querystring["dmg"][0]=='e')
					$('#operatorDamage').val("e");
				if(location.querystring["dmg"][0]=='l')
					$('#operatorDamage').val("lt");
				if(location.querystring["dmg"][0]=='g')
					$('#operatorDamage').val("gt");
				$('#filterDamage').val(location.querystring["dmg"].substring(1));
				exists = true;
			}
			if (typeof(location.querystring["hor"]) !== "undefined"){
				if(location.querystring["hor"][0]=='e')
					$('#operatorHorror').val("e");
				if(location.querystring["hor"][0]=='l')
					$('#operatorHorror').val("lt");
				if(location.querystring["hor"][0]=='g')
					$('#operatorHorror').val("gt");
				$('#filterHorror').val(location.querystring["hor"].substring(1));
				exists = true;
			}
			if (typeof(location.querystring["shd"]) !== "undefined"){
				if(location.querystring["shd"][0]=='e')
					$('#operatorShroud').val("e");
				if(location.querystring["shd"][0]=='l')
					$('#operatorShroud').val("lt");
				if(location.querystring["shd"][0]=='g')
					$('#operatorShroud').val("gt");
				$('#filterShroud').val(location.querystring["shd"].substring(1));
				exists = true;
			}
			if (typeof(location.querystring["clu"]) !== "undefined"){
				if(location.querystring["clu"][0]=='e')
					$('#operatorClue').val("e");
				if(location.querystring["clu"][0]=='l')
					$('#operatorClue').val("lt");
				if(location.querystring["clu"][0]=='g')
					$('#operatorClue').val("gt");
				$('#filterClue').val(location.querystring["clu"].substring(1));
				exists = true;
			}
			if (typeof(location.querystring["vct"]) !== "undefined"){
				if(location.querystring["vct"][0]=='e')
					$('#operatorVictory').val("e");
				if(location.querystring["vct"][0]=='l')
					$('#operatorVictory').val("lt");
				if(location.querystring["vct"][0]=='g')
					$('#operatorVictory').val("gt");
				$('#filterVictory').val(location.querystring["vct"].substring(1));
				exists = true;
			}

			return(exists);
	}
	if (location.querystring["advanced"]=="true"){
		$('#divMoreSearchButton').hide();
		$('#divMoreSearch').slideDown();
		advanced=true;
	}
	if (searchExists){
		$("#ahdb-submit-button").trigger('click');
	}
});
