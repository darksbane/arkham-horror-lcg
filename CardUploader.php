 <?php

class CardUploadHandler {
    const USER_ID = 26685;
	const IP_ADDRESS = "107.0.239.179"; 
	
	var $db_id;  // Database ID depending on the game
	var $r;
	
	function __construct($registry, $DB) {
   		$this->registry = $registry;
		$this->DB = $DB;
	}
	
    // Add $num articles of $artnr to the cart
	var $product_name;
    function insertCard() {
		echo "insert card method";	
    }
	
	function checkCredentials() {
		$ip = $_SERVER['REMOTE_ADDR'];
		if ($_SERVER['REQUEST_METHOD'] == "POST" && $_POST['api_key'] == "39473922349asrfreernferf") {
			return true;
		} else {
			echo "NOT AUTHORIZED: ".$ip;
			$this->registry->output->setHeaderCode( 403, "Forbidden" );
			return false;
		}
	}

    function processRequest() {
		// generic processing for request
    	// check category
		if (!$this->checkCredentials()) {
			return;
		}
		echo "REQUEST METHOD: ".$_SERVER['REQUEST_METHOD'];
		$this->product_name = $_POST['set'];
		//$product_name = $this->DB->addSlashes($_POST["set"]);
		$product_name = IPSText::parseCleanValue(urldecode($_POST["set"]));
		echo "Got product name:".$product_name;
		
		$this->DB->build( array( 'select' => '*', 'from' => 'ccs_database_categories', 'where' => 'category_database_id='.$this->db_id.' and category_name="'.$product_name.'"' ) );
		echo "Built Query!";
		$data = $this->DB->execute();
		$category_exist = false;
		$affected_rows = $this->DB->getAffectedRows();
		echo $affected_rows;
		while ( $row = $this->DB->fetch($data) ){
			$category_exist = true;
			$this->category_id = $row['category_id'];
			echo "Category already exists:".$category_id;
		}
		if (!$affected_rows) {
			echo "Creating category ...";
			// check if the category for the cycle exist
			// if it doesn't create a category for the cycle
			// create the category for the product
		}
		echo "In process request";
		//check if the record exists
		if(!$this->cardExist()) {
			echo "Card doesn't exist created a new card.";
			$this->createCard();
		} else {
			echo "Card exist updating values new card.";
			$this->updateCard();
		}
	}

	function cardExist() {
		echo "in cardExist:";
		$ffg_id = $_POST['ffg_id'];
		$table = 'ccs_custom_database_'.$this->db_id;
		$where = $this->FFG_ID.'="'.$ffg_id.'"';
		echo $table.' '.$where;
		$this->DB->build( array( 'select' => '*', 'from' => $table, 'where' => $where) );	
		echo "built query";
		$this->DB->execute();
		echo "executed query";
		$exist = $this->DB->getAffectedRows();
		return $exist;
	}
	
	function createCard() {
		echo "Creating new card !";
		$fields = $this->retrieveFields();
        $this->DB->insert('ccs_custom_database_'.$this->db_id, $fields);
		echo "Inserted new card !";
       	$this->updateCategoryCount();
		echo "Updated category count !";
		// update category card count;
	}
	
	function updateCard() {
		$fields = $this->retrieveFields();
		echo "Updating card :";
		if ($fields[$this->FFG_ID]) {
			$where = $this->FFG_ID."='".$fields[$this->FFG_ID]."'";
			echo "WHERE CLAUSE: ".$where;
	        $this->DB->update('ccs_custom_database_'.$this->db_id, $fields, $where);
	 		"Category count updated !";
	 	}
		// update category card count;
	}
	
	function updateCategoryCount() {
		$result = $this->DB->query("SELECT count(*) from ccs_custom_database_".$this->db_id." where category_id=".$this->category_id);
		$row = $result->fetch_row();
		echo "CARDs in CATEGORY: ".$row[0];
		$this->DB->update('ccs_database_categories', array('category_records' => $row[0]), 'category_id='.$this->category_id);	
	}
	
	function retrieveFields() {
        $db_fields = array();
        $r = $_POST;
        foreach ($this->fields as $key => $value) {
            // check if the value is set.
			if ($r[$value] || $r[$value]=="" || $r[$value]==0){
                echo "\nKey: ".$value. " Value: ".$r[$value];
                $db_fields[$key] = $r[$value];
            }
        }
		$db_fields['category_id'] = $this->category_id;
		$db_fields['record_approved'] = 1;
        return $db_fields;
    }

	function createCategory() {
		echo "WARNING missing category, category should be created !";
	}
}

class ConquestCardUploadHandler extends CardUploadHandler {
	var $FFG_ID = 'field_518';
	var $db_id='34';	
	
	var $fields = array(
		'field_432' => 'card_name',
		'field_493' => 'card_image',
		'field_517' => 'card_back_image',
		'field_436' => 'card_type',
		'field_435' => 'faction',
		'field_433' => 'unique',
		'field_434' => 'cost',
		'field_474' => 'planet_types',
		'field_476' => 'card_bonus',
		'field_475' => 'resource_bonus',
		'field_477' => 'command_icons',
		'field_478' => 'shields',
		'field_479' => 'attack_value',
		'field_480' => 'hit_points',
		'field_482' => 'traits',
		'field_483' => 'game_text',
		'field_491' => 'flavor_text',
		'field_484' => 'signature',
		'field_485' => 'starting_hand_size',
		'field_486' => 'starting_resource',
		'field_487' => 'squad_number',
		'field_488' => 'in_squad_number',
		'field_489' => 'card_number',
		'field_490' => 'illustrator',
		'field_492' => 'set',
		'field_494' => 'quantity',
		'field_527' => 'max_number',
		'field_518' => 'ffg_id'
	);	

	function __construct($registry, $DB) {
       parent::__construct($registry, $DB);
       print "In SubClass constructor\n";
   	}

}

class LotRCardUploaderHandler extends CardUploadHandler {
	var $FFG_ID = 'field_544';
	var $db_id='12';	
	
	var $fields = array(
		'field_141' => 'card_name',
		'field_158' => 'card_image',
		'field_144' => 'unique',
		'field_142' => 'card_type',
		'field_161' => 'card_deck',
		'field_143' => 'card_sphere',
		'field_162' => 'card_encounter_info',
		'field_165' => 'card_encounter_list',
		'field_151' => 'cost',
		'field_152' => 'startingthreat_threshold',
		'field_150' => 'willpower_threat',
		'field_148' => 'attack',
		'field_149' => 'defense',
		'field_147' => "hp_questp",
		'field_146' => "traits",
		'field_145' => "game_text",
		'field_153' => "shadow_text",
		'field_157' => "flavor_text",
		'field_154' => "set",
		'field_155' => "number",
		'field_156' => "illustrator",
		'field_163' => "quest_seq_num",
		'field_164' => "show_in_deckbuilder",
		'field_183' => "quantity",
		'field_437'	=> "max_number",
		'field_438'	=> "victory",
		
		'field_544' => 'ffg_id'
	);	

	function __construct($registry, $DB) {
       parent::__construct($registry, $DB);
       print "In SubClass constructor\n";
   	}
}

class NetrunnerCardUploaderHandler extends CardUploadHandler {
	var $FFG_ID = 'field_545';
	var $db_id='21';	
	
	var $fields = array(
		'field_225' => 'card_name',
		'field_237' => "number",
		'field_263' => 'unique',
		'field_241' => 'side', // Corp or Runner
		'field_226' => 'type', // card type Event | Program | Hardware | Resource | Operation | Agenda | Node | Asset | ICE | Upgrade | Identity
		'field_227' => 'subtype', // subtype for ice and programs
		'field_228' => 'cost',
		'field_229' => 'strength',
		'field_270' => 'agenda_points',
		'field_230' => 'memory',
		'field_231' => 'trash_cost',
		'field_232' => 'identity', // identity for corp and runners Anarch | Criminal | Shaper | Haas-Bioroid | Jinteki | The Weyland Consortium | NBN | Neutral
		'field_233' => 'identity_cost', // out of faction cost
		'field_234' => 'game_text', // game text for the card
		'field_235' => 'flavor_text', // flavor text for the card
		'field_236' => 'set', // card set
		'field_238' => 'quantity', // how many cards in the product
		'field_239' => 'illustrator',
		'field_240' => 'image', // card image file name
		'field_261' => 'min_deck_size', // minimum deck size for ideentoty cards
		'field_262' => 'influence_limit', // influence limit for identity cards
		'field_264' => 'runner_base_link', // runner base link
		'field_269' => 'errata', // errata on the card
		'field_455' => 'restricted', // "Restricted" or ""
		'field_456' => 'max_number', // Max copies in a deck
		'field_310' => 'show_in_builder', //
		
		'field_545' => 'ffg_id'
	);	

	function __construct($registry, $DB) {
       parent::__construct($registry, $DB);
       print "In SubClass constructor\n";
   	}
}

class StarWarsCardUploaderHandler extends CardUploadHandler {
	var $FFG_ID = 'field_546';
	var $db_id='24';	
	
	var $fields = array(
		'field_285' => 'card_name',
		'field_287' => 'unique',
		'field_288' => 'cost',
		'field_352' => 'fate_icons',
		'field_289' => 'force_icons',
		'field_290' => 'side', // values "Light" or "Dark"
		'field_291' => 'affiliation', // values "Imperial Navy", "Jedi", "Rebel Alliance", "Scum and Villainy", "Sith", "Smugglers and Spies", "Neutral"
		'field_292' => 'type', // card type "Imperial Navy", "Jedi", "Rebel Alliance", "Scum and Villainy", "Sith", "Smugglers and Spies", "Neutral"
		'field_293' => 'udamage_black', // unit damage black icons on the card
		'field_294' => 'udamage_white', // unit damage white icons on the card
		'field_295' => 'bdamage_black', // blast damage black icons on the card
		'field_296' => 'bdamage_white', // blast damage white icons on the card
		'field_297' => 'tactic_black', // tactic black icons on the card
		'field_298' => 'tactic_white', // tactic white icons on the card
		'field_299' => 'traits',
		'field_286' => 'game_text',
		'field_308' => 'flavor_text',
		'field_300' => 'health',
		'field_301' => 'resources',
		'field_302' => 'set',
		'field_304' => 'number',
		'field_305' => 'block', // the number of the pod in which the card is
		'field_306' => 'in_block_num', // the sequential number inside the pod
		'field_307' => 'image', // card image file name
		'field_309' => 'illustrator',
		'field_546' => 'ffg_id'
	);	

	function __construct($registry, $DB) {
       parent::__construct($registry, $DB);
       print "In SubClass constructor\n";
   	}
}

class AGoTCardUploadeHandler extends CardUploadHandler {
	var $FFG_ID = 'field_547';
	var $db_id='3';	
	
	var $fields = array(
		'field_14' => 'card_name',
		'field_87' => 'quantity', // how many cards in the product
		'field_31' => 'name_prefix', //this is a list of string separated by comma Unique or Infinity
		'field_30' => 'type', // values are "Agenda", "Attachment", "Character", "Event", "House", "Location", "Plot"
		'field_15' => 'image', // card image file name
		'field_29' => 'max_number', // Max copies in a deck
		// NO LONGER USED 'field_16' => 'house', // list of houses on the card separated by commas "Baratheon", "Greyjoy", "Lannister", "Martell", "Stark", "Targaryen", "Neutral"
		'field_40' => 'Baratheon', // values Yes or No
		'field_41' => 'Greyjoy', // values Yes or No
		'field_42' => 'Martell', // values Yes or No
		'field_43' => 'Lannister', // values Yes or No
		'field_44' => 'Stark', // values Yes or No
		'field_45' => 'Targaryen', // values Yes or No
		'field_46' => 'Neutral', // values Yes or No
		'field_17' => 'cost',
		'field_18' => 'strength',
		//	NO LONGER USED 'field_19' => 'challenge_icons', // comma separated values "Military", "Intrigue", "Power" e.g. Intrigue, Power
		'field_47' => 'military_icon', // values Yes or No
		'field_353'=> 'military_icon_mod', // values is "Naval" or ""
		'field_48' => 'intrigue_icon', // values Yes or No
		'field_354'=> 'intrigue_icon_mod', // values is "Naval" or ""
		'field_49' => 'power_icon', // values Yes or No
		'field_355'=> 'power_icon_mod', // values is "Naval" or ""
		'field_20' => 'traits',
		'field_160' => 'rulings',
		'field_383' => 'restricted', // value is one of the following: "Restricted (Joust)", "Restricted (Melee)", "Restricted (Both)"
		'field_21' => 'game_text',
		// NO LONGER USED 'field_22' => 'card_crest', // comma separated values "Learned", "Noble", "War", "Shadow"
		'field_50' => 'holy_crest', // values Yes or No
		'field_51' => 'learned_crest', // values Yes or No
		'field_52' => 'noble_crest', // values Yes or No
		'field_53' => 'war_crest', // values Yes or No
		'field_54' => 'shadow_crest', // values Yes or No
		'field_23' => 'income',
		'field_384' => 'cost_reduction',
		'field_33' => 'influence',
		'field_24' => 'initiative',
		'field_25' => 'claim',
		'field_26' => 'flavor_text',
		'field_27' => 'number',
		'field_32' => 'set',
		'field_88' => 'illustrator',
		
		'field_547' => 'ffg_id'
	);	

	function __construct($registry, $DB) {
       parent::__construct($registry, $DB);
       print "In SubClass constructor\n";
   	}
}

class AGoT2ndCardUploadeHandler extends CardUploadHandler {
	var $FFG_ID = 'field_582';
	var $db_id='43';	
	
	var $fields = array(
		'field_551' => 'card_name',
		'field_552' => 'unique', // unique value is Y or N
		'field_553' => 'quantity', // how many cards in the product
		'field_554' => 'type', // values are "Character", "Event", "Location", "Attachment", "Plot", "Agenda", "Faction", "Title"
		'field_555' => 'image', // card image file name
		'field_556' => 'max_number', // Max copies in a deck
		'field_557' => 'number', // Card Number in the set
		'field_558' => 'set', // Card Set
		'field_559' => 'illustrator',
		
		'field_560' => 'Baratheon', // values Y or N
		'field_561' => 'Greyjoy', // values Y or N
		'field_562' => 'Lannister', // values Y or N
		'field_563' => 'Martell', // values Y or N
		'field_564' => 'Stark', // values Y or N
		'field_565' => 'Targaryen', // values Y or N
		'field_566' => 'Tyrell', // values Y or N
		'field_567' => "NightWatch", // values Y or N
		'field_568' => 'Neutral', // values Y or N
		'field_570' => 'Loyalty', // values L or empty string. L if the card is a Loyal card.
		'field_569' => 'cost',
		'field_571' => 'traits',
		'field_572' => 'game_text',
		'field_581' => 'flavor_text',
		
		'field_573' => 'military_icon', // values Yes or No
		'field_574' => 'intrigue_icon', // values Yes or No
		'field_575' => 'power_icon', // values Yes or No
		'field_576' => 'strength',
		'field_577' => 'income', // Gold the card produce
		'field_578' => 'initiative',
		'field_579' => 'claim',
		'field_580' => 'reserve',
		'field_583' => 'show_in_deckbuilder', // values is Y or N
		
		'field_582' => 'ffg_id'
	);	

	function __construct($registry, $DB) {
       parent::__construct($registry, $DB);
       print "In SubClass constructor\n";
   	}
}


class CoCCardUploaderHandler extends CardUploadHandler {
	var $FFG_ID = 'field_548';
	var $db_id='7';	
	
	var $fields = array(
		'field_60' => 'card_name',
		'field_81' => 'image', // card image file name
		'field_72' => 'number',
		'field_61' => 'descriptor',
		'field_78' => 'unique',
		'field_74' => 'type', // card type "Character", "Conspiracy", "Event", "Story", "Support"
		'field_62' => 'cost',
		'field_63' => 'faction', // faction name "Hastur", "Miskatonic University", "Shub-Niggurath", "Silver Twilight", "Syndicate", "The Agency", "Yog-Sothoth", "Neutral"
		'field_84' => 'icons', // Icons on the card in order. Ex (T)(C)(A)(I) TODO ask Drew
		'field_64' => 'terror_icons', // number of terror icons
		'field_65' => 'combat_icons',
		'field_66' => 'arcane_icons',
		'field_67' => 'investigation_icons',
		'field_68' => 'skill',
		'field_69' => 'subtype',
		'field_70' => 'game_text',
		'field_79' => 'special attribute', //This is something like Steadfast, Zoog Resource, or Transient TODO ask Drew
		'field_85' => 'steadfast_count',
		'field_86' => 'steadfast_faction', // faction name "Hastur", "Miskatonic University", "Shub-Niggurath", "Silver Twilight", "Syndicate", "The Agency", "Yog-Sothoth", "Neutral"
		'field_77' => 'flavor_text',
		'field_75' => 'illustrator',
		'field_82' => 'set',
		'field_404' => 'restricted', // value are Yes or No
		'field_431' => 'max_number', // Max copies in a deck
		'field_430' => 'banned', // value Yes or No
		
		'field_548' => 'ffg_id'
	);	

	function __construct($registry, $DB) {
       parent::__construct($registry, $DB);
       print "In SubClass constructor\n";
   	}
}


class CardUploaderHandlerFactory {
	public static function newInstance($db_id, $registry, $DB) {
		$db_map = Array(
            "34" => "ConquestCardUploadHandler",
            "12" => "LotRCardUploaderHandler",
			"21" => "NetrunnerCardUploaderHandler",
			"24" => "StarWarsCardUploaderHandler",
			"7" => "CoCCardUploaderHandler",
			"3" => "AGoTCardUploadeHandler",
			"43" => "AGoT2ndCardUploadeHandler"
			);
		
		return new $db_map[$db_id]($registry, $DB);
	}
}






?>
