var section = new buildSection;
function buildSection () {
	this.typeLI = "";
	this.costLI = "";
	this.nameLI = "";
	this.weaknessLI = "";
	this.investigatorLI = "";
	this.spoilerView = "";
	this.octgnCache = "";
	this.saveTextCache = "";
	this.urlCache = "";
	this.faction = "";
	this.myid = "";
}
buildSection.prototype.build = function (deckObjectToParse){
		var displayLIIdentity = {};
		var displayLIByType = {};
		var displaySpoilerByType = {};
		var displayLIByCost = {};
		var displayLIByName = {};
		var displayURLByType = {};
		var saveTextByType = {};
		var saveOCTGN = {};
		var X = 0;
		var myid = "";
		deckObjectToParse.cards.sort(dynamicSort('name'));
		
		deckObjectToParse.cards.each(function (c){
			var myid = c.id,
				cardType = c.type,
				restrictedtext = "";
			if (c.wtype == 'Weakness' || c.wtype == 'Basic Weakness'){
				cardType = 'Weakness';
			}
			if (cardType != 'Investigator' && cardType!= 'Weakness'){
				

				if (c.rabbr != ""){
					if (c.rabbr == "RJ")
						restrictedtext = "[Restricted Joust]";
					if (c.rabbr == "RM")
						restrictedtext = "[Restricted Melee]";
					if (c.rabbr == "RB")
						restrictedtext = "[Restricted Both]";
				}
				
				if (typeof displayLIByCost[c.cost] == "undefined"){
					displayLIByCost[c.cost] = "";
				} 
				displayLIByCost[c.cost] += cardListTemplate(c);
				
				if (typeof displayLIByType[cardType] == "undefined"){
					displayLIByType[cardType] = "";
				}
				displayLIByType[cardType] += cardListTemplate(c);
				if (typeof displaySpoilerByType[cardType] == "undefined"){
					displaySpoilerByType[cardType] = "";
				}
				displaySpoilerByType[cardType] += cardSpoilerTemplate(c, 'spoiler');
				if (typeof saveTextByType[cardType] == "undefined"){
					saveTextByType[cardType] = "";
				}
				saveTextByType[cardType] += c.deckquantity+"x "+c.name+" ["+c.lvl+"] ("+c.setname+") "+restrictedtext+"\n";
				if (typeof displayURLByType[cardType] == "undefined"){
					displayURLByType[cardType] = "";
				}

				displayURLByType[cardType] += c.deckquantity+"x [url="+c.fullurl+"]"+c.name+" ["+c.lvl+"] ("+c.setname+")[/url] "+restrictedtext+"\n";
				cardName = c.name;
				if (typeof displayLIByName[cardName] == "undefined"){
					displayLIByName[cardName] = "";
				}
				displayLIByName[cardName] += cardListTemplate(c);
				if (typeof saveOCTGN['Deck'] == "undefined"){
					saveOCTGN['Deck'] = "";
				}

			} else {
				if (typeof displayLIByType[cardType] == "undefined"){
					displayLIByType[cardType] = "";
				}
				if (typeof displaySpoilerByType[cardType] == "undefined"){
					displaySpoilerByType[cardType] = "";
				}
				if (typeof saveTextByType[cardType] == "undefined"){
					saveTextByType[cardType] = "";
				}
				if (typeof displayURLByType[cardType] == "undefined"){
					displayURLByType[cardType] = "";
				}
				displayLIByType[cardType] += cardListTemplate(c);
				displaySpoilerByType[cardType] += cardSpoilerTemplate(c, 'spoiler');
				saveTextByType[cardType] += c.deckquantity+"x "+c.name+" ("+c.setname+") "+restrictedtext+"\n";
				displayURLByType[cardType] += c.deckquantity+"x [url="+c.fullurl+"]"+c.name+" ("+c.setname+")[/url] "+restrictedtext+"\n";
			}
			
		});

		var tList = "",
		wList = "",
		iList = "";
		deckObject.arkhamhorror.typeList.each(function (section){
			if (section != 'Investigator' && section != 'Weakness'){
				if (typeof displayLIByType[section] != "undefined")
					tList += "<li data-id='"+section.replace(/ /g, '')+"' style='padding-top:10px'><b>"+section+"</b> ("+deckObject.arkhamhorror.counts[section.toLowerCase().replace(/ /g, '').replace("-","")]+")</li>"+displayLIByType[section];
				else
					tList += "<li data-id='"+section.replace(/ /g, '')+"' style='padding-top:10px'><b>"+section+"</b> (0)</li>";
			} else if (section =='Weakness'){
				if (typeof displayLIByType[section] != "undefined")
					wList += "<li data-id='"+section.replace(/ /g, '')+"' style=''><b>Weakness</b> </li>"+displayLIByType[section];
				else
					wList += "<li data-id='"+section.replace(/ /g, '')+"' style=''><b>Weakness</b> </li>";
			} else if (section =='Investigator'){
				if (typeof displayLIByType[section] != "undefined")
					iList += "<li data-id='"+section.replace(/ /g, '')+"' style=''><b>Investigator</b> </li>"+displayLIByType[section];
				else
					iList += "<li data-id='"+section.replace(/ /g, '')+"' style=''><b>Investigator</b></li>";
			}
		});
		this.typeLI = tList;
		this.weaknessLI = wList;
		this.investigatorLI = iList;
		
		var sList = "";
		var stList = "";
		var urlList = "";
		var octgnList = "";
		deckObject.arkhamhorror.typeList.each(function(section){
			if (section != 'Faction'){
				if (typeof displaySpoilerByType[section] != "undefined")
					sList += "<li data-id='"+section.replace(/ /g, '')+"' class='spoilerHeader' style='padding-top:10px'><b>"+section+"</b> ("+deckObject.arkhamhorror.counts[section.toLowerCase().replace(/ /g, '').replace("-","")]+")</li>"+displaySpoilerByType[section];
				else
					sList += "<li data-id='"+section.replace(/ /g, '')+"' class='spoilerHeader' style='padding-top:10px'><b>"+section+"</b> (0)</li>";
				if (typeof saveTextByType[section] != "undefined")
					stList += ""+section+" ("+deckObject.arkhamhorror.counts[section.toLowerCase().replace(/ /g, '').replace("-","")]+")\n"+saveTextByType[section]+"\n\n";
				else
					stList += " "+section+" (0)\n\n";
					
				if (typeof displayURLByType[section] != "undefined")
					urlList += "[b]"+section+" ("+deckObject.arkhamhorror.counts[section.toLowerCase().replace(/ /g, '').replace("-","")]+")[/b]\n"+displayURLByType[section]+"\n\n";
				else
					urlList += "[b]"+section+"[/b] (0)\n\n";
			} else {
				if (typeof displaySpoilerByType[section] == "undefined")
					displaySpoilerByType[section] == "";
			}
		
		});
		
		this.spoilerView = sList;
		this.urlCache = "Class: \n"+deckObject.class+"\n\n"+urlList;
		this.saveTextCache = "Class: \n"+deckObject.class+"\n\n"+stList;
		
		var cList = "";
		for (var prop in displayLIByCost) {
			/**/if (typeof deckObjectToParse.arkhamhorror.costs[prop] != "undefined"){
				costQ = deckObjectToParse.arkhamhorror.costs[prop];
			} else {
				costQ = X;
			}
			
			cList += "<li data-id='cst-"+prop+"' style='padding-top:10px'><b>Cost "+prop+"</b> ("+costQ+")</li>";
			cList += displayLIByCost[prop];
		}
		this.costLI = cList;
		
		var nList = "";
		for (var prop in displayLIByName) {
			nList += displayLIByName[prop];
		}
		this.nameLI = nList;
		
	}
