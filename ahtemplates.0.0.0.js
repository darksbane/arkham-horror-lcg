	function cardListTemplate(record){
		var liTemplate = '<li class="deckcard {{type}}" data-id = "{{id}}">{{quantity}} <a href="{{fullurl}}" class="{{type}}Hover" data-cid = "{{id}}" data-img = "{{img}}" data-type= "{{type}}" data-card="{{card}}" title="{{name}}">{{name}}</a> <span class="{{restrictedclass}}">{{restricted}}</span> {{level}} ({{setname}})</li>',

		prefix = "med_",
		_image = '',
		_level = '',
		gametext,
		deckquantity ='',
		restrictedText = '',
		restrictedClass = ''
		cardType = record.type;
		//if (cardType != 'Investigator'){
			deckquantity = record.deckquantity+'x';
		//}
		if (record.wtype == "Weakness" || record.wtype == "Basic Weakness"){
			cardType = "Weakness";
		}
		if (cardType != "Investigator" && cardType != "Weakness"){
			_level = "["+record.lvl+"]"
		}
		if (record.traits != ''){
			gametext = "<strong>"+record.traits+"</strong><br />"+record.text;
		} else {
			gametext = record.text;
		}
		if (record.restricted == 'R'){
			restrictedText = 'R';
			restrictedClass = 'R';
		}
		if (record.banned == 'B'){
			restrictedText = 'Banned';
			restrictedClass = 'R';
		}
		if (record.imgf.startsWith('http://') || record.imgf.startsWith('https://')){
			_image = record.imgf;
		} else {
			_image = ahGlobals.imagepath+record.imgf;
		}
		theLI = liTemplate	.replace(/{{type}}/gi, cardType.toLowerCase().replace(/ /g, '').replace("-",""))
					.replace(/{{quantity}}/gi, deckquantity)
//					.replace(/{{factionasclass}}/gi, record.faction.toLowerCase().replace(/ /g, '').replace("-",""))
					.replace(/{{fullurl}}/gi, record.fullurl)
					.replace(/{{card}}/gi, Object.toJSON(record).replace(/"/g, '&quot;'))
					.replace(/{{id}}/gi, record.id)
					.replace(/{{imgfolder}}/gi, ahGlobals.imagepath)
					.replace(/{{imgprefix}}/gi, prefix )
					.replace(/{{img}}/gi, _image)
					.replace(/{{level}}/gi, _level)
					.replace(/{{name}}/gi, record.name)
					.replace(/{{restricted}}/gi, restrictedText)
					.replace(/{{restrictedclass}}/gi, restrictedClass)
					.replace(/{{setname}}/gi, record.setname);
		return theLI;
	}
	
	function cardSpoilerTemplate(record,view,imgprefix,imgsuffix){
				var templates = [],
				_image,
				gametext,
				uniquehtml='',
				loyalText = '',
				iconDisplay = '',
				factions=[],
				stats = '',
				templateType,
				restrictedText = "",
				skilltesticons = "",
				skilltesticonslistview = "",
				slotDisplay = "",
				healthDisplay = "",
				sanityDisplay = "",
				victoryDisplay = "",
				imageName,
				rating = '',
				staron = '<img src="'+deckbuilderGlobals.image_path+'/images/star.png" alt="*" class="rate_img">',
				staroff = '<img src="'+deckbuilderGlobals.image_path+'/images/star_off.png" alt="-" class="rate_img">';


				templates['Investigator'] = 	'<li class="searchLiCard"><div class="searchDivCard"><div class="searchDivImage"><a href="{{fullurl}}"><img class="searchImgCard" src="{{img}}" alt="{{name}}"><br /><img class="searchImgCard" src="{{imgB}}" alt="{{name}}"></a><div class="rating">{{rating}}</div><div class="deckcount">x{{deckcount}}</div></div><div class="searchTextPlot"><div class="searchTextRowLabel" ></div><div class="searchTextContent" ><h1 class="searchH1Name">{{unique}}<a href="{{fullurl}}">{{name}}</a> {{subtitle}} <span class="{{restrictedClass}}">{{restrictedPhrase}}</span></h1></div><div class="searchTextRowLabel">Class:</div><div class="searchTextContent">{{class}}</div><div class="searchTextRowLabel" >Type:</div><div class="searchTextContent" >{{type}}</div><div class="searchTextRowLabel" >Skills:</div><div class="searchTextContent" >{{skilltesticons}}&nbsp;</div><div class="searchTextRowLabel" >Health:</div><div class="searchTextContent" >{{health}}&nbsp;</div><div class="searchTextRowLabel" >Sanity:</div><div class="searchTextContent" >{{sanity}}&nbsp;</div><div class="searchTextRowLabel">Card Text:</div><div class="searchTextContent">{{text}}&nbsp;</div><div class="searchTextRowLabel">Set:</div><div class="searchTextContent">{{setname}}</div><div class="searchTextRowLabel">Number:</div><div class="searchTextContent">{{cardnum}}</div><div class="searchTextRowLabel">Quantity:</div><div class="searchTextContent">{{quantity}}</div><div class="searchTextRowLabel">Illustrator:</div><div class="searchTextContent"> {{illustrator}}</div></div><div class="clear"></div><div class="cardComments"><a href="{{fullurl}}#commentsStart"><img src="/deckbuilder/comments.png">{{cardComments}}</a></div></div></li>';
				templates['Asset'] = 		'<li class="searchLiCard"><div class="searchDivCard"><div class="searchDivImage"><a href="{{fullurl}}"><img class="searchImgCard" src="{{img}}" alt="{{name}}"></a><div class="rating">{{rating}}</div><div class="deckcount">x{{deckcount}}</div></div><div class="searchText"><div class="searchTextRowLabel" ></div><div class="searchTextContent" ><h1 class="searchH1Name">{{unique}}<a href="{{fullurl}}">{{name}}</a> {{subtitle}} <span class="{{restrictedClass}}">{{restrictedPhrase}}</span></h1></div><div class="searchTextRowLabel">Class:</div><div class="searchTextContent">{{class}}</div><div class="searchTextRowLabel" >Type:</div><div class="searchTextContent" >{{type}}</div><div class="searchTextRowLabel">Cost:</div><div class="searchTextContent">{{cost}}</div><div class="searchTextRowLabel">Level:</div><div class="searchTextContent">{{level}}</div><div class="searchTextRowLabel">Skill Icons:</div><div class="searchTextContent">{{skilltesticons}}</div><div class="searchTextRowLabel {{healthDisplay}}" >Health:</div><div class="searchTextContent {{healthDisplay}}" >{{health}}&nbsp;</div><div class="searchTextRowLabel {{sanityDisplay}}" >Sanity:</div><div class="searchTextContent {{sanityDisplay}}" >{{sanity}}&nbsp;</div><div class="searchTextRowLabel {{slotDisplay}}">Slot:</div><div class="searchTextContent {{slotDisplay}}">{{slot}}</div><div class="searchTextRowLabel">Card Text:</div><div class="searchTextContent">{{text}}&nbsp;</div><div class="searchTextRowLabel">Set:</div><div class="searchTextContent">{{setname}}</div><div class="searchTextRowLabel">Number:</div><div class="searchTextContent">{{cardnum}}</div><div class="searchTextRowLabel">Quantity:</div><div class="searchTextContent">{{quantity}}</div><div class="searchTextRowLabel">Illustrator:</div><div class="searchTextContent"> {{illustrator}}</div></div><div class="clear"></div><div class="cardComments"><a href="{{fullurl}}#commentsStart"><img src="/deckbuilder/comments.png">{{cardComments}}</a></div></div></li>';
				templates['Event'] = 	'<li class="searchLiCard"><div class="searchDivCard"><div class="searchDivImage"><a href="{{fullurl}}"><img class="searchImgCard" src="{{img}}" alt="{{name}}"></a><div class="rating">{{rating}}</div><div class="deckcount">x{{deckcount}}</div></div><div class="searchText"><div class="searchTextRowLabel" ></div><div class="searchTextContent" ><h1 class="searchH1Name">{{unique}}<a href="{{fullurl}}">{{name}}</a> {{subtitle}} <span class="{{restrictedClass}}">{{restrictedPhrase}}</span></h1></div><div class="searchTextRowLabel">Class:</div><div class="searchTextContent">{{class}}</div><div class="searchTextRowLabel" >Type:</div><div class="searchTextContent" >{{type}}</div><div class="searchTextRowLabel">Cost:</div><div class="searchTextContent">{{cost}}</div><div class="searchTextRowLabel">Level:</div><div class="searchTextContent">{{level}}</div><div class="searchTextRowLabel">Skill Icons:</div><div class="searchTextContent">{{skilltesticons}}</div><div class="searchTextRowLabel">Card Text:</div><div class="searchTextContent">{{text}}&nbsp;</div><div class="searchTextRowLabel">Set:</div><div class="searchTextContent">{{setname}}</div><div class="searchTextRowLabel">Number:</div><div class="searchTextContent">{{cardnum}}</div><div class="searchTextRowLabel">Quantity:</div><div class="searchTextContent">{{quantity}}</div><div class="searchTextRowLabel">Illustrator:</div><div class="searchTextContent"> {{illustrator}}</div></div><div class="clear"></div><div class="cardComments"><a href="{{fullurl}}#commentsStart"><img src="/deckbuilder/comments.png">{{cardComments}}</a></div></div></li>';
				templates['Skill'] = 		'<li class="searchLiCard"><div class="searchDivCard"><div class="searchDivImage"><a href="{{fullurl}}"><img class="searchImgCard" src="{{img}}" alt="{{name}}"></a><div class="rating">{{rating}}</div><div class="deckcount">x{{deckcount}}</div></div><div class="searchText"><div class="searchTextRowLabel" ></div><div class="searchTextContent" ><h1 class="searchH1Name">{{unique}}<a href="{{fullurl}}">{{name}}</a> {{subtitle}} <span class="{{restrictedClass}}">{{restrictedPhrase}}</span></h1></div><div class="searchTextRowLabel">Class:</div><div class="searchTextContent">{{class}}</div><div class="searchTextRowLabel" >Type:</div><div class="searchTextContent" >{{type}}</div><div class="searchTextRowLabel">Level:</div><div class="searchTextContent">{{level}}</div><div class="searchTextRowLabel">Skill Icons:</div><div class="searchTextContent">{{skilltesticons}}</div><div class="searchTextRowLabel">Card Text:</div><div class="searchTextContent">{{text}}&nbsp;</div><div class="searchTextRowLabel">Set:</div><div class="searchTextContent">{{setname}}</div><div class="searchTextRowLabel">Number:</div><div class="searchTextContent">{{cardnum}}</div><div class="searchTextRowLabel">Quantity:</div><div class="searchTextContent">{{quantity}}</div><div class="searchTextRowLabel">Illustrator:</div><div class="searchTextContent"> {{illustrator}}</div></div><div class="clear"></div><div class="cardComments"><a href="{{fullurl}}#commentsStart"><img src="/deckbuilder/comments.png">{{cardComments}}</a></div></div></li>';
				templates['Agenda'] = 	'<li class="searchLiCard"><div class="searchDivCard"><div class="searchDivImage"><a href="{{fullurl}}"><img class="searchImgCard" src="{{img}}" alt="{{name}}"><br /><img class="searchImgCard" src="{{imgB}}" alt="{{name}}"></a><div class="rating">{{rating}}</div><div class="deckcount">x{{deckcount}}</div></div><div class="searchText"><div class="searchTextRowLabel" ></div><div class="searchTextContent" ><h1 class="searchH1Name">{{unique}}<a href="{{fullurl}}">{{name}}</a> {{subtitle}} <span class="{{restrictedClass}}">{{restrictedPhrase}}</span></h1></div><div class="searchTextRowLabel">Encounter Set:</div><div class="searchTextContent">{{encounter}}</div><div class="searchTextRowLabel" >Type:</div><div class="searchTextContent" >{{type}}</div><div class="searchTextRowLabel {{victoryDisplay}}" >Victory:</div><div class="searchTextContent" >{{victory}}</div><div class="searchTextRowLabel">Card Text:</div><div class="searchTextContent">{{text}}&nbsp;</div><div class="searchTextRowLabel">Set:</div><div class="searchTextContent">{{setname}}</div><div class="searchTextRowLabel">Number:</div><div class="searchTextContent">{{cardnum}}</div><div class="searchTextRowLabel">Quantity:</div><div class="searchTextContent">{{quantity}}</div><div class="searchTextRowLabel">Illustrator:</div><div class="searchTextContent"> {{illustrator}}</div></div><div class="clear"></div><div class="cardComments"><a href="{{fullurl}}#commentsStart"><img src="/deckbuilder/comments.png">{{cardComments}}</a></div></div></li>';
				templates['Treachery'] = 		'<li class="searchLiCard"><div class="searchDivCard"><div class="searchDivImage"><a href="{{fullurl}}"><img class="searchImgCard" src="{{img}}" alt="{{name}}"></a><div class="rating">{{rating}}</div><div class="deckcount">x{{deckcount}}</div></div><div class="searchText"><div class="searchTextRowLabel" ></div><div class="searchTextContent" ><h1 class="searchH1Name">{{unique}}<a href="{{fullurl}}">{{name}}</a> {{subtitle}} <span class="{{restrictedClass}}">{{restrictedPhrase}}</span></h1></div><div class="searchTextRowLabel">Encounter Set:</div><div class="searchTextContent">{{encounter}}&nbsp;</div><div class="searchTextRowLabel" >Type:</div><div class="searchTextContent" >{{type}}</div><div class="searchTextRowLabel {{victoryDisplay}}" >Victory:</div><div class="searchTextContent" >{{victory}}</div><div class="searchTextRowLabel">Card Text:</div><div class="searchTextContent">{{text}}&nbsp;</div><div class="searchTextRowLabel">Set:</div><div class="searchTextContent">{{setname}}</div><div class="searchTextRowLabel">Number:</div><div class="searchTextContent">{{cardnum}}</div><div class="searchTextRowLabel">Quantity:</div><div class="searchTextContent">{{quantity}}</div><div class="searchTextRowLabel">Illustrator:</div><div class="searchTextContent"> {{illustrator}}</div></div><div class="clear"></div><div class="cardComments"><a href="{{fullurl}}#commentsStart"><img src="/deckbuilder/comments.png">{{cardComments}}</a></div></div></li>';
				templates['Enemy'] = 		'<li class="searchLiCard"><div class="searchDivCard"><div class="searchDivImage"><a href="{{fullurl}}"><img class="searchImgCard" src="{{img}}" alt="{{name}}"></a><div class="rating">{{rating}}</div><div class="deckcount">x{{deckcount}}</div></div><div class="searchText"><div class="searchTextRowLabel" ></div><div class="searchTextContent" ><h1 class="searchH1Name">{{unique}}<a href="{{fullurl}}">{{name}}</a> {{subtitle}} <span class="{{restrictedClass}}">{{restrictedPhrase}}</span></h1></div><div class="searchTextRowLabel">Encounter Set:</div><div class="searchTextContent">{{encounter}}&nbsp;</div><div class="searchTextRowLabel" >Type:</div><div class="searchTextContent" >{{type}}</div><div class="searchTextRowLabel" >Fight:</div><div class="searchTextContent" >{{fight}}</div><div class="searchTextRowLabel" >Health:</div><div class="searchTextContent" >{{health}}</div><div class="searchTextRowLabel" >Evade:</div><div class="searchTextContent" >{{evade}}</div><div class="searchTextRowLabel" >Damage:</div><div class="searchTextContent" >{{damage}}</div><div class="searchTextRowLabel" >Horror:</div><div class="searchTextContent" >{{horror}}</div><div class="searchTextRowLabel {{victoryDisplay}}" >Victory:</div><div class="searchTextContent" >{{victory}}</div><div class="searchTextRowLabel">Card Text:</div><div class="searchTextContent">{{text}}&nbsp;</div><div class="searchTextRowLabel">Set:</div><div class="searchTextContent">{{setname}}</div><div class="searchTextRowLabel">Number:</div><div class="searchTextContent">{{cardnum}}</div><div class="searchTextRowLabel">Quantity:</div><div class="searchTextContent">{{quantity}}</div><div class="searchTextRowLabel">Illustrator:</div><div class="searchTextContent"> {{illustrator}}</div></div><div class="clear"></div><div class="cardComments"><a href="{{fullurl}}#commentsStart"><img src="/deckbuilder/comments.png">{{cardComments}}</a></div></div></li>';
				templates['Act'] = 		'<li class="searchLiCard"><div class="searchDivCard"><div class="searchDivImage"><a href="{{fullurl}}"><img class="searchImgCard" src="{{img}}" alt="{{name}}"><br /><img class="searchImgCard" src="{{imgB}}" alt="{{name}}"></a><div class="rating">{{rating}}</div><div class="deckcount">x{{deckcount}}</div></div><div class="searchText"><div class="searchTextRowLabel" ></div><div class="searchTextContent" ><h1 class="searchH1Name">{{unique}}<a href="{{fullurl}}">{{name}}</a> {{subtitle}} <span class="{{restrictedClass}}">{{restrictedPhrase}}</span></h1></div><div class="searchTextRowLabel">Encounter Set:</div><div class="searchTextContent">{{encounter}}&nbsp;</div><div class="searchTextRowLabel" >Type:</div><div class="searchTextContent" >{{type}}</div><div class="searchTextRowLabel {{victoryDisplay}}" >Victory:</div><div class="searchTextContent" >{{victory}}</div><div class="searchTextRowLabel">Card Text:</div><div class="searchTextContent">{{text}}&nbsp;</div><div class="searchTextRowLabel">Set:</div><div class="searchTextContent">{{setname}}</div><div class="searchTextRowLabel">Number:</div><div class="searchTextContent">{{cardnum}}</div><div class="searchTextRowLabel">Quantity:</div><div class="searchTextContent">{{quantity}}</div><div class="searchTextRowLabel">Illustrator:</div><div class="searchTextContent"> {{illustrator}}</div></div><div class="clear"></div><div class="cardComments"><a href="{{fullurl}}#commentsStart"><img src="/deckbuilder/comments.png">{{cardComments}}</a></div></div></li>';
				templates['Location'] = 		'<li class="searchLiCard"><div class="searchDivCard"><div class="searchDivImage"><a href="{{fullurl}}"><img class="searchImgCard" src="{{img}}" alt="{{name}}"><img class="searchImgCard" src="{{imgB}}" alt="{{name}}"></a><div class="rating">{{rating}}</div><div class="deckcount">x{{deckcount}}</div></div><div class="searchText"><div class="searchTextRowLabel" ></div><div class="searchTextContent" ><h1 class="searchH1Name">{{unique}}<a href="{{fullurl}}">{{name}}</a> {{subtitle}} <span class="{{restrictedClass}}">{{restrictedPhrase}}</span></h1></div><div class="searchTextRowLabel">Encounter Set:</div><div class="searchTextContent">{{encounter}}</div><div class="searchTextRowLabel" >Type:</div><div class="searchTextContent" >{{type}}</div><div class="searchTextRowLabel" >Shroud:</div><div class="searchTextContent" >{{shroud}}</div><div class="searchTextRowLabel" >Clue Value:</div><div class="searchTextContent" >{{clue}}</div><div class="searchTextRowLabel {{victoryDisplay}}" >Victory:</div><div class="searchTextContent" >{{victory}}</div><div class="searchTextRowLabel">Card Text:</div><div class="searchTextContent">{{text}}&nbsp;</div><div class="searchTextRowLabel">Set:</div><div class="searchTextContent">{{setname}}</div><div class="searchTextRowLabel">Number:</div><div class="searchTextContent">{{cardnum}}</div><div class="searchTextRowLabel">Quantity:</div><div class="searchTextContent">{{quantity}}</div><div class="searchTextRowLabel">Illustrator:</div><div class="searchTextContent"> {{illustrator}}</div></div><div class="clear"></div><div class="cardComments"><a href="{{fullurl}}#commentsStart"><img src="/deckbuilder/comments.png">{{cardComments}}</a></div></div></li>';
				
				templates['list'] = 		'<tr><td><h1 class="searchH1Name"><a href="{{fullurl}}">{{unique}}{{name}}</a> <span class="{{restrictedClass}}">{{restrictedPhrase}}</span></h1></td><td>{{class}}{{encounter}}</td><td>{{type}}</td><td>{{cost}}</td><td>{{stats}}</td><td><span class="tooltipGameText" title="{{text}}"><img src="'+deckbuilderGlobals.image_path+'/images/page_white_text.png"</span></td><td>{{setname}} {{cardnum}}</td></tr>';
				
				if (record.restricted == 'R'){
					restrictedText = "Restricted";
				}
				if (record.banned == 'B'){
					restrictedText = "Banned";
				}
				restrictedClass = "hideitem";

				if (view == "spoiler"){
					templateType = record.type;
				} else if (view == "list"){
					templateType = "list";
					
				}

				for (i=1;i<=5;i++){
					if (record.rating >= i){
						rating += staron;
					} else {
						rating += staroff;
					}
						
				}

				if (record.numcomments == 0){
					var comments = 'Be the first to comment on this card!';
				} else if (record.numcomments == 1){
					var comments = '1 Comment';
				} else {
					var comments = record.numcomments + ' Comments';
				}

				if (record.traits != ''){
					gametext = "<strong>"+record.traits+"</strong><br />"+record.text;
				} else {
					gametext = record.text;
				}
				if (record.textb != ''){
					gametext += "<br />"+record.textb;
				}

				if (record.unique == "Y"){
					uniquehtml = "&bull;&nbsp;"
				}

				if (record.will != ""){
					skilltesticons += " Willpower: "+record.will;
					skilltesticonslistview += " W:"+record.will;
				}
				if (record.int != ""){
					skilltesticons += " Intellect: "+record.int;
					skilltesticonslistview += " I:"+record.int;
				}
				if (record.cmbt != ""){
					skilltesticons += " Combat: "+record.cmbt;
					skilltesticonslistview += " C:"+record.cmbt;
				}
				if (record.agi != ""){
					skilltesticons += " Agility: "+record.agi;
					skilltesticonslistview += " A:"+record.agi;
				}
				if (record.wild != ""){
					skilltesticons += " Wild: "+record.wild;
					skilltesticonslistview += " ?:"+record.wild;
				}
				if (templateType == 'list'){
					if (record.type == 'Asset' || record.type == 'Event' || record.type == 'Skill' || record.type == 'Investigator') {
						stats=skilltesticonslistview;
						if ((record.type == 'Asset' || record.type == 'Investigator') && (record.hlth != "" || record.snty != "")){
							stats += " Health: "+record.hlth + " Sanity: "+record.snty;
						}
					} else if (record.type == 'Enemy'){
						stats="F:"+record.fght+" H:"+record.hlth+" E:"+record.evade+" Damage:"+record.dmg+" Horror:"+record.horr;
					}
				}

				if (record.imgf.startsWith('http://') || record.imgf.startsWith('https://')){
					_image = record.imgf;
				} else {
					_image = deckbuilderGlobals.card_images_path+record.imgf;
				}

				if (record.imgb.startsWith('http://') || record.imgb.startsWith('https://')){
					_imageB = record.imgb;
				} else {
					_imageB = deckbuilderGlobals.card_images_path+record.imgb;
				}
				if (record.slot ==""){
					slotDisplay = "hideitem";
				}
				if (record.hlth ==""){
					healthDisplay = "hideitem";
				}
				if (record.snty ==""){
					sanityDisplay = "hideitem";
				}
				if (record.vctry ==""){
					victoryDisplay = "hideitem";
				}

				if (record.count == "") record.count = "?";
				theLI = templates[templateType].replace(/{{type}}/gi, record.type)
					.replace(/{{quantity}}/gi, record.quantity)
					.replace(/{{rating}}/gi, rating)
					.replace(/{{class}}/gi, record.clss)
					.replace(/{{level}}/gi, record.lvl)
					.replace(/{{skilltesticons}}/gi, skilltesticons)
					.replace(/{{encounter}}/gi, record.encounter)
					.replace(/{{shroud}}/gi, record.shrd)
					.replace(/{{clue}}/gi, record.clue)
					.replace(/{{health}}/gi, record.hlth)
					.replace(/{{healthDisplay}}/gi, healthDisplay)
					.replace(/{{sanity}}/gi, record.snty)
					.replace(/{{sanityDisplay}}/gi, sanityDisplay)
					.replace(/{{slot}}/gi, record.slot)
					.replace(/{{slotDisplay}}/gi, slotDisplay)
					.replace(/{{stats}}/gi, stats)
					.replace(/{{victoryDisplay}}/gi, victoryDisplay)
					.replace(/{{victory}}/gi, record.vctry)
					.replace(/{{horror}}/gi, record.horr)
					.replace(/{{damage}}/gi, record.dmg)
					.replace(/{{fight}}/gi, record.fght)
					.replace(/{{evade}}/gi, record.evade)
					.replace(/{{subtitle}}/gi, record.subtitle)
					.replace(/{{cardComments}}/gi, comments)
					.replace(/{{deckcount}}/gi, record.deckquantity)
					.replace(/{{fullurl}}/gi, record.fullurl)
					.replace(/{{id}}/gi, record.id)
					.replace(/{{img}}/gi, _image)
					.replace(/{{imgB}}/gi, _imageB)
					.replace(/{{name}}/gi, record.name)
					.replace(/{{text}}/gi, gametext )
					.replace(/{{cost}}/gi,  record.cost)
					.replace(/{{unique}}/gi,  uniquehtml)
					.replace(/{{illustrator}}/gi,  record.illus)
					.replace(/{{cardnum}}/gi,  record.num)
					.replace(/{{restrictedPhrase}}/gi, restrictedText)
					.replace(/{{restrictedClass}}/gi, restrictedClass)
					.replace(/{{setname}}/gi, record.setname);
				
				return (theLI );

	}
	
	function deckbuilderTemplate(record, view, show){
		var templates = [],
		_image,
		_imageB,
		bannedrestrictedMessage = "",
		restrictedclass = "",
		loyalClass = "",
		loyaltyText = "",
		qtyincore = "",
		_level = "",
		imagepath,
		imagesize,
		imgfolder;

		if(typeof(_show)==='undefined') _show = true;
		cardObjectString = JSON.stringify(record).replaceAll('&', '&amp;')
					            .replaceAll('"', '&quot;')
					            .replaceAll("'", '&#39;')
					            .replaceAll('<', '&lt;')
					            .replaceAll('>', '&gt;');
		
		templates['deck'] = '<li class="cardOption deckCard " id= "dc{{id}}" data-cardid="{{id}}" data-cardcost="{{cost}}" data-cardname="{{name}}" data-cardtype="{{type}}" data-cardobject="'+cardObjectString+'"><table><tr class="trCardRow"><td style="width:40px" class="cardImg"><img src="{{imgf}}" style="padding-right:3px;max-height:50px;max-width:50px;" /></td><td class="tdCardName"><div>{{name}} <span class="{{restrictedClass}}">{{restricted}}</span></div><div class="deckCardSet"><span>{{setname}}</span></div></td><td class="{{levelClass}}"><div>{{level}}</div></td><td style="width:16px;text-align:center;"><span id="deckcardcount{{id}}" class="fright">{{deckquantity}}</span></td><td class="adjustTD" style="width:22px"><div class="add" class="fright"><button id= "dcab{{id}}" data-cardid="{{id}}" data-quantity="1"  class="addCards button-link '+ahGlobals.buttonType+'">+</button></div><div class="subtract" class="fright"><button data-quantity="-1" data-cardid="{{id}}" class="addCards button-link red '+ahGlobals.buttonType+'">-</button></div></td></tr></table></li>';
		templates['search'] = '<li class="cardOption searchCard" id="search{{id}}"  data-cardid="{{id}}" data-taffyid="{{taffyid}}" data-cardobject="'+cardObjectString+'"><table><tr class="trCardRow"><td style="width:40px" class="cardImg"><img src="'+deckbuilderGlobals.image_path+'/images/ic-loading-{{imgsize}}.gif" data-original="{{imgf}}" class="lazy" style="padding-right:3px;max-height:50px;max-width:50px;" /></td><td  class="tdCardName"><div>{{name}} <span class="{{restrictedClass}}">{{restricted}}</span></div><div class="searchCardSet"><span>{{setname}} {{packquantity}}</span></div></td><td class="{{levelClass}}"><div>{{level}}</div></td><td class="adjustTD" style="width:22px"><div class="add" class="fright"><button class="addCards button-link '+ahGlobals.buttonType+'" data-quantity="1" data-taffyid="{{taffyid}}" data-cardid="{{id}}" data-type="{{type}}" data-oset="{{objectiveset}}">+</button></div><div class="add" class="fright"><button class="addCards button-link '+ahGlobals.buttonType+'" data-quantity="{{maxperdeck}}" data-taffyid="{{taffyid}}" data-cardid="{{id}}" data-type="{{type}}" data-oset="{{objectiveset}}">+{{maxperdeck}}</button></div></td></tr></table></li>';
		templates['searchOffFaction'] = '<li class="cardOption searchCard" id="search{{id}}"  data-cardid="{{id}}" data-taffyid="{{taffyid}}"  data-cardobject="'+cardObjectString+'"><table><tr class="trCardRow"><td style="width:40px" class="cardImg"><img src="'+deckbuilderGlobals.image_path+'/images/ic-loading-{{imgsize}}.gif" data-original="{{imgf}}" class="lazy" style="padding-right:3px;max-height:50px;" /></td><td class="tdCardName"><div>{{name}} <span class="{{restrictedClass}}">{{restricted}}</span></div><div class="searchCardSet"><span>{{setname}}</span></div></td><td class="{{levelClass}}"><span>{{level}}</span></td><td class="adjustTD" style="width:22px"></td></tr></table></li>';
		templates['searchBanned'] = '<li class="cardOption searchCard" id="search{{id}}"  data-cardid="{{id}}" data-taffyid="{{taffyid}}"  data-cardobject="'+cardObjectString+'"><table><tr class="trCardRow"><td style="width:40px" class="cardImg"><img src="'+deckbuilderGlobals.image_path+'/images/ic-loading-{{imgsize}}.gif" data-original="{{imgf}}" class="lazy" style="padding-right:3px;max-height:50px;" /></td><td class="tdCardName"><div>{{name}} <span class="R">Banned</span></div><div class="searchCardSet"><span>{{setname}}</span></div></td><td class="adjustTD" style="width:22px"></td></tr></table></li>';
		templates['required'] = '<li class="cardOption deckCard " id= "dc{{id}}" data-cardid="{{id}}" data-cardcost="{{cost}}" data-cardname="{{name}}" data-cardtype="{{type}}" data-cardobject="'+cardObjectString+'"><table><tr class="trCardRow"><td style="width:40px" class="cardImg"><img src="{{imgf}}" style="padding-right:3px;max-height:50px;max-width:50px;" /></td><td class="tdCardName"><div>{{name}} <span class="{{restrictedClass}}">{{restricted}}</span></div><div class="deckCardSet"><span>{{setname}}</span></div></td><td style="width:16px"><span id="deckcardcount{{id}}" class="fright">{{deckquantity}}</span></td></tr></table></li>';
		templates['investigator'] = '<li class="cardOption deckCard " id= "dc{{id}}" data-cardid="{{id}}" data-cardcost="{{cost}}" data-cardname="{{name}}" data-cardtype="{{type}}" data-cardobject="'+cardObjectString+'"><table><tr class="trCardRow"><td style="width:40px" class="cardImg"><img src="{{imgf}}" style="padding-right:3px;max-height:50px;max-width:50px;" /></td><td class="tdCardName"><div>{{name}} <span class="{{restrictedClass}}">{{restricted}}</span></div><div class="deckCardSet"><span>{{setname}}</span></div></td><td style="width:16px"><span id="deckcardcount{{id}}" class="fright">{{deckquantity}}</span></td></tr></table></li>';

		templates['investigatorList'] = '<li class="cardOption searchCard" id="investigatorSearch{{id}}"  data-cardid="{{id}}" data-taffyid="{{taffyid}}" data-squadnumber="{{squadnumber}}" data-class="{{class}}"  data-cardobject="'+cardObjectString+'"><table><tr><td><div style="width:240px;display:inline-block;padding-bottom:5px;" class="cardImg"><img src="'+deckbuilderGlobals.image_path+'/images/ic-loading-{{imgsize}}.gif" data-original="{{imgf}}" class="lazy" style="padding-right:3px;max-width:240px" /><br /><img src="'+deckbuilderGlobals.image_path+'/images/ic-loading-{{imgsize}}.gif" data-original="{{imgb}}" class="lazy" style="padding-right:3px;max-width:240px" /></div><div class="investigatorInfo" style="width:240px;display:inline-block;padding:5px;vertical-align:top;"><div><span class="investigatorName">{{name}}</span> <span class="{{restrictedClass}}">{{restricted}}</span><br /><span>{{setname}}</span><br />{{class}}<br /><div>{{investigatorGameText}}</div></div>{{investigatorButtons}}</div></td></tr></table></li>';

		if (record.max == 0 && view == "search"){
			view = "searchBanned";
		}
		if (record.banned == "B" ){
			restrictedclass = "R";
			bannedrestrictedMessage = "Banned";
		}
		if (record.type == "Investigator"){
			imagesize = 'wide';
		} else {
			imagesize = 'high';
		}

		if (record.wtype == "Basic Weakness" || record.wtype == "Weakness"){
			_level = "";
		} else {
			_level = record.lvl;
		}


		investigatorButtons = '<button class="button-investigator " title="'+record.name+'" data-taffyid="'+record['___id']+'" data-cardid="'+record.id+'" data-type="'+record.type+'" data-investigator="'+record.name+'"  data-set="'+record.setname+'" data-maxdecksize = "'+record.dsize+'" data-deckoptions = "'+record.dopt+'" data-deckrequirements = "'+record.dreq+'" data-deckrestrictions = "'+record.dres+'" data-override = "false">Start Deck</button><button class="view-investigator " title="'+record.name+'" data-taffyid="'+record['___id']+'" data-cardid="'+record.id+'" data-type="'+record.type+'" data-investigator="'+record.name+'"  data-set="'+record.setname+'" >View Required Cards</button>';
		
		if (record.imgf.startsWith('http://') || record.imgf.startsWith('https://')){
			_image = record.imgf;
		} else {
			if (view != "investigatorList"){
				_image = ahGlobals.imagepath+'/tn_'+record.imgf;
			} else {
				_image = ahGlobals.imagepath+'/'+record.imgf;
			}
			
		}

		if (record.imgb.startsWith('http://') || record.imgb.startsWith('https://')){
			_imageB = record.imgb;
		} else {
			if (view != "investigatorList"){
				_imageB = ahGlobals.imagepath+'/tn_'+record.imgb;
			} else {
				_imageB = ahGlobals.imagepath+'/'+record.imgb;
			}
		}

		if (record.setname == "Core"){
			qtyincore = "("+record.quantity+"x in Core)";
		}
		theLI = templates[view].replace(/{{type}}/gi, record.type)
					.replace(/{{fullurl}}/gi, record.fullurl)
					.replace(/{{id}}/gi, record.id)
					.replace(/{{maxperdeck}}/gi, record.max)
					.replace(/{{imgsize}}/gi, imagesize)
					.replace(/{{imgf}}/gi, _image)
					.replace(/{{imgb}}/gi, _imageB)
					.replace(/{{investigatorGameText}}/gi, record.textb)
					.replace(/{{taffyid}}/gi, record['___id'])
					.replace(/{{class}}/gi, record.clss)
					.replace(/{{name}}/gi, record.name)
					.replace(/{{deckquantity}}/gi, record.deckquantity)
					.replace(/{{packquantity}}/gi, qtyincore)
					.replace(/{{restricted}}/gi, bannedrestrictedMessage)
					.replace(/{{cost}}/gi,  record.costnumeric)
					.replace(/{{type}}/gi,  record.type)
					.replace(/{{restrictedClass}}/gi,  restrictedclass)
					.replace(/{{levelClass}}/gi,  record.clss + " clevel" )
					.replace(/{{level}}/gi,  _level)
					.replace(/{{illustrator}}/gi,  record.illustrator)
					.replace(/{{cardnum}}/gi,  record.num)
					.replace(/{{investigatorButtons}}/gi, investigatorButtons)
					.replace(/{{setname}}/gi, record.setname);
		

		return (theLI);
	}

	function cardViewTemplate(cvRecords,cvid,_viewtype){
		if (typeof _viewtype === 'undefined') _viewtype = "inpage";
		var restrictedText = "",
		bannedText = "",
		combinedText,
		imagepath = ahGlobals.imagepath,
		_image = "",
		_imageB = "",
		_imageBHover = "",
		imagefolder,
		restrictedText = "",
		_card;
		filterCVRecords = cvRecords.filter(function (thecard) {
			return (thecard.id == cvid);
		});
		_card = filterCVRecords[0];
		combinedText = _card.text+'<br />'+_card.textb;
		
		imagefolder = _card.imgfolder;
		imageName = imagefolder+'/med_'+_card.img;

		if (_card.img == ""){
			imageName = _card.furl;
		}
		if (_card.traits != "")
			combinedText = "<b>"+_card.traits+"</b><br />"+combinedText 
		if (_card.restricted == "R"|| _card.banned == "B"){
			if (_card.restricted == "R"){
				restrictedText = "Restricted";
			}
			if (_card.banned == "B"){
				bannedText = "Banned";
			}
			combinedText = "<span style='font-weight:bold;color:red'>"+bannedText+restrictedText+"</span><br />"+combinedText
		}
		combinedText = combinedText+"<br /><i><b>"+_card.setname + " #"+ _card.num+" x"+_card.quantity+"</b></i>";
		if (_card.type == "Investigator"){
			imagesize = "wide"
		} else {
			imagesize = "high"
		}

		if (_card.imgf.startsWith('http://') || _card.imgf.startsWith('https://')){
			_image = _card.imgf;
		} else {
			_image = ahGlobals.imagepath+"/"+_card.imgf;
		}
		if (_card.imgb.startsWith('http://') || _card.imgb.startsWith('https://')){
			_imageBHover = '<img src="'+deckbuilderGlobals.image_path+'/images/card-loading-'+imagesize+'-new.gif" data-original="'+_card.imgb+'" class="lazy hoverImage" style="" />';
			_imageB = '<br /><img class="viewImage" src="'+_card.imgb+'" />';
		} else if (_card.imgb != "") {
			_imageBHover = '<img src="'+deckbuilderGlobals.image_path+'/images/card-loading-'+imagesize+'-new.gif" data-original="'+ahGlobals.imagepath+_card.imgb+'" class="lazy hoverImage" style="" />';
			_imageB = '<br /><img class="viewImage" src="'+ahGlobals.imagepath+"/"+_card.imgb+'" />';
		}

		if (navigator.onLine){
			//display = '<div style="height:360px" ><img class="viewImage" src="'+imagepath+'/'+imageName+'.jpg" /></div><br />'+combinedText;
			if (_viewtype == "hover"){
				display = '<div class="hoverImageDiv'+imagesize+'"><img src="'+deckbuilderGlobals.image_path+'/images/card-loading-'+imagesize+'-new.gif" data-original="'+_image+'" class="lazy hoverImage" style="" />'+_imageBHover+'</div><div class="hoverText hoverText'+imagesize+'">'+combinedText+'</div>';
			} else {
				display = '<div class="imageContainer"><img class="viewImage" src="'+_image+'" />'+_imageB+'</div><div class="divImageText">'+combinedText+'</div>';
			}
		} else {
			var templates = [],
			factions=[],
			uniquehtml='',
			loyalText = '',
			iconDisplay = '';

			templates['Character'] = 	'<div class="viewCardText" ><div ><h1 class="searchH1Name">{{unique}}{{name}} <span class="{{restrictedClass}}">{{restrictedPhrase}}</span></h1></div><div class="searchTextRowLabel">Faction:</div><div class="searchTextContent">{{faction}}</div><div class="searchTextRowLabel" >Type:</div><div class="searchTextContent" >{{type}}</div><div class="searchTextRowLabel">Cost:</div><div class="searchTextContent">{{cost}}</div><div class="searchTextRowLabel" >Icons:</div><div class="searchTextContent" >{{icons}}</div><div class="searchTextRowLabel" >Strength:</div><div class="searchTextContent" >{{str}}</div><div class="searchTextRowLabel">Card Text:</div><div class="searchTextContent">{{text}}&nbsp;</div><div class="searchTextRowLabel">Set:</div><div class="searchTextContent">{{setname}}</div><div class="searchTextRowLabel">Number:</div><div class="searchTextContent">{{cardnum}}</div><div class="searchTextRowLabel">Quantity:</div><div class="searchTextContent">{{quantity}}</div><div class="searchTextRowLabel">Illustrator:</div><div class="searchTextContent"> {{illustrator}}</div></div><div class="clear"></div><div class="cardComments"><a href="{{fullurl}}#commentsStart"><img src="/deckbuilder/comments.png">{{cardComments}}</a></div></div></div>';
			templates['Plot'] = 		'<div class="viewCardText" ><div ><h1 class="searchH1Name">{{unique}}{{name}} <span class="{{restrictedClass}}">{{restrictedPhrase}}</span></h1></div><div class="searchTextRowLabel">Faction:</div><div class="searchTextContent">{{faction}}</div><div class="searchTextRowLabel" >Type:</div><div class="searchTextContent" >{{type}}</div><div class="searchTextRowLabel">Gold:</div><div class="searchTextContent">{{gold}}</div><div class="searchTextRowLabel" >Initiative:</div><div class="searchTextContent" >{{initiative}}&nbsp;</div><div class="searchTextRowLabel" >Claim:</div><div class="searchTextContent" >{{claim}}&nbsp;</div><div class="searchTextRowLabel">Card Text:</div><div class="searchTextContent">{{text}}&nbsp;</div><div class="searchTextRowLabel">Reserve:</div><div class="searchTextContent">{{reserve}}</div><div class="searchTextRowLabel">Set:</div><div class="searchTextContent">{{setname}}</div><div class="searchTextRowLabel">Number:</div><div class="searchTextContent">{{cardnum}}</div><div class="searchTextRowLabel">Quantity:</div><div class="searchTextContent">{{quantity}}</div><div class="searchTextRowLabel">Illustrator:</div><div class="searchTextContent"> {{illustrator}}</div></div><div class="clear"></div><div class="cardComments"><a href="{{fullurl}}#commentsStart"><img src="/deckbuilder/comments.png">{{cardComments}}</a></div></div></div>';
			templates['Location'] = 	'<div class="viewCardText" ><div ><h1 class="searchH1Name">{{unique}}{{name}} <span class="{{restrictedClass}}">{{restrictedPhrase}}</span></h1></div><div class="searchTextRowLabel">Faction:</div><div class="searchTextContent">{{faction}}</div><div class="searchTextRowLabel" >Type:</div><div class="searchTextContent" >{{type}}</div><div class="searchTextRowLabel">Cost:</div><div class="searchTextContent">{{cost}}</div><div class="searchTextRowLabel">Card Text:</div><div class="searchTextContent">{{text}}&nbsp;</div><div class="searchTextRowLabel">Set:</div><div class="searchTextContent">{{setname}}</div><div class="searchTextRowLabel">Number:</div><div class="searchTextContent">{{cardnum}}</div><div class="searchTextRowLabel">Quantity:</div><div class="searchTextContent">{{quantity}}</div><div class="searchTextRowLabel">Illustrator:</div><div class="searchTextContent"> {{illustrator}}</div></div><div class="clear"></div><div class="cardComments"><a href="{{fullurl}}#commentsStart"><img src="/deckbuilder/comments.png">{{cardComments}}</a></div></div></div>';
			templates['Event'] = 		'<div class="viewCardText" ><div ><h1 class="searchH1Name">{{unique}}{{name}} <span class="{{restrictedClass}}">{{restrictedPhrase}}</span></h1></div><div class="searchTextRowLabel">Faction:</div><div class="searchTextContent">{{faction}}</div><div class="searchTextRowLabel" >Type:</div><div class="searchTextContent" >{{type}}</div><div class="searchTextRowLabel">Cost:</div><div class="searchTextContent">{{cost}}</div><div class="searchTextRowLabel">Card Text:</div><div class="searchTextContent">{{text}}&nbsp;</div><div class="searchTextRowLabel">Set:</div><div class="searchTextContent">{{setname}}</div><div class="searchTextRowLabel">Number:</div><div class="searchTextContent">{{cardnum}}</div><div class="searchTextRowLabel">Quantity:</div><div class="searchTextContent">{{quantity}}</div><div class="searchTextRowLabel">Illustrator:</div><div class="searchTextContent"> {{illustrator}}</div></div><div class="clear"></div><div class="cardComments"><a href="{{fullurl}}#commentsStart"><img src="/deckbuilder/comments.png">{{cardComments}}</a></div></div></div>';
			templates['Attachment'] = 	'<div class="viewCardText" ><div ><h1 class="searchH1Name">{{unique}}{{name}} <span class="{{restrictedClass}}">{{restrictedPhrase}}</span></h1></div><div class="searchTextRowLabel">Faction:</div><div class="searchTextContent">{{faction}}</div><div class="searchTextRowLabel" >Type:</div><div class="searchTextContent" >{{type}}</div><div class="searchTextRowLabel">Cost:</div><div class="searchTextContent">{{cost}}</div><div class="searchTextRowLabel">Card Text:</div><div class="searchTextContent">{{text}}&nbsp;</div><div class="searchTextRowLabel">Set:</div><div class="searchTextContent">{{setname}}</div><div class="searchTextRowLabel">Number:</div><div class="searchTextContent">{{cardnum}}</div><div class="searchTextRowLabel">Quantity:</div><div class="searchTextContent">{{quantity}}</div><div class="searchTextRowLabel">Illustrator:</div><div class="searchTextContent"> {{illustrator}}</div></div><div class="clear"></div><div class="cardComments"><a href="{{fullurl}}#commentsStart"><img src="/deckbuilder/comments.png">{{cardComments}}</a></div></div></div>';
			templates['Faction'] = 		'<div class="viewCardText" ><div ><h1 class="searchH1Name">{{unique}}{{name}} <span class="{{restrictedClass}}">{{restrictedPhrase}}</span></h1></div><div class="searchTextRowLabel">Faction:</div><div class="searchTextContent">{{faction}}</div><div class="searchTextRowLabel" >Type:</div><div class="searchTextContent" >{{type}}</div><div class="searchTextRowLabel">Cost:</div><div class="searchTextContent">{{cost}}</div><div class="searchTextRowLabel">Card Text:</div><div class="searchTextContent">{{text}}&nbsp;</div><div class="searchTextRowLabel">Set:</div><div class="searchTextContent">{{setname}}</div><div class="searchTextRowLabel">Number:</div><div class="searchTextContent">{{cardnum}}</div><div class="searchTextRowLabel">Quantity:</div><div class="searchTextContent">{{quantity}}</div><div class="searchTextRowLabel">Illustrator:</div><div class="searchTextContent"> {{illustrator}}</div></div><div class="clear"></div><div class="cardComments"><a href="{{fullurl}}#commentsStart"><img src="/deckbuilder/comments.png">{{cardComments}}</a></div></div></div>';
			templates['Agenda'] = 		'<div class="viewCardText" ><div ><h1 class="searchH1Name">{{unique}}{{name}} <span class="{{restrictedClass}}">{{restrictedPhrase}}</span></h1></div><div class="searchTextRowLabel" >Type:</div><div class="searchTextContent" >{{type}}</div><div class="searchTextRowLabel">Card Text:</div><div class="searchTextContent">{{text}}&nbsp;</div><div class="searchTextRowLabel">Set:</div><div class="searchTextContent">{{setname}}</div><div class="searchTextRowLabel">Number:</div><div class="searchTextContent">{{cardnum}}</div><div class="searchTextRowLabel">Quantity:</div><div class="searchTextContent">{{quantity}}</div><div class="searchTextRowLabel">Illustrator:</div><div class="searchTextContent"> {{illustrator}}</div></div><div class="clear"></div><div class="cardComments"><a href="{{fullurl}}#commentsStart"><img src="/deckbuilder/comments.png">{{cardComments}}</a></div></div></div>';
			
			if (_card.traits != ''){
				gametext = "<strong>"+_card.traits+"</strong><br />"+_card.text;
			} else {
				gametext = _card.text;
			}

			if (_card.unique == "Y"){
				uniquehtml = "&bull;&nbsp;"
			}

			if (_card.loyal == "L"){
				loyalText = " (Loyal)";
			}
			if (_card.baratheon == 'Y'){
				factions.push("Baratheon"+loyalText);
			}
			if (_card.greyjoy == 'Y'){
				factions.push("Greyjoy"+loyalText);
			}
			if (_card.lannister == 'Y'){
				factions.push("Lannister"+loyalText);
			}
			if (_card.martell == 'Y'){
				factions.push("Martell"+loyalText);
			}
			if (_card.martell == 'Y'){
				factions.push("Martell"+loyalText);
			}
			if (_card.stark == 'Y'){
				factions.push("Stark"+loyalText);
			}
			if (_card.targaryen == 'Y'){
				factions.push("Targaryen"+loyalText);
			}
			if (_card.tyrell == 'Y'){
				factions.push("Tyrell"+loyalText);
			}
			if (_card.thenightswatch == 'Y'){
				factions.push("The Night's Watch"+loyalText);
			}
			if (_card.neutral == 'Y'){
				factions.push("Neutral"+loyalText)
			}
			if (_card.military == 'Y'){
				iconDisplay += "<img class='spoilerIcon' src='http://www.cardgamedb.com/deckbuilders/gameofthrones2ndedition/military-icon.png' alt='Military' /> "
			}
			if (_card.intrigue == 'Y'){
				iconDisplay += "<img class='spoilerIcon' src='http://www.cardgamedb.com/deckbuilders/gameofthrones2ndedition/intrigue-icon.png' alt='Intrigue' /> "
			}
			if (_card.power == 'Y'){
				iconDisplay += "<img class='spoilerIcon' src='http://www.cardgamedb.com/deckbuilders/gameofthrones2ndedition/power-icon.png' alt='Power' /> "
			}
			display = templates[_card.type].replace(/{{type}}/gi, _card.type)
					.replace(/{{quantity}}/gi, _card.quantity)
					.replace(/{{icons}}/gi, iconDisplay)
					.replace(/{{str}}/gi, _card.strength)
					.replace(/{{gold}}/gi, _card.gold)
					.replace(/{{initiative}}/gi, _card.initiative)
					.replace(/{{claim}}/gi, _card.claim)
					.replace(/{{reserve}}/gi, _card.reserve)
					.replace(/{{faction}}/gi, factions.join(', '))
					.replace(/{{fullurl}}/gi, _card.fullurl)
					.replace(/{{id}}/gi, _card.id)
					.replace(/{{img}}/gi, _image)
					.replace(/{{name}}/gi, _card.name)
					.replace(/{{restricted}}/gi, _card.restricted)
					.replace(/{{text}}/gi, gametext )
					.replace(/{{cost}}/gi,  _card.cost)
					.replace(/{{unique}}/gi,  uniquehtml)
					.replace(/{{restrictedPhrase}}/gi,  restrictedText)
					.replace(/{{restrictedClass}}/gi,  _card.restricted)
					.replace(/{{illustrator}}/gi,  _card.illustrator)
					.replace(/{{cardnum}}/gi,  _card.num)
					.replace(/{{setname}}/gi, _card.setname);
		}
		
		return(display);
	}
